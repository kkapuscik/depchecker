# Development Environment

---------------------------------

## Overview

Following sections describes how to set up the development environment. The list may not be
complete - if anything is missing please update this document or add a ticket so such work
may be scheduled.

### Some useful rules

+   Use paths without whitespace characters (spaces, tabs, etc.)
+   Prefer paths that contains only directory separators and alphanumeric characters
    (a-z, A-Z, 0-9) and selected symbols (hyphen, underscore).



## Repository

DepChecker sources are currently stored in SVN repository on Assembla hosting.

Repository URL: <https://subversion.assembla.com/svn/depchecker/>



## Eclipse

Preferred base version is **Eclipse Standard** version **Kepler** (current: 4.3.1).
It can be downloaded from [Eclipse website](http://www.eclipse.org/).



## Eclipse plugins

After installation of the Eclipse (actually: unpacking it at some place like C:\eclipse43)
start the IDE and create/select the workspace.

### Installation of Maven & Subversive plugins

Using Eclipse **Help** > **Install new software...** menu option install following plugins:

+   Eclipse XML Editors and Tools
+   m2e - Maven Integration for Eclipse
+   Subversive SVN Team Provider
+   Subversive SVN JDT Ignore Extensions (Optional)
+   Subversive SVN Team Provider Localization (Optional)

### Installation of Subversive connectors 

After restarting the eclipse it is needed to install SVN connectors for both Subversive and
Maven.

There are two possible ways to install Subversive connectors. Normally dialog with connector
installation options shall appear automatically (e.g. when entering **Window** > **Preferences** >
**SVN**). Select the **SVNKit** connectors as the pure Java version is preferred.

If for some reason that cannot be done it is needed to manually install the connectors. Add
Polarion connectors update site in Eclipse plugin installation window:
[Site URL](http://community.polarion.com/projects/subversive/download/eclipse/3.0/kepler-site/).
Install following connectors:

+   SVNKit 1.3.8 Implementation (Optional)
+   SVNKit 1.7.10 Implementation (Optional)

Subversive installation instruction is available on
[Eclipse site](http://www.eclipse.org/subversive/installation-instructions.php).
Subversive download paths are available on
[Polarion site](http://www.polarion.com/products/svn/subversive/download.php).

### Installation of Maven connectors

To install Maven connectors it is required to enter Maven connector installation dialog.
To do so select:

1.  Select **Window** > **Preferences** menu option.
2.  Select **Maven** > **Discovery** category.
3.  Click **Open Catalog**.
4.  Select **m2e-subversive** and begin installation.
5.  Wait a moment so the installtion will start and ask for Eclipse restart.



## Importing the project

At this point there shall exist working Eclipse with Maven and Subversive plugins.
The next step is to import the project.

1.  Select **File** > **Import** menu option.
2.  Select **Maven** > **Check out Maven Projects from SCM** in dialog.
3.  Click **Next**.
4.  Select **svn** in combo box near **SCM URL** and enter project repository URL available
    at top of this document. Append **/trunk** at end so the sources from main branch will
    imported.  
5.  Click **Next**, **Finish**.
6.  Enter SVN authorization data when requested.

After above steps a new project shall appear in workspace. Make sure to update the
sources first:

1.  Right-click on the project item **Package Explorer**.
2.  Select **Team** > **Update** from the context menu.
3.  Wait a moment so the sources are updated.
4.  Right-click on the project item **Package Explorer**.
5.  Select **Maven** > **Update Project**
6.  Make sure the project is selected in the dialog.
7.  Click **OK**

Now the fully updated project shall be ready for development.



## Sharing the project

The project should have the Team sharing enabled by default - the repository URL should be
visible just after the project name in the Package Explorer. If that is not true perform the
following steps:

1.  Right click on the project in Package Explorer.
2.  Select **Team** > **Share Project...**.
3.  Select **SVN** in the dialog.
4.  On the next screen verify that the URL is valid and **Use Project settings** option
    is selected.
5.  Click **Finish**.



## Configuring Eclipse

To have consistent development settings which may affect code consistency some steps
are required to configure eclipse.

### Import Eclipse settings

A set of Eclipse settings is present in repository. If the project is present in workspace
following steps may be taken to import them:

1.  Select **File** > **Import** from menu.
2.  Select **General** > **Preferences** in dialog.
3.  Select the preferences files. The file shall be in workspace directory, project subfolder.
    It shall be called **eclipse-settings.epf**.
4.  Make sure **Import all** is checked.
5.  Start import operation.

### Other settings

Below set of options is also required (enter **Window** > **Preferences** dialog):

+   **General** > **Editors** > **TextEditors** cathegory:
    +   **Displayed tab size** = 4.
    +   **Insert spaces for tabs** checked.
    +   **Show print margin** checked.
    +   **Print margin column** set to 100.
    +   **Show line numbers** checked.
+   **XML** > **XML Files** > **Editor** cathegory:
    +   **Line width** set to 100.
    +   **Indent using spaces** selected.
    +   **Indentation size** set to 4.



## Working with Eclipse

Note that normally there is no need to run Maven manually during development. Eclipse will
build the application internally so it is possible to run or debug it without using Maven.

Also normally Eclipse will have automatic build option enabled (check the **Project** >
**Build Automatically** option) so there is no need to request rebuild. The one exception is
when the pom.xml (Maven project description) was modified. In that case it is needed to update
the Eclipse project using Maven settings using following steps:  
 
1.  Right-click on the project item **Package Explorer**.
2.  Select **Maven** > **Update Project**

It is possible to run or debug application as any other project in Eclipse - just create run
or debug configuration for **Java Application** scheme. Detailed manual on how to run or
debug Java application is out of scope of this document.



## Working with Maven in Eclipse
 
Read some articles about Maven first:

+   [Maven in 5 minutes](http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)
+   [Lifecycle introduction](http://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)

To run a Maven with selected goal use:

1.  Right-click on the project item **Package Explorer**.
2.  Select **Run As** > **Maven build\.\.\.** (note the dots).
3.  Enter name of the configuration in **Name** field (e.g. **DepChecker compile**).
4.  Enter name of the goal in **Goals** fields (e.g. **test**).

Commonly used goals:

+   compile - compile the sourceds
+   test    - build & run tests 
+   package - build final package
+   site    - build project site (includes documentation)
