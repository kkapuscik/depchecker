# Project File Definition

---------------------------------

## Overview

The following sections describes the DepChecker project elements details. These description
is normally used to build a valid DepChecker project file XML scheme definition.

Currently defined version is 0.1 (major.minor). All previous version definitions should be stored
in the repository. If there are more than one project definition available there should be a way
to migrate from older version to newer one - the opposite is not required.



## Main project elements

Three main project elements are currently defined:  

+   Info Header
+   Input
+   Preprocessor
+   Doxygen



### Info Header

Information header shall hold all the information describing the project. That may include:

+   File format version
+   Project name

The file format version is used to determine which project definition should be used for a
specific project file. As mentioned above if currently supported version is never there must
be a way to migrate an older version to the new one.

Project name should be specified by the user.


### Input

Input group contains information about files and directories that should be processed.

Currently the group is used to define files and directories to be processed and information
if directories shall be processed recursively.


### Preprocessor
 
Preprocessor settings group holds settings for input preprocessor. Currently it is possible
to set a list of directories describing external include directories.


### Doxygen

Doxygen group is used to hold extra settings for Doxygen-based input processing backend.


## Enhancement proposals

This sections is a summary of change proposals that may be introduced in next versions of
project definition. 

The new things to be added include:

+   Author
+   Copyright
+   Description
+   File filters (both input & include)
+   More doxygen settings
