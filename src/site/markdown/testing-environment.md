# Testing Environment

---------------------------------

## Overview

Following sections describes how the project will be tested and how to prepare the
environment to write & run the tests.



## Types of tests

Currently supported/used types of tests:

+   **Unit/Whitebox tests**

### What are the whitebox tests?

The concept is to use unit test approach (class with test methods) but if it is possible
focus on testing functionalities instead of testing single methods.

Rationale: there will be not enough time to perform full unit-testing and working
functionality is much more important than covering all methods by tests.



## Writing tests

Following sections describes how to write new tests.

### Writing unit/whitebox tests

To create unit/whitebox tests the standard java JUnit fremework will be used.

Simple tutorial on how to write tests is available
on [Lars Vogel website](http://www.vogella.com/articles/JUnit/article.html).

Some details may also be found on
[JUnit website](http://junit.org/).



## Running tests

Following sections describes how to run tests.

### Running unit tests

To run unit tests use the standard Maven **test** goal.

Details on how to run Maven form Eclipse could be found in Development Environment document.
In simple steps:

1.  Right-click on the project item **Package Explorer**.
2.  Select **Run As** > **Maven build\.\.\.** (note the dots).
3.  Enter name of the configuration (e.g. **DepChecker run tests**) in **Name** field.
4.  Enter name of the goal (in this case **test**) in **Goals** fields.
 