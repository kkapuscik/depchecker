# Project Overview

---------------------------------

## Goal(s)

The goal of DepChecker project is to create an utility for generation and visualization
of code dependency graphs.

The initial version shall be able to process C/C++ source code files and generate graphs
from dependencies between the files. Other features like file contents or object files
analysis may be added in the future.



## Description

DepChecker is a tool for developers to allow them generate dependency graphs and
visualize them for further analysis. It will take specified source files, other project
properties like include files or user-defined groups and generate the graph. Then using
existing visualization framework the graph will be presented to the user.

The main idea is to create an application which will be able to:

+   analyze the sources
    +   source paths/files
    +   include path/files
    +   group definitions
+   detect the dependencies
+   create the dependency graph
+   post-process the graph
    +   e.g. give possibility to remove meaningless dependencies
+   visualize the graph
    +   using different graph layout algorithms



## Selected technologies

The technologies mentioned in following sections were selected to be used for DepChecker
development. The selection was done keeping in mind that the development should be easy
but the paths for extending the project shall not become closed.

### Programming Language

Java was selected as a main language for the following reasons:

+   No/Limited memory management is needed (simplifies the code).
+   Many libraries are available and could be taken to do project tasks with them.
+   Great IDEs exists for Java like NetBeans or Eclipse.
+   Multiplatform availability.

### Project management platform

Assembla Renzoku was selected as project management platform. It consists of
agile/scrum/kanban-like project management tools including things backlog, milestones
and workflows as well as a SVN repository for storing the code.

### Build engine

Maven was selected as a build engine as it is widely used by many Java projects. Also
it solves many non-trivial problems like deploying the application, running tests or
solving library dependencies.

### IDE

Eclipse was selected as an IDE:

+   One of the leading Java IDEs.
+   Very user-friendly.
+   Multiplatform.
+   Maven support is available.
+   SVN support is available.

### JAXB

As there will be user project definitions needed it was decided that XML format shall
be used to store them. That given possibility to extend them easily in future as well as
make it acceptable by other tools if needed. Java provides great utility to create connection
between XML and code - the Java Architecture for XML Binding.

Also there is a plugin for Maven that automates code generation from given XSD (XML Schema)
files.

### JavaFX

JavaFX was selected as a library to create User Interface. The reason was that it is the
built-in Java UI library supported directly by Oracle and promoted as the current solution
for new Java applications.

JavaFX gives possibility to deploy the library as both an web site application and a desktop
application - and such possibility may be very useful in future.

JavaFX Scene Builder is an application for designing the UI. It is available for download at
http://www.oracle.com/technetwork/java/javafx/tools/index.html   

### Markdown

Markdown was selected as a tool to support creating documentation. It joins two in one:

+   clarity of plain text
+   possibility to embed in generated documentation (HTML-like format)

There is a Maven plugin available so no manual work is needed to convert markdown files
into final documentation form.

### ZGRViewer / ZVTM

Graph visualization is one the crucial tasks of DepChecker application. As the matter
is complicated (graph layouting, fast browsing etc.) the decision was to use existing
solution. ZGRViewer which is build on top of ZVTM library is an application that is
prepared to visualize large graphs and is used by universities and laboratories.

### Logging

Logging is an important thing when it comes to troubleshooting. Also logging is useful from
user point of view: user wants to know what is happening and why something went wrong.

As there is a built-in logging feature in Java it will be used. The classes are available
in *java.util.logging* package.
