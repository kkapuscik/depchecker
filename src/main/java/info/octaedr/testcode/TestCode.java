package info.octaedr.testcode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import info.octaedr.depchecker.engine.DotGraphSerializer;
import info.octaedr.depchecker.engine.EngineManager;
import info.octaedr.depchecker.engine.doxygen.DoxygenInputProcessor;
import info.octaedr.depchecker.engine.simple.SimpleInputProcessor;
import info.octaedr.depchecker.model.DepCheckerModel;
import info.octaedr.depchecker.model.InputGraph;
import info.octaedr.depchecker.model.LogWriter;
import info.octaedr.depchecker.tools.Path;
import info.octaedr.graphviz.GraphvizExecutor;
import info.octaedr.graphviz.LayoutCommand;

public class TestCode {

    private static class ConsoleLogWriter implements LogWriter {

        @Override
        public void debug(final String message) {
            System.out.println("DBG : " + message);
        }

        @Override
        public void info(final String message) {
            System.out.println("INFO: " + message);
        }

        @Override
        public void warning(final String message) {
            System.out.println("WARN: " + message);
        }

        @Override
        public void error(final String message) {
            System.out.println("ERR : " + message);
        }
        
    }
    
    public static void main(String[] args) {
        try {
            /* project file */
            final File projectFile = new File(
                    "f:\\MyWorkspace\\depchecker\\samples\\project1\\project1.dcp");
            if (!projectFile.exists()) {
                throw new FileNotFoundException(projectFile.toString());
            }

            /* prepare directories */
            final File baseDirectory = projectFile.getParentFile();
            final File outputDirectory = Path.resolve(baseDirectory, "output");
            if (!outputDirectory.exists()) {
                outputDirectory.mkdir();
            }

            /* create & engine and register its components */
            final EngineManager engineManager = new EngineManager();
            engineManager.registerProcessor(new DoxygenInputProcessor());
            engineManager.registerProcessor(new SimpleInputProcessor());

            /* load the project */
            final DepCheckerModel model = new DepCheckerModel(engineManager);
            model.getProjectModel().load(projectFile);

            /* process the project data */
            final SimpleInputProcessor processor = new SimpleInputProcessor();
            final InputGraph[] graphs = processor.process(model.getProjectModel(), baseDirectory,
                    outputDirectory, new ConsoleLogWriter());
            if (graphs != null) {
                System.out.println("SZATAN!");

                /* generate output */
                for (final InputGraph graph : graphs) {
                    System.out.println(graph.getLocator().toString());

                    final File dotGraphFile = Path.resolve(outputDirectory, graph.getLocator()
                            .toString() + ".dot");

                    final DotGraphSerializer serializer = new DotGraphSerializer();
                    serializer.serialize(graph.getData(), dotGraphFile);

                    System.out.println("SZATAN2!");

                    final GraphvizExecutor executor = new GraphvizExecutor(LayoutCommand.FDP);
                    executor.setLogWriter(new ConsoleLogWriter());
                    executor.setOutputFormat("svg");
                    executor.setInputFile(dotGraphFile);
                    executor.setWorkingDirectory(outputDirectory);
                    executor.run();

                    System.out.println("SZATAN3!");
                }
            } else {
                System.out.println("Processing failed");
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
