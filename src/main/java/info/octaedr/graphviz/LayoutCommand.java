package info.octaedr.graphviz;

/**
 * Graphviz layout commands.
 */
public class LayoutCommand {

    /**
     * CIRCO layouter.
     */
    public static final LayoutCommand CIRCO = new LayoutCommand("circo");

    /**
     * DOT layouter.
     */
    public static final LayoutCommand DOT = new LayoutCommand("dot");

    /**
     * FDP layouter.
     */
    public static final LayoutCommand FDP = new LayoutCommand("fdp");

    /**
     * NEATO layouter.
     */
    public static final LayoutCommand NEATO = new LayoutCommand("neato");

    /**
     * OSAGE layouter.
     */
    public static final LayoutCommand OSAGE = new LayoutCommand("osage");

    /**
     * SFDP layouter.
     */
    public static final LayoutCommand SFDP = new LayoutCommand("sfdp");

    /**
     * TWOPI layouter.
     */
    public static final LayoutCommand TWOPI = new LayoutCommand("twopi");

    /**
     * Name of the layout command.
     */
    private final String name;

    /**
     * Constructs the command.
     * 
     * @param name
     *            Name of the command.
     */
    public LayoutCommand(final String name) {
        this.name = name;
    }

    /**
     * Returns name of the command.
     * 
     * @return Name of the command.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns string representation of the object.
     * 
     * @return String with the name of the command.
     */
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return super.toString();
    }
}
