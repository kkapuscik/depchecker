package info.octaedr.graphviz;

import java.io.File;

import info.octaedr.cmd.PathExecutableFinder;
import info.octaedr.cmd.ProcessExecutor;

/**
 * Utility for running Graphviz.
 */
public class GraphvizExecutor extends ProcessExecutor {

    /**
     * Input file.
     */
    private File inputFile;

    /**
     * Output file.
     * 
     * <p>
     * If null then the input file with prefix is used.
     */
    private File outputFile;

    /**
     * Output format.
     * 
     * <p>
     * If null default 'positioned dot' output is created.
     */
    private String outputFormat;

    /**
     * Constructs the executor.
     * 
     * <p>
     * Tries to set the executable name using layout command name and searching
     * the path.
     * 
     * @param layoutCommand
     *            Layout command.
     */
    public GraphvizExecutor(final LayoutCommand layoutCommand) {
        setExecutable(new PathExecutableFinder().find(layoutCommand.getName()
                + PathExecutableFinder.DEFAULT_WINDOWS_EXE_EXTENSION, layoutCommand.getName()));
    }

    /**
     * Constructs the executor.
     * 
     * <p>
     * Sets the executable to the one given.
     * 
     * @param exeFile
     *            Executable file to be run.
     */
    public GraphvizExecutor(final File exeFile) {
        setExecutable(exeFile);
    }

    /**
     * Returns input file.
     * 
     * @return Input file or null if not set.
     */
    public File getInputFile() {
        return this.inputFile;
    }

    /**
     * Sets input file.
     * 
     * @param inputFile
     *            Input file or null to unset.
     */
    public void setInputFile(final File inputFile) {
        this.inputFile = inputFile;
    }

    /**
     * Returns output file.
     * 
     * @return Output file or null if not set.
     */
    public File getOutputFile() {
        return this.outputFile;
    }

    /**
     * Sets output file.
     * 
     * @param outputFile
     *            Output file or null to unset.
     */
    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

    /**
     * Returns output format.
     * 
     * @return Output format or null if not set.
     */
    public String getOutputFormat() {
        return this.outputFormat;
    }

    /**
     * Sets output format.
     * 
     * @param outputFormat
     *            Output format to set or null to unset.
     */
    public void setOutputFormat(String outputFormat) {
        this.outputFormat = outputFormat;
    }

    @Override
    public boolean run() {
        if (this.inputFile == null) {
            return false;
        }

        clearArguments();

        // turn on verbose mode
        addArgument("-v");
        
        if (this.outputFormat != null) {
            addArgument("-T");
            addArgument(this.outputFormat);
        }
        
        if (this.outputFile == null) {
            addArgument("-O");
        } else {
            addArgument("-o");
            addArgument(this.outputFile.getAbsolutePath());
        }

        addArgument(this.inputFile.getAbsolutePath());

        return super.run();
    }

}
