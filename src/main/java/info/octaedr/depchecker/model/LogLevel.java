package info.octaedr.depchecker.model;

/**
 * Level of importance of log message.
 */
public enum LogLevel {

    /**
     * Error messages log level.
     */
    ERROR(0),

    /**
     * Warning messages log level.
     */
    WARNING(1),

    /**
     * Informational messages log level.
     */
    INFO(2),

    /**
     * Debug messages log level.
     */
    DEBUG(3);

    /**
     * Level priority.
     */
    private final int priority;

    /**
     * Constructs enum value.
     * 
     * @param priority
     *            Level priority.
     */
    LogLevel(final int priority) {
        this.priority = priority;
    }

    /**
     * Checks if this level includes the one given.
     * 
     * @param other
     *            The other level to check.
     * 
     * @return True if this level includes the one given, false otherwise.
     */
    public boolean includes(final LogLevel other) {
        return other.priority <= this.priority;
    }

}
