package info.octaedr.depchecker.model;

import java.io.File;
import java.util.prefs.Preferences;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Recently opened files model.
 */
public class RecentFilesModel {

    private static final String RECENT_PREFS_KEY_PREFIX = "recent";

    private static final String EMPTY_VALUE = "";

    private static final int MAX_RECENT_FILES_SIZE = 5;

    private final ReadOnlyObjectWrapper<ObservableList<String>> recentPaths = new ReadOnlyObjectWrapper<>(
            FXCollections.<String> observableArrayList());

    /**
     * Constructs the model.
     */
    RecentFilesModel() {
        // nothing to do
    }
    
    void setProjectModel(final ProjectModel projectModel) {
        projectModel.projectFileProperty().addListener(new ChangeListener<File>() {

            @Override
            public void changed(final ObservableValue<? extends File> observable, final File oldValue, final File newValue) {
                RecentFilesModel.this.onProjectFileChange(newValue);
            }
        });
        
        loadList();
    }

    private void onProjectFileChange(final File newValue) {
        if (newValue != null) {
            final ObservableList<String> list = this.recentPaths.getValue();
            final String projectFilePath = newValue.getAbsolutePath();

            // remove if project was already on list
            list.remove(projectFilePath);
            // add as first element of the list
            list.add(0, projectFilePath);

            // make sure the list is not too big
            while (list.size() > MAX_RECENT_FILES_SIZE) {
                list.remove(list.size() - 1);
            }
            
            saveList();
        }
    }

    private void saveList() {
        final Preferences prefsNode = Preferences.userNodeForPackage(getClass());
        
        final ObservableList<String> list = this.recentPaths.getValue();
        
        int index = 0;
        for (; (index < list.size()) && (index < MAX_RECENT_FILES_SIZE); ++index) {
            prefsNode.put(RECENT_PREFS_KEY_PREFIX + index, list.get(index));
        }
        for (; index < MAX_RECENT_FILES_SIZE; ++index) {
            prefsNode.put(RECENT_PREFS_KEY_PREFIX + index, EMPTY_VALUE);
        }
    }

    private void loadList() {
        final Preferences prefsNode = Preferences.userNodeForPackage(getClass());
        
        final ObservableList<String> list = this.recentPaths.getValue();
        
        list.clear();
        
        int index = 0;
        for (; index < MAX_RECENT_FILES_SIZE; ++index) {
            final String value = prefsNode.get(RECENT_PREFS_KEY_PREFIX + index, null);
            if ((value != null) && (value.length() > 0)) {
                list.add(value);
            }
        }
    }


    public final ObservableList<String> getRecentPaths() {
        return this.recentPaths.get();
    }

    public final ReadOnlyObjectProperty<ObservableList<String>> recentPathsProperty() {
        return this.recentPaths.getReadOnlyProperty();
    }

}
