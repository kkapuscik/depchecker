package info.octaedr.depchecker.model;

import info.octaedr.depchecker.UserPreferences;

import java.io.File;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


/**
 * Model with preferences dialog data.
 */
public class PreferencesDataModel {

    /**
     * path to doxygen executable.
     */    
    private final StringProperty doxygenFilePath = new SimpleStringProperty();
        
    /**
     * path to graphviz executable.
     */    
    private final StringProperty graphvizDirPath = new SimpleStringProperty();
    
    /**
     * Constructs the model.
     */
    PreferencesDataModel() {
        this.doxygenFilePath.set(UserPreferences.getDoxygenPath());
        this.graphvizDirPath.set(UserPreferences.getGraphvizPath());
    }

    /**
     * Returns path to doxygen executable
     * 
     * @return path to doxygen executable
     */
    public final String getDoxygenFilePath() {
        return this.doxygenFilePath.get();
    }

    /**
     * Sets path to doxygen executable     
     * 
     * @param path
     *            path to doxygen executable
     * 
	 */
	public void setDoxygenFilePath(final String path) {
		this.doxygenFilePath.set(path);
        UserPreferences.setDoxygenPath(this.doxygenFilePath.get());
	}

	/**
     * Returns application name property.
     * 
     * @return Property object.
     */
    public final StringProperty doxygenFilePathProperty() {
        return this.doxygenFilePath;
    }
    
    /**
     * Returns path to graphviz executable
     * 
     * @return path to graphviz executable
     */
    public final String getGraphvizDirPath() {
        return this.graphvizDirPath.get();
    }

    /**
     * Sets path to graphviz executable     
     * 
     * @param path
     *            path to graphviz executable
     * 
	 */
	public void setGraphvizDirPath(final String path) {
		this.graphvizDirPath.set(path);
        UserPreferences.setGraphvizPath(this.graphvizDirPath.get());
	}

	/**
     * Returns application name property.
     * 
     * @return Property object.
     */
    public final StringProperty graphvizDirPathProperty() {
        return this.graphvizDirPath;
    }

    public boolean checkDoxygenPath(final String filePath) {
        final File file = new File(filePath);
        return file.canExecute() && file.isFile();
    }

    public boolean checkGraphvizPath(final String dirPath) {
        final File dir = new File(dirPath);
        if (dir.isDirectory()) {
            final File dot = new File(dir, "dot");
            if (dot.isFile() && dot.canExecute()) {
                return true;
            }

            final File dotExe = new File(dir, "dot.exe");
            if (dotExe.isFile() && dotExe.canExecute()) {
                return true;
            }
        }
        return false;
    }
}
