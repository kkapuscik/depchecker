package info.octaedr.depchecker.model;

public interface LogWriter {

    void debug(String message);
    
    void info(String message);
    
    void warning(String message);
    
    void error(String message);
    
}
