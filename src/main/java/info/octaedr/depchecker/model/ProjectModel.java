package info.octaedr.depchecker.model;

import java.io.File;
import java.io.IOException;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

/**
 * Model of the project.
 */
public class ProjectModel {

    /**
     * Current project file.
     */
    private final ReadOnlyObjectWrapper<File> projectFile = new ReadOnlyObjectWrapper<>();

    /**
     * Flag indicating that project was modified.
     */
    private final ReadOnlyBooleanWrapper modifiedFlag = new ReadOnlyBooleanWrapper();

    private final StringProperty projectName = new SimpleStringProperty();

    private final BooleanProperty builtinStlSupport = new SimpleBooleanProperty();

    private final ReadOnlyObjectWrapper<ObservableList<DirectoryInfo>> inputDirectories = new ReadOnlyObjectWrapper<>(
            FXCollections.<DirectoryInfo> observableArrayList());

    private final ReadOnlyObjectWrapper<ObservableList<FileInfo>> inputFiles = new ReadOnlyObjectWrapper<>(
            FXCollections.<FileInfo> observableArrayList());

    private final BooleanProperty inputRecursive = new SimpleBooleanProperty();

    private final ReadOnlyObjectWrapper<ObservableList<DirectoryInfo>> includeDirectories = new ReadOnlyObjectWrapper<>(
            FXCollections.<DirectoryInfo> observableArrayList());

    private final StringProperty outputDirectory = new SimpleStringProperty();
    
    /**
     * Constructs empty project.
     */
    ProjectModel() {
        // create listener for properties modification
        final ChangeListener<Object> modificationListener = new ChangeListener<Object>() {
            @Override
            public void changed(ObservableValue<? extends Object> arg0, Object arg1, Object arg2) {
                ProjectModel.this.modifiedFlag.set(true);
            }
        };

        final ListChangeListener<Object> listModificationListener = new ListChangeListener<Object>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends Object> value) {
                ProjectModel.this.modifiedFlag.set(true);
            }
        };

        this.projectName.addListener(modificationListener);
        this.builtinStlSupport.addListener(modificationListener);
        this.inputDirectories.get().addListener(listModificationListener);
        this.inputFiles.get().addListener(listModificationListener);
        this.inputRecursive.addListener(modificationListener);
        
        clear();
    }

    /**
     * Returns project modified flag.
     * 
     * @return True if project was modified after last save, false otherwise.
     */
    public final boolean getModifiedFlag() {
        return this.modifiedFlag.get();
    }

    /**
     * Returns project modified flag property.
     * 
     * @return Property object.
     */
    public final ReadOnlyBooleanProperty modifiedFlagProperty() {
        return this.modifiedFlag.getReadOnlyProperty();
    }

    /**
     * Returns current project file.
     * 
     * @return Current project file or null if project was not yet saved.
     */
    public final File getProjectFile() {
        return this.projectFile.get();
    }

    /**
     * Returns current project file property.
     * 
     * @return Property object.
     */
    public final ReadOnlyObjectProperty<File> projectFileProperty() {
        return this.projectFile.getReadOnlyProperty();
    }

    public final String getProjectName() {
        return this.projectName.get();
    }

    public final void setProjectName(final String projectName) {
        this.projectName.set(projectName);
    }

    public final StringProperty projectNameProperty() {
        return this.projectName;
    }

    public final boolean getBuiltinStlSupport() {
        return this.builtinStlSupport.get();
    }

    public final void setBuiltinStlSupport(final boolean value) {
        this.builtinStlSupport.set(value);
    }

    public final BooleanProperty builtinStlSupportProperty() {
        return this.builtinStlSupport;
    }

    public final ObservableList<DirectoryInfo> getInputDirectories() {
        return this.inputDirectories.get();
    }

    public final ReadOnlyObjectProperty<ObservableList<DirectoryInfo>> inputDirectoriesProperty() {
        return this.inputDirectories.getReadOnlyProperty();
    }

    public final ObservableList<DirectoryInfo> getIncludeDirectories() {
        return this.includeDirectories.get();
    }

    public final ReadOnlyObjectProperty<ObservableList<DirectoryInfo>> includeDirectoriesProperty() {
        return this.includeDirectories.getReadOnlyProperty();
    }

    public final ObservableList<FileInfo> getInputFiles() {
        return this.inputFiles.get();
    }

    public final ReadOnlyObjectProperty<ObservableList<FileInfo>> inputFilesProperty() {
        return this.inputFiles.getReadOnlyProperty();
    }

    public final boolean getInputRecursive() {
        return this.inputRecursive.get();
    }

    public final void setInputRecursive(final boolean value) {
        this.inputRecursive.set(value);
    }

    public final BooleanProperty inputRecursiveProperty() {
        return this.inputRecursive;
    }

    public final String getOutputDirectory() {
        return this.outputDirectory.get();
    }
    
    public final void setOutputDirectory(final String path) {
        this.outputDirectory.set(path);
    }
    
    public StringProperty outputDirectoryProperty() {
        return this.outputDirectory;
    }

    /**
     * Clears the contents of the project.
     * 
     * <p>
     * Removes all the project content and resets project file.
     */
    public void clear() {
        // reset internal state
        clearProjectFile();

        // reset project info
        setProjectName(null);
        // reset input settings
        getInputDirectories().clear();
        getInputFiles().clear();
        setInputRecursive(true);
        // reset output settings
        setOutputDirectory("output");
        // reset preprocessor settings
        getIncludeDirectories().clear();
        // reset doxygen settings
        setBuiltinStlSupport(true);

        clearModified();
    }

    /**
     * Loads project contents from file.
     * 
     * <p>
     * If project was loaded successfully the project file will change to the
     * one given.
     * 
     * @param file
     *            The file from which project shall be loaded.
     * 
     * @throws IOException
     *             when project could not be loaded due to any I/O error.
     */
    public void load(final File file) throws IOException {
        assert file != null;

        // initial clear
        clear();

        final ProjectSerializer serializer = new ProjectSerializer(this);
        if (serializer.deserialize(file)) {
            setProjectFile(file);
            clearModified();
        } else {
            clear();
            throw new IOException("Cannot open project from given file: " + file);
        }
    }

    /**
     * Saves project contents to a file.
     * 
     * <p>
     * If project was saved successfully the project file will change to the one
     * given.
     * 
     * @param file
     *            The file to which project shall be saved.
     * 
     * @throws IOException
     *             when project could not be loaded due to any I/O error.
     */
    public void save(final File file) throws IOException {
        assert file != null;

        final ProjectSerializer serializer = new ProjectSerializer(this);
        if (serializer.serialize(file)) {
            setProjectFile(file);
            clearModified();
        } else {
            clear();
            throw new IOException("Cannot save project to given file: " + file);
        }
    }

    /**
     * Checks the status of project modification flag.
     * 
     * @return Current value of the flag.
     */
    public boolean isModified() {
        return this.modifiedFlag.get();
    }

    /**
     * Sets the project modification flag.
     */
    public void setModified() {
        this.modifiedFlag.set(true);
    }

    /**
     * Clears the project modification flag.
     */
    private void clearModified() {
        this.modifiedFlag.set(false);
    }

    /**
     * Clears the project file currently set.
     */
    private void clearProjectFile() {
        this.projectFile.set(null);
    }

    /**
     * Sets the project file.
     * 
     * @param file
     *            Project file to be set.
     */
    private void setProjectFile(final File file) {
        assert file != null;

        this.projectFile.set(file);
    }

}
