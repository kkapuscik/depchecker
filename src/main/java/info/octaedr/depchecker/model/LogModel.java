package info.octaedr.depchecker.model;

import java.util.ArrayList;

import javafx.application.Platform;

public class LogModel {

    private class Writer implements LogWriter {

        private final String name;

        public Writer(final String name) {
            assert name != null;

            this.name = name;
        }

        @Override
        public void debug(final String message) {
            LogModel.this.writeMessage(this.name, LogLevel.DEBUG, message);
        }

        @Override
        public void info(final String message) {
            LogModel.this.writeMessage(this.name, LogLevel.INFO, message);
        }

        @Override
        public void warning(final String message) {
            LogModel.this.writeMessage(this.name, LogLevel.WARNING, message);
        }

        @Override
        public void error(final String message) {
            LogModel.this.writeMessage(this.name, LogLevel.ERROR, message);
        }

    }

    private final ArrayList<LogListener> listenerCollection = new ArrayList<>();
    private LogListener[] listenersArray = new LogListener[0];

    LogModel() {
        // nothing to do
    }

    public synchronized void addListener(final LogListener listener) {
        assert listener != null;

        this.listenerCollection.add(listener);

        final LogListener[] newListenersArray = this.listenerCollection
                .toArray(new LogListener[this.listenerCollection.size()]);

        setListenersArray(newListenersArray);
    }

    public LogWriter getWriter(final String name) {
        return new Writer(name);
    }

    public synchronized void clearLog() {
        final LogListener[] listeners = getListenersArray();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                for (final LogListener listener : listeners) {
                    listener.logCleared();
                }
            }
        });
    }
    
    private void writeMessage(final String source, final LogLevel level, final String message) {
        final LogListener[] listeners = getListenersArray();

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                for (final LogListener listener : listeners) {
                    listener.lineAdded(source, level, message);
                }
            }
        });
    }

    private synchronized void setListenersArray(final LogListener[] newListenersArray) {
        this.listenersArray = newListenersArray;
    }

    private synchronized LogListener[] getListenersArray() {
        return this.listenersArray;
    }

}
