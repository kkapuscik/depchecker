package info.octaedr.depchecker.model;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;

/**
 * Element described by path.
 * 
 * <p>
 * Object of this class are used to represent the paths in project definition.
 */
public class PathInfo {

    /**
     * File path.
     */
    private final ReadOnlyStringWrapper path = new ReadOnlyStringWrapper();

    /**
     * Creates the path info.
     * 
     * <p>
     * The path is set to the given one.
     * 
     * @param path
     *            Path to set.
     */
    public PathInfo(final String path) {
        assert path != null;
        
        setPath(path);
    }

    /**
     * Returns the path.
     * 
     * @return Path string.
     */
    public final String getPath() {
        return this.path.get();
    }

    /**
     * Sets the path file.
     * 
     * @param path
     *            Path to set.
     */
    public final void setPath(final String path) {
        assert path != null;

        this.path.set(path);
    }

    /**
     * Returns the file path property.
     * 
     * @return Property object.
     */
    public final ReadOnlyStringProperty pathProperty() {
        return this.path.getReadOnlyProperty();
    }

}
