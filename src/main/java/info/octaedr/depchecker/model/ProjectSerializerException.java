package info.octaedr.depchecker.model;

/**
 * Serialization/deserialization exception.
 */
public class ProjectSerializerException extends Exception {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -7690316917134311620L;

    /**
     * Constructs exception.
     */
    public ProjectSerializerException() {
        super();
    }

    /**
     * Constructs exception.
     * 
     * @param message
     *            Exception message.
     */
    public ProjectSerializerException(final String message) {
        super(message);
    }

    /**
     * Constructs exception.
     * 
     * @param cause
     *            Exception cause.
     */
    public ProjectSerializerException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructs exception.
     * 
     * @param message
     *            Exception message.
     * @param cause
     *            Exception cause.
     */
    public ProjectSerializerException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
