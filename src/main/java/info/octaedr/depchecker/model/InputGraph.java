package info.octaedr.depchecker.model;

import info.octaedr.depchecker.engine.graph.DependencyGraph;

/**
 * Graph being an input to the analysis.
 */
public class InputGraph {

    /**
     * Graph locator.
     */
    private final InputGraphLocator locator;
    
    /**
     * Graph data.
     */
    private final DependencyGraph data;

    /**
     * Constructs the graph.
     * 
     * @param locator
     *            Graph locator.
     * @param data
     *            Graph data.
     */
    public InputGraph(final InputGraphLocator locator, final DependencyGraph data) {
        assert locator != null;
        assert data != null;

        this.locator = locator;
        this.data = data;
    }

    /**
     * Returns graph locator.
     * 
     * @return Locator of the graph.
     */
    public InputGraphLocator getLocator() {
        return this.locator;
    }

    /**
     * Returns the graph data.
     * 
     * @return Data of the graph.
     */
    public DependencyGraph getData() {
        return this.data;
    }
}
