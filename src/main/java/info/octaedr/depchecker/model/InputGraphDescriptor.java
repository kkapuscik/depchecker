package info.octaedr.depchecker.model;

/**
 * Descriptor of the input graph.
 */
public class InputGraphDescriptor {

    /**
     * Graph locator.
     */
    private final InputGraphLocator locator;

    /**
     * Graph name.
     */
    private final String name;

    /**
     * Constructs the descriptor.
     * 
     * @param locator
     *            Graph locator.
     * @param name
     *            Graph name.
     */
    public InputGraphDescriptor(final InputGraphLocator locator, final String name) {
        this.locator = locator;
        this.name = name;
    }

    /**
     * Returns locator of the graph.
     * 
     * @return Locator object.
     */
    public InputGraphLocator getLocator() {
        return this.locator;
    }

    /**
     * Returns name of the graph.
     * 
     * @return Name string.
     */
    public String getName() {
        return this.name;
    }
    
}
