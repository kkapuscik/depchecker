package info.octaedr.depchecker.model;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;

/**
 * Information about the 3rd party element (library).
 */
public class ThirdPartyInfo {

    /**
     * Name of the 3rd party element.
     */
    private final ReadOnlyStringWrapper name = new ReadOnlyStringWrapper();

    /**
     * License that applies to 3rd party element.
     */
    private final ReadOnlyStringWrapper license = new ReadOnlyStringWrapper();

    /**
     * Constructs 3rd party element.
     * 
     * @param name
     *            Name of the 3rd party element.
     * @param license
     *            License that applies to 3rd party element.
     */
    public ThirdPartyInfo(final String name, final String license) {
        assert name != null;
        assert license != null;

        this.name.set(name);
        this.license.set(license);
    }

    /**
     * Returns name of 3rd party element.
     * 
     * @return Name of the element.
     */
    public final String getName() {
        return this.name.get();
    }

    /**
     * Returns name property.
     * 
     * @return Property object.
     */
    public final ReadOnlyStringProperty nameProperty() {
        return this.name.getReadOnlyProperty();
    }

    /**
     * Returns license of 3rd party element.
     * 
     * @return License text.
     */
    public final String getLicense() {
        return this.license.get();
    }

    /**
     * Returns license property.
     * 
     * @return Property object.
     */
    public final ReadOnlyStringProperty licenseProperty() {
        return this.license.getReadOnlyProperty();
    }

}