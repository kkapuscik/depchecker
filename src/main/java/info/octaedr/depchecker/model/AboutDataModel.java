package info.octaedr.depchecker.model;

import info.octaedr.depchecker.FatalException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.jar.Manifest;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Model with application about data.
 */
public class AboutDataModel {

    /**
     * WORKAROUND for broken manifest generation.
     * 
     * FIXME Investigate the manifest generation.
     */
    private final static boolean BROKEN_MANIFEST_WORKAROUND = true;

    /**
     * Application name.
     */
    private final ReadOnlyStringWrapper appName = new ReadOnlyStringWrapper();

    /**
     * Application version string.
     */
    private final ReadOnlyStringWrapper appVersion = new ReadOnlyStringWrapper();

    /**
     * Credits text.
     */
    private final ReadOnlyStringWrapper credits = new ReadOnlyStringWrapper();

    /**
     * License text.
     */
    private final ReadOnlyStringWrapper license = new ReadOnlyStringWrapper();

    /**
     * Third-party information collection.
     */
    private final ReadOnlyObjectWrapper<ObservableList<ThirdPartyInfo>> thirdPartyInfos = new ReadOnlyObjectWrapper<>();

    /**
     * Constructs the model.
     */
    AboutDataModel() {
        loadManifestData();
        loadCredits();
        loadLicense();
        loadThirdPartyInfos();
    }

    /**
     * Returns application name.
     * 
     * @return Application name.
     */
    public final String getAppName() {
        return this.appName.get();
    }

    /**
     * Returns application name property.
     * 
     * @return Property object.
     */
    public final ReadOnlyStringProperty appNameProperty() {
        return this.appName.getReadOnlyProperty();
    }

    /**
     * Returns application version string.
     * 
     * @return Application version.
     */
    public String getAppVersion() {
        return this.appVersion.get();
    }

    /**
     * Returns application version property.
     * 
     * @return Property object.
     */
    public final ReadOnlyStringProperty appVersionProperty() {
        return this.appVersion.getReadOnlyProperty();
    }

    /**
     * Returns credits text.
     * 
     * @return Credits text.
     */
    public final String getCredits() {
        return this.credits.get();
    }

    /**
     * Returns credits text property.
     * 
     * @return Property object.
     */
    public final ReadOnlyStringProperty creditsProperty() {
        return this.credits.getReadOnlyProperty();
    }

    /**
     * Returns license text.
     * 
     * @return License text.
     */
    public final String getLicense() {
        return this.license.get();
    }

    /**
     * Returns license text property.
     * 
     * @return Property object.
     */
    public final ReadOnlyStringProperty licenseProperty() {
        return this.license.getReadOnlyProperty();
    }

    /**
     * Returns third party info collection.
     * 
     * @return Unmodifiable observable list.
     */
    public final ObservableList<ThirdPartyInfo> getThirdPartyInfos() {
        return FXCollections.unmodifiableObservableList(this.thirdPartyInfos.get());
    }

    /**
     * Returns third party info collection property.
     * 
     * @return Property object.
     */
    public final ReadOnlyObjectProperty<ObservableList<ThirdPartyInfo>> thirdPartyInfosProperty() {
        return this.thirdPartyInfos.getReadOnlyProperty();
    }

    /**
     * Loads application data from manifest.
     */
    void loadManifestData() {
        if (BROKEN_MANIFEST_WORKAROUND) {
            this.appName.set("DepChecker");
            this.appVersion.set("-Manifest-Missing-");
        } else {
            try {
                final InputStream manifestStream = openResourceStream("META-INF/MANIFEST.MF");
    
                final Manifest appManifest = new Manifest(manifestStream);
    
                this.appName.set(appManifest.getMainAttributes().getValue("Implementation-Title"));
                this.appVersion.set(appManifest.getMainAttributes().getValue("Implementation-Version"));
    
                if ((getAppName() == null) || (getAppVersion() == null)) {
                    throw new FatalException("Manifest is incomplete - missing about data");
                }
            } catch (final IOException e) {
                throw new FatalException("Cannot load about data from manifest", e);
            }
        }
    }

    /**
     * Loads credits text.
     */
    private void loadCredits() {
        try {
            final InputStream resourceStream = openResourceStream("info/octaedr/depchecker/about/credits.txt");

            this.credits.set(combineText(readTextLines(resourceStream)));
        } catch (final IOException e) {
            throw new FatalException("Cannot load credits data", e);
        }
    }

    /**
     * Loads license text.
     */
    private void loadLicense() {
        try {
            final InputStream resourceStream = openResourceStream("info/octaedr/depchecker/about/license.txt");

            this.license.set(combineText(readTextLines(resourceStream)));
        } catch (final IOException e) {
            throw new FatalException("Cannot load license data", e);
        }
    }

    /**
     * Loads third-party information.
     */
    private void loadThirdPartyInfos() {
        final ObservableList<ThirdPartyInfo> infos = FXCollections.observableArrayList();

        try {
            final InputStream resourceStream = openResourceStream("info/octaedr/depchecker/about/thirdparty.txt");

            final ArrayList<String> thirdPartyEntries = readTextLines(resourceStream);
            for (String entry : thirdPartyEntries) {
                final String[] tokens = entry.split("-", 2);
                if (tokens.length != 2) {
                    throw new IOException("Invalid third party info: " + entry);
                }
                final String entryId = tokens[0].trim();
                final String entryName = tokens[1].trim();

                final InputStream licenseStream = openResourceStream("info/octaedr/depchecker/about/"
                        + entryId + "-license.txt");
                final String entryLicenseLines = combineText(readTextLines(licenseStream));

                infos.add(new ThirdPartyInfo(entryName, entryLicenseLines));
            }
        } catch (final IOException e) {
            throw new FatalException("Cannot load license data", e);
        }

        this.thirdPartyInfos.set(infos);
    }

    /**
     * Opens stream to a resource data.
     * 
     * @param resourceName
     *            Name of the resource including path.
     * 
     * @return Opened stream.
     * 
     * @throws IOException
     *             if stream cannot be opened.
     */
    private InputStream openResourceStream(final String resourceName) throws IOException {
        final InputStream resourceStream = getClass().getClassLoader().getResourceAsStream(
                resourceName);
        if (resourceStream == null) {
            throw new IOException("Resource not found: " + resourceName);
        }
        return resourceStream;
    }

    /**
     * Reads text lines from given stream.
     * 
     * <p>
     * If the line read from stream ends with a '\' character then it is
     * concatenated with the next line.
     * 
     * @param inputStream
     *            Stream for reading the data.
     * 
     * @return Collection of lines read from stream.
     * 
     * @throws IOException
     *             if any I/O error occurred.
     */
    private ArrayList<String> readTextLines(final InputStream inputStream) throws IOException {
        final ArrayList<String> lines = new ArrayList<>();
        final BufferedReader linesReader = new BufferedReader(new InputStreamReader(inputStream));
        final StringBuilder lineBuffer = new StringBuilder();

        for (;;) {
            final String line = linesReader.readLine();
            if (line == null) {
                break;
            }

            if (line.endsWith("\\")) {
                lineBuffer.append(line.substring(0, line.length() - 1));
            } else {
                lineBuffer.append(line);
                lines.add(lineBuffer.toString());
                lineBuffer.setLength(0);
            }
        }

        if (lineBuffer.length() > 0) {
            lines.add(lineBuffer.toString());
        }

        linesReader.close();

        return lines;
    }

    /**
     * Combines the lines to a text.
     * 
     * <p>
     * The combined text consists of the lines separated by newline characters.
     * 
     * @param lines
     *            Lines to be combined.
     * 
     * @return Combined text.
     */
    private String combineText(final ArrayList<String> lines) {
        StringBuilder builder = new StringBuilder();

        for (final String line : lines) {
            builder.append(line);
            builder.append('\n');
        }

        return builder.toString();
    }

}
