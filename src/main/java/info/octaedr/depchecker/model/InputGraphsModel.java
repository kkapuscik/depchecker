package info.octaedr.depchecker.model;

import info.octaedr.depchecker.engine.EngineManager;
import info.octaedr.depchecker.engine.InputProcessor;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class InputGraphsModel {

    private final EngineManager engineManager;

    private final ReadOnlyObjectWrapper<ObservableList<InputProcessorEntry>> inputProcessors = new ReadOnlyObjectWrapper<>(
            FXCollections.<InputProcessorEntry> observableArrayList());

    private final ReadOnlyObjectWrapper<ObservableList<InputGraphEntry>> inputGraphs = new ReadOnlyObjectWrapper<>(
            FXCollections.<InputGraphEntry> observableArrayList());

    InputGraphsModel(final EngineManager engineManager) {
        assert engineManager != null;

        this.engineManager = engineManager;

        /* prepare input processors collection */
        for (int i = 0; i < this.engineManager.getProcessorCount(); ++i) {
            final InputProcessor processor = this.engineManager.getProcessor(i);

            this.inputProcessors.getValue().add(new InputProcessorEntry(processor));
        }
    }

    public final ObservableList<InputProcessorEntry> getInputProcessors() {
        return this.inputProcessors.get();
    }

    public final ReadOnlyObjectProperty<ObservableList<InputProcessorEntry>> inputProcessorsProperty() {
        return this.inputProcessors.getReadOnlyProperty();
    }

    public ObservableList<InputGraphEntry> getInputGraphs() {
        return this.inputGraphs.get();
    }

    public final ReadOnlyObjectProperty<ObservableList<InputGraphEntry>> inputGraphsProperty() {
        return this.inputGraphs.getReadOnlyProperty();
    }

}
