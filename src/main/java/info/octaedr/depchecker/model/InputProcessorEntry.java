package info.octaedr.depchecker.model;

import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import info.octaedr.depchecker.engine.InputProcessor;

public class InputProcessorEntry {

    private final InputProcessor processor;
    private final ReadOnlyStringWrapper uniqueId = new ReadOnlyStringWrapper();
    private final ReadOnlyStringWrapper name = new ReadOnlyStringWrapper();

    public InputProcessorEntry(final InputProcessor processor) {
        this.processor = processor;
        assert processor != null;

        this.uniqueId.setValue(processor.getUniqueId());
        this.name.setValue(processor.getName());
    }

    public String getUniqueId() {
        return this.uniqueId.getValue();
    }

    public ReadOnlyStringProperty uniqueIdProperty() {
        return this.uniqueId.getReadOnlyProperty();
    }

    public String getName() {
        return this.name.getValue();
    }

    public ReadOnlyStringProperty nameProperty() {
        return this.name.getReadOnlyProperty();
    }

    public InputProcessor getInputProcessor() {
        return this.processor;
    }

}
