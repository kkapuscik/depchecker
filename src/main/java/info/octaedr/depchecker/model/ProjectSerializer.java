package info.octaedr.depchecker.model;

import info.octaedr.depchecker.FatalException;
import info.octaedr.depchecker.xml.DirPath;
import info.octaedr.depchecker.xml.Doxygen;
import info.octaedr.depchecker.xml.FilePath;
import info.octaedr.depchecker.xml.Info;
import info.octaedr.depchecker.xml.Input;
import info.octaedr.depchecker.xml.ObjectFactory;
import info.octaedr.depchecker.xml.Output;
import info.octaedr.depchecker.xml.Preprocessor;
import info.octaedr.depchecker.xml.Project;
import info.octaedr.depchecker.xml.Version;

import java.io.File;
import java.net.URL;
import java.util.List;

import javafx.collections.ObservableList;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

/**
 * Utility class for project serialization/deserialization.
 */
class ProjectSerializer {

    /**
     * Model for which this serializer is built.
     */
    private final ProjectModel model;

    /**
     * JAXB context for marshalling/unmarshalling.
     */
    private final JAXBContext jaxbContext;

    /**
     * Factory for XML elements.
     */
    private final ObjectFactory xmlObjectFactory;

    /**
     * Factory for creating validation schemas.
     */
    private final SchemaFactory schemaFactory;

    /**
     * Constructs the utility.
     * 
     * @param model
     *            Model for which utility is built.
     */
    public ProjectSerializer(final ProjectModel model) {
        this.model = model;
        this.xmlObjectFactory = new ObjectFactory();
        this.schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try {
            this.jaxbContext = JAXBContext.newInstance(Project.class.getPackage().getName());
        } catch (JAXBException e) {
            throw new FatalException("Cannot create JAXB context");
        }
    }

    /**
     * Serializes project to a file.
     * 
     * @param outputFile
     *            File to which project shall be saved.
     * 
     * @return True on success, false on error.
     */
    public boolean serialize(final File outputFile) {
        assert outputFile != null;

        boolean result = false;

        try {
            final Project xmlProject = serializeProject();
            if (xmlProject == null) {
                throw new JAXBException("Cannot build XML");
            }

            final JAXBElement<Project> rootElement = this.xmlObjectFactory
                    .createProject(xmlProject);

            final Marshaller marshaller = this.jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            marshaller.marshal(rootElement, outputFile);

            result = true;
        } catch (final JAXBException e) {
            // TODO
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Deserializes project from a file.
     * 
     * @param inputFile
     *            File with serialized project.
     * 
     * @return True on success, false on error.
     */
    public boolean deserialize(final File inputFile) {
        assert inputFile != null;

        boolean result = false;

        try {
            final URL schemaUrl = getClass().getClassLoader().getResource("depchecker-v0.1.xsd");
            if (schemaUrl == null) {
                throw new JAXBException("Schema resource is not available");
            }

            final Schema xmlSchema = this.schemaFactory.newSchema(schemaUrl);

            final Unmarshaller unmarshaller = this.jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(xmlSchema);    
            
            final Object unmarshalResult = unmarshaller.unmarshal(inputFile);
            if (!(unmarshalResult instanceof JAXBElement)) {
                throw new JAXBException("Invalid unmarshaling result (not a JAXBElement)");
            }

            final JAXBElement<?> projectElement = (JAXBElement<?>) unmarshalResult;
            if (!(projectElement.getValue() instanceof Project)) {
                throw new JAXBException("Invalid unmarshaling result (not a Project)");
            }

            Project project = (Project) projectElement.getValue();

            deserializeProject(project);
            
            result = true;
        } catch (final JAXBException e) {
            // TODO
            e.printStackTrace();
        } catch (final ProjectSerializerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }

    private void deserializeProject(final Project project) throws ProjectSerializerException {
        deserializeInfo(project.getInfo());
        deserializeInput(project.getInput());
        deserializeOutput(project.getOutput());
        deserializePrepreocessor(project.getPreprocessor());
        deserializeDoxygen(project.getDoxygen());
    }

    private Project serializeProject() {
        final Project xmlProject = new Project();

        xmlProject.setInfo(serializeInfo());
        xmlProject.setInput(serializeInput());
        xmlProject.setOutput(serializeOutput());
        xmlProject.setPreprocessor(serializePreprocessor());
        xmlProject.setDoxygen(serializeDoxygen());

        return xmlProject;
    }

    private void deserializeInfo(final Info info) throws ProjectSerializerException {
        if ((info.getVersion().getMajor() != 0) ||(info.getVersion().getMinor() != 1)) {
            throw new ProjectSerializerException("Invalid version (expected 0.1)");
        }

        this.model.setProjectName(info.getName());
    }

    private Info serializeInfo() {
        final Info xmlInfo = new Info();

        final Version xmlVersion = new Version();
        xmlVersion.setMajor(0);
        xmlVersion.setMinor(1);

        xmlInfo.setVersion(xmlVersion);

        xmlInfo.setName(this.model.getProjectName());

        return xmlInfo;
    }

    private void deserializeInput(final Input input) {
        deserializeInputDirectories(input.getDirectory());
        deserializeInputFiles(input.getFile());
        
        this.model.setInputRecursive(input.isRecursive());
    }

    private Input serializeInput() {
        final Input xmlInput = new Input();

        serializeInputDirectories(xmlInput.getDirectory());
        serializeInputFiles(xmlInput.getFile());
        
        xmlInput.setRecursive(this.model.getInputRecursive());

        return xmlInput;
    }

    private void deserializeOutput(final Output input) {
        this.model.setOutputDirectory(input.getDirectory().getPath());
    }
    
    private Output serializeOutput() {
        final Output xmlOutput = new Output();
        
        final DirPath xmlOutputDirectory = new DirPath();
        xmlOutputDirectory.setPath(this.model.getOutputDirectory());
        
        xmlOutput.setDirectory(xmlOutputDirectory);
        
        return xmlOutput;
    }

    private void serializeInputFiles(final List<FilePath> xmlFiles) {
        xmlFiles.clear();
        
        for (final FileInfo projectFile : this.model.getInputFiles()) {
            final FilePath xmlPath = new FilePath();
            xmlPath.setPath(projectFile.getPath());
            
            xmlFiles.add(xmlPath);
        }
    }

    private void deserializeInputFiles(final List<FilePath> files) {
        final ObservableList<FileInfo> projectFiles = this.model.getInputFiles();
        projectFiles.clear();

        for (final FilePath file: files) {
            final String path = file.getPath();
            
            // silently skip empty paths
            if (!path.isEmpty()) {
                projectFiles.add(new FileInfo(path));
            }
        }
    }

    private void serializeInputDirectories(final List<DirPath> xmlDirectories) {
        xmlDirectories.clear();
        
        for (final DirectoryInfo projectDir : this.model.getInputDirectories()) {
            final DirPath xmlPath = new DirPath();
            xmlPath.setPath(projectDir.getPath());
            
            xmlDirectories.add(xmlPath);
        }
    }

    private void deserializeInputDirectories(final List<DirPath> xmlDirectories) {
        final ObservableList<DirectoryInfo> inputDirectories = this.model.getInputDirectories();
        inputDirectories.clear();

        for (final DirPath dir: xmlDirectories) {
            final String path = dir.getPath();
            
            // silently skip empty paths
            if (!path.isEmpty()) {
                inputDirectories.add(new DirectoryInfo(path));
            }
        }
    }
    
    private Preprocessor serializePreprocessor() {
        final Preprocessor xmlPreprocessor = new Preprocessor();

        serializeIncludeDirectories(xmlPreprocessor.getIncDir());

        return xmlPreprocessor;
    }

    private void deserializePrepreocessor(final Preprocessor preprocessor) {
        deserializeIncludeDirectories(preprocessor.getIncDir());
    }

    private void serializeIncludeDirectories(final List<DirPath> xmlDirectories) {
        xmlDirectories.clear();
        
        for (final DirectoryInfo projectDir : this.model.getIncludeDirectories()) {
            final DirPath xmlPath = new DirPath();
            xmlPath.setPath(projectDir.getPath());
            
            xmlDirectories.add(xmlPath);
        }
    }

    private void deserializeIncludeDirectories(final List<DirPath> xmlDirectories) {
        final ObservableList<DirectoryInfo> includeDirectories = this.model.getIncludeDirectories();
        includeDirectories.clear();

        for (final DirPath dir: xmlDirectories) {
            final String path = dir.getPath();
            
            // silently skip empty paths
            if (!path.isEmpty()) {
                includeDirectories.add(new DirectoryInfo(path));
            }
        }
    }
    
    private void deserializeDoxygen(final Doxygen doxygen) {
        this.model.setBuiltinStlSupport(doxygen.isBuiltinStlSupport());
    }

    private Doxygen serializeDoxygen() {
        final Doxygen xmlDoxygen = new Doxygen();

        xmlDoxygen.setBuiltinStlSupport(this.model.getBuiltinStlSupport());

        return xmlDoxygen;
    }

}
