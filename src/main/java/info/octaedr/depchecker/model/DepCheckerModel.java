package info.octaedr.depchecker.model;

import info.octaedr.depchecker.engine.EngineManager;

/**
 * DepChecker data model.
 */
public class DepCheckerModel {

    /**
     * Manager of the engine elements.
     */
    private final EngineManager engineManager;

    /**
     * About information sub-model.
     */
    private final AboutDataModel aboutDataModel;

    /**
     * Log sub-model.
     */
    private final LogModel logModel;

    /**
     * Preferences sub-model.
     */
    private final PreferencesDataModel preferencesDataModel;

    /**
     * Project information sub-model.
     */
    private final ProjectModel projectModel;

    /**
     * Recent files sub-model.
     */
    private final RecentFilesModel recentFilesModel;

    /**
     * Input graphs sub-model.
     */
    private final InputGraphsModel inputGraphsModel;

    /**
     * Constructs the model.
     * 
     * @param engineManager
     *            Manager of the engine elements.
     */
    public DepCheckerModel(final EngineManager engineManager) {
        assert engineManager != null;

        this.engineManager = engineManager;

        this.aboutDataModel = new AboutDataModel();
        this.logModel = new LogModel();
        this.preferencesDataModel = new PreferencesDataModel();
        this.projectModel = new ProjectModel();
        this.recentFilesModel = new RecentFilesModel();
        this.inputGraphsModel = new InputGraphsModel(this.engineManager);

        this.recentFilesModel.setProjectModel(this.projectModel);
    }

    /**
     * Returns about information sub-model.
     * 
     * @return About data model.
     */
    public AboutDataModel getAboutDataModel() {
        return this.aboutDataModel;
    }

    /**
     * Returns preferences sub-model.
     * 
     * @return Preferences data model.
     */
    public PreferencesDataModel getPreferencesDataModel() {
        return this.preferencesDataModel;
    }

    /**
     * Returns project information sub-model.
     * 
     * @return Project information model.
     */
    public ProjectModel getProjectModel() {
        return this.projectModel;
    }

    /**
     * Returns recent files sub-model.
     * 
     * @return Recent files model.
     */
    public RecentFilesModel getRecentFilesModel() {
        return this.recentFilesModel;
    }

    /**
     * Returns input graphs sub-model.
     * 
     * @return Input graphs model.
     */
    public InputGraphsModel getInputGraphsModel() {
        return this.inputGraphsModel;
    }

    /**
     * Returns log sub-model.
     * 
     * @return Log model.
     */
    public LogModel getLogModel() {
        return this.logModel;
    }
}
