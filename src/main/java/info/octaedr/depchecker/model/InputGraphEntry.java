package info.octaedr.depchecker.model;

import java.util.Date;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.ReadOnlyStringWrapper;

public class InputGraphEntry {

    private final ReadOnlyObjectWrapper<InputGraph> graph = new ReadOnlyObjectWrapper<>();
    private final ReadOnlyStringWrapper userName = new ReadOnlyStringWrapper();
    private final ReadOnlyStringWrapper graphName = new ReadOnlyStringWrapper();
    private final ReadOnlyStringWrapper processorName = new ReadOnlyStringWrapper();
    private final ReadOnlyStringWrapper updateDate = new ReadOnlyStringWrapper();

    public InputGraphEntry(final InputGraph graph, final String userName, final String graphName, String processorName) {
        this.graph.set(graph);
        this.userName.set(userName);
        this.graphName.set(graphName);
        this.processorName.set(processorName);
        this.updateDate.set(new Date().toString());
    }

    public String getUserName() {
        return this.userName.get();
    }

    public ReadOnlyStringProperty userNameProperty() {
        return this.userName.getReadOnlyProperty();
    }


    public String getGraphName() {
        return this.graphName.get();
    }

    public ReadOnlyStringProperty graphNameProperty() {
        return this.graphName.getReadOnlyProperty();
    }

    public String getProcessorName() {
        return this.processorName.get();
    }

    public ReadOnlyStringProperty processorNameProperty() {
        return this.processorName.getReadOnlyProperty();
    }

    public String getUpdateDate() {
        return this.updateDate.get();
    }

    public ReadOnlyStringProperty updateDateProperty() {
        return this.updateDate.getReadOnlyProperty();
    }

    public InputGraph getGraph() {
        return this.graph.get();
    }

    public ReadOnlyObjectProperty<InputGraph> graphProperty() {
        return this.graph.getReadOnlyProperty();
    }
}
