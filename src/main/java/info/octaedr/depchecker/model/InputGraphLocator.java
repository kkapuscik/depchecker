package info.octaedr.depchecker.model;

/**
 * Locator for the input graph.
 */
public final class InputGraphLocator {

    /**
     * Unique identifier of the input processor.
     */
    private final String inputProcessorId;

    /**
     * Unique identifier of the graph.
     */
    private final String graphId;

    /**
     * Constructs the locator.
     * 
     * @param inputProcessorId
     *            Unique identifier of the input processor.
     * @param graphId
     *            Unique identifier of the graph.
     */
    public InputGraphLocator(final String inputProcessorId, final String graphId) {
        assert inputProcessorId != null;
        assert graphId != null;

        this.inputProcessorId = inputProcessorId;
        this.graphId = graphId;
    }

    /**
     * Returns input processor identifier.
     * 
     * @return Unique identifier of the input processor.
     */
    public String getInputProcessorId() {
        return this.inputProcessorId;
    }

    /**
     * Returns graph identifier.
     * 
     * @return Unique identifier of the graph.
     */
    public String getGraphId() {
        return this.graphId;
    }

    @Override
    public boolean equals(final Object obj) {
        boolean result = false;

        if (obj instanceof InputGraphLocator) {
            final InputGraphLocator other = (InputGraphLocator) obj;

            result = this.getGraphId().equals(other.getGraphId());
            result &= this.getInputProcessorId().equals(other.getInputProcessorId());
        }
        
        return result;
    }

    @Override
    public int hashCode() {
        return this.inputProcessorId.hashCode() + this.graphId.hashCode();
    }

}
