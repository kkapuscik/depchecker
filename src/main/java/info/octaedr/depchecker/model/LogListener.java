package info.octaedr.depchecker.model;

public interface LogListener {

    void logCleared();

    void lineAdded(String source, LogLevel level, String message);
    
}
