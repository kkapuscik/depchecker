package info.octaedr.depchecker.model;

/**
 * Descriptor of the file.
 * 
 * <p>
 * Object of this class are used to represent the files in project definition.
 * 
 * <p>
 * Currently it is a simple wrapper over path created for type safety purposed
 * and to allow easy changes in future.
 */
public class FileInfo extends PathInfo {

    /**
     * Creates the file info.
     * 
     * <p>
     * The file path is set to the given one.
     * 
     * @param path
     *            Path to the file.
     */
    public FileInfo(final String path) {
        super(path);
    }

}
