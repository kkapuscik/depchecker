package info.octaedr.depchecker.model;

/**
 * Descriptor of the output graph.
 * 
 * <p>
 * The descriptor stores all the information needed to reconstruct output graph
 * from input data including include graph and a set of filter to be applied.
 */
public class OutputGraphDescriptor {

    /**
     * Locator of the input graph.
     */
    private InputGraphLocator inputGraphLocator;

    /**
     * Constructs descriptor without any input definition.
     */
    public OutputGraphDescriptor() {
        // nothing to do
    }

    /**
     * Returns locator to input graph to be used.
     * 
     * @return Input graph locator or null if not defined.
     */
    public InputGraphLocator getInputGraphType() {
        return this.inputGraphLocator;
    }

    /**
     * Sets locator to input graph to be used.
     * 
     * @param locator
     *            Input graph specified by type. Null to unset.
     */
    public void setInputGraphLocator(final InputGraphLocator locator) {
        this.inputGraphLocator = locator;
    }

}
