package info.octaedr.depchecker.model;

/**
 * Descriptor of the directory.
 * 
 * <p>
 * Object of this class are used to represent the directories in project
 * definition.
 * 
 * <p>
 * Currently it is a simple wrapper over path created for type safety purposed
 * and to allow easy changes in future.
 */
public class DirectoryInfo extends PathInfo {

    /**
     * Creates the directory info.
     * 
     * @param path
     *            Path to the directory.
     */
    public DirectoryInfo(final String path) {
        super(path);
    }

}
