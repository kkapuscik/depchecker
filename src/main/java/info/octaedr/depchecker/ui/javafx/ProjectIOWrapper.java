package info.octaedr.depchecker.ui.javafx;

import java.io.File;
import java.io.IOException;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.ui.javafx.common.MessageBox;
import info.octaedr.depchecker.ui.javafx.common.MessageBoxButton;

/**
 * Wrapper over project I/O operations.
 * 
 * <p>
 * This class is used to protect the user from losing the data when application
 * is closed, new project is loaded etc. by checking if project should not be
 * saved before the requested operation is done.
 * 
 * <p>
 * It is also used to provide a single point of interaction between the project
 * I/O and the user interface by managing the file dialogs.
 */
public class ProjectIOWrapper {

    /**
     * Extension filter - "DepChecker Project".
     */
    private static final ExtensionFilter EXTENSION_FILTER_DEPCHECKER_PROJECT = new ExtensionFilter(
            "DepChecker Project", "*.dcp");

    /**
     * Extension filter - "All Files".
     */
    private static final ExtensionFilter EXTENSION_FILTER_ALL_FILES = new ExtensionFilter(
            "All Files", "*.*");

    /**
     * Model for which the I/O operations are wrapped.
     */
    private ProjectModel model;

    /**
     * File chooser dialog.
     */
    private FileChooser projectFileChooser;

    /**
     * Stage being parent of the dialogs.
     */
    private Stage stage;

    /**
     * Constructs the wrapper.
     */
    public ProjectIOWrapper() {
        // nothing to do
    }

    /**
     * Initializes the wrapper user interface elements.
     */
    public void initialize() {
        this.projectFileChooser = new FileChooser();
        this.projectFileChooser.getExtensionFilters().add(EXTENSION_FILTER_DEPCHECKER_PROJECT);
        this.projectFileChooser.getExtensionFilters().add(EXTENSION_FILTER_ALL_FILES);
    }

    /**
     * Sets stage that shall be used as dialogs parent.
     * 
     * @param stage
     *            Stage to be used.
     */
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    /**
     * Sets the model to be wrapped.
     * 
     * @param model
     *            Model object.
     */
    public void setModel(final ProjectModel model) {
        this.model = model;
    }

    /**
     * Processes the "New Project" request.
     */
    public void onProjectNew() {
        try {
            if (!processSaveModified()) {
                return;
            }

            // then clear the model
            this.model.clear();
        } catch (final IOException e) {
            MessageBox.warning(this.stage, "I/O Error",
                    "Error during the operation:\n" + e.getLocalizedMessage());
        }
    }

    /**
     * Processes the "Open Project" request.
     */
    public void onProjectOpen() {
        try {
            if (!processSaveModified()) {
                return;
            }

            // ask user for file to be loaded
            final File projectFile = showOpenDialog();
            if (projectFile != null) {
                this.model.load(projectFile);
            }
        } catch (final IOException e) {
            MessageBox.warning(this.stage, "I/O Error",
                    "Error during the operation:\n" + e.getLocalizedMessage());
        }
    }

    /**
     * Processes the "Save Project" request.
     */
    public void onProjectSave() {
        try {
            File projectFile = this.model.getProjectFile();

            if (projectFile == null) {
                projectFile = showSaveDialog();
                if (projectFile == null) {
                    return;
                }
            }

            this.model.save(projectFile);
        } catch (final IOException e) {
            MessageBox.warning(this.stage, "I/O Error",
                    "Error during the operation:\n" + e.getLocalizedMessage());
        }
    }

    /**
     * Processes the "Save Project As" request.
     */
    public void onProjectSaveAs() {
        try {
            File projectFile = showSaveDialog();
            if (projectFile == null) {
                return;
            }

            this.model.save(projectFile);
        } catch (final IOException e) {
            MessageBox.warning(this.stage, "I/O Error",
                    "Error during the operation:\n" + e.getLocalizedMessage());
        }
    }

    /**
     * Processes the "App Exit" request.
     * 
     * @return True if application can be closed, false if operation shall be
     *         aborted.
     */
    public boolean onAppExit() {
        boolean allowClose = true;

        if (this.model.isModified()) {
            if (MessageBox.question(this.stage, "Quit?",
                    "Are you sure you want to quit?\n\nProject changes will be lost.",
                    MessageBoxButton.YES, MessageBoxButton.YES, MessageBoxButton.NO) != MessageBoxButton.YES) {
                allowClose = false;
            }
        }

        return allowClose;
    }

    /**
     * Processes the case when project is marked as modified and is about to be
     * replaced with new contents.
     * 
     * @return True if the requested operation shall be continued, false if it
     *         should be aborted.
     * 
     * @throws IOException
     *             if an I/O error occurred.
     */
    private boolean processSaveModified() throws IOException {
        // if project is modified - save it first
        if (this.model.getModifiedFlag()) {
            final MessageBoxButton selectedButton = MessageBox
                    .question(
                            this.stage,
                            "Save?",
                            "Current project contains changes that were not saved. Do you want to save them first?",
                            MessageBoxButton.YES, MessageBoxButton.YES, MessageBoxButton.NO,
                            MessageBoxButton.CANCEL);

            if (selectedButton == MessageBoxButton.YES) {
                try {
                    File projectFile = this.model.getProjectFile();

                    if (projectFile == null) {
                        projectFile = showSaveDialog();
                        if (projectFile == null) {
                            return false;
                        }
                    }

                    this.model.save(projectFile);
                } catch (final IOException e) {
                    MessageBox.warning(this.stage, "I/O Error",
                            "Error during the operation:\n" + e.getLocalizedMessage());

                    return false;
                }
            } else if (selectedButton == MessageBoxButton.NO) {
                return true;
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Shows open project file dialog.
     * 
     * @return File selected by the user, null if user cancelled the operation.
     */
    private File showOpenDialog() {
        this.projectFileChooser.setTitle("Open Project");

        return this.projectFileChooser.showOpenDialog(this.stage);
    }

    /**
     * Shows save project file dialog.
     * 
     * @return File selected by the user, null if user cancelled the operation.
     */
    private File showSaveDialog() {
        this.projectFileChooser.setTitle("Save Project");

        return this.projectFileChooser.showSaveDialog(this.stage);
    }

}
