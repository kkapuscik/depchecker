package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.engine.EngineManager;
import info.octaedr.depchecker.engine.doxygen.DoxygenInputProcessor;
import info.octaedr.depchecker.engine.simple.SimpleInputProcessor;
import info.octaedr.depchecker.model.DepCheckerModel;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Main class of DepChecker application using JAVAFX.
 */
public class DepCheckerApplication extends Application {

    /**
     * Application start point method.
     * 
     * @param args
     *            Arguments passed to application.
     */
    public static void main(final String[] args) {
        launch(args);
    }

    @Override
    public void start(final Stage stage) throws Exception {
        // create & engine and register its components
        final EngineManager engineManager = new EngineManager();
        engineManager.registerProcessor(new SimpleInputProcessor());
        engineManager.registerProcessor(new DoxygenInputProcessor());

        // create the model
        final DepCheckerModel appModel = new DepCheckerModel(engineManager);
        
        // create the UI
        final MainWindow mainWindow = new MainWindow(stage, appModel);
        
        // start application
        mainWindow.show();
    }
}