package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.engine.InputProcessor;
import info.octaedr.depchecker.engine.filter.DependencyGraphFilter;
import info.octaedr.depchecker.engine.filter.VertexCollapseFilter;
import info.octaedr.depchecker.engine.filter.VertexRemoveFilter;
import info.octaedr.depchecker.engine.graph.DependencyGraph;
import info.octaedr.depchecker.engine.graph.Vertex;
import info.octaedr.depchecker.model.DepCheckerModel;
import info.octaedr.depchecker.model.InputGraph;
import info.octaedr.depchecker.model.InputGraphDescriptor;
import info.octaedr.depchecker.model.InputGraphEntry;
import info.octaedr.depchecker.model.InputGraphLocator;
import info.octaedr.depchecker.model.InputGraphsModel;
import info.octaedr.depchecker.model.InputProcessorEntry;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.tools.Path;
import info.octaedr.depchecker.ui.javafx.common.Controller;
import info.octaedr.depchecker.ui.javafx.common.MessageBox;
import info.octaedr.depchecker.ui.javafx.common.SimpleListCellFactory;

import java.io.File;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class InputProcessorsViewController implements Controller<DepCheckerModel> {

    private class AnalysisThread extends Thread {

        private final ProjectModel projectModel;
        private final File workDirectory;
        private final File outputDirectory;
        private final InputProcessor inputProcessor;
        private final String graphName;

        public AnalysisThread(final ProjectModel projectModel, final File projectDirectory,
                final File outputDirectory, final InputProcessor inputProcessor, final String graphName) {
            this.projectModel = projectModel;
            this.workDirectory = projectDirectory;
            this.outputDirectory = outputDirectory;
            this.inputProcessor = inputProcessor;
            this.graphName = graphName;
        }

        @Override
        public void run() {
            boolean result = false;

            try {
                result = performAnalysis(this.projectModel, this.workDirectory,
                        this.outputDirectory, this.inputProcessor, this.graphName);
            } catch (final Throwable t) {
                // catch everything & notify failure
                t.printStackTrace();
            }

            notifyThreadFinished(result);
        }

        private void notifyThreadFinished(final boolean result) {
            javafx.application.Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    onAnalysisFinished(result);
                }
            });
        }
    }

    
    private DepCheckerModel model;

    private Stage stage;

    private AnalysisThread workThread;

    @FXML
    ComboBox<InputProcessorEntry> inputProcessorCombo;

    @FXML
    TextField graphNameText;
    
    @FXML
    Button runAnalysisButton;

    @Override
    public void initialize() {
        assert this.inputProcessorCombo != null;
        assert this.graphNameText != null;
        assert this.runAnalysisButton != null;
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;

        SimpleListCellFactory<InputProcessorEntry> processorCellFactory = new SimpleListCellFactory<InputProcessorEntry>() {
            @Override
            protected void updateItem(final ListCell<InputProcessorEntry> cell,
                    final InputProcessorEntry item, final boolean empty) {
                if (empty) {
                    cell.textProperty().unbind();
                } else {
                    cell.textProperty().bind(item.nameProperty());
                }
            }
        };

        this.inputProcessorCombo.setCellFactory(processorCellFactory);
        this.inputProcessorCombo.setButtonCell(processorCellFactory.call(null));
    }

    @Override
    public void setModel(final DepCheckerModel model) {
        this.model = model;

        final InputGraphsModel inputGraphsModel = this.model.getInputGraphsModel();
        
        this.inputProcessorCombo.setItems(inputGraphsModel.getInputProcessors());
        if (inputGraphsModel.getInputProcessors().size() > 0) {
            this.inputProcessorCombo.getSelectionModel().select(0);
        }
    }

    @FXML
    void onRunProcessor(final ActionEvent event) {
        final InputProcessorEntry entry = this.inputProcessorCombo.getSelectionModel()
                .getSelectedItem();
        if (entry == null) {
            MessageDialogBox.showMessageDialog(this.stage, "Select input processor.", "Error");
            return;
        }

        String graphName = this.graphNameText.getText();
        if (graphName == null) {
            MessageDialogBox.showMessageDialog(this.stage, "Enter graph name.", "Error");
            return;
        }
        graphName = graphName.trim();
        if (graphName.isEmpty()) {
            MessageDialogBox.showMessageDialog(this.stage, "Enter graph name.", "Error");
            return;
        }

        prepareInputData(entry.getInputProcessor(), graphName);
    }

    private void prepareInputData(final InputProcessor inputProcessor, final String graphName) {
        final ProjectModel projectModel = this.model.getProjectModel();

        final File projectFile = projectModel.getProjectFile();
        if (projectFile == null) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - project file not saved");
            return;
        }
        if (!projectFile.isFile()) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - project path does not represent a file");
            return;
        }

        final File projectDirectory = projectFile.getParentFile();
        if ((projectDirectory == null) || (!projectDirectory.isDirectory())) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - cannot calculate project directory");
            return;
        }

        final String outputPath = projectModel.getOutputDirectory();
        if ((outputPath == null) || (outputPath.isEmpty())) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - output directory not set");
            return;
        }

        final File outputDirectory = Path.resolve(projectDirectory, outputPath);
        if (!outputDirectory.exists()) {
            if (!outputDirectory.mkdir()) {
                MessageBox.warning(this.stage, "Analysis",
                        "Cannot perform analysis - cannot create output directory");
                return;
            }
        }

        if (!outputDirectory.isDirectory()) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - output path is not a directory");
            return;
        }

        startProcessing(inputProcessor, graphName, projectModel, projectDirectory, outputDirectory);
    }

    private void startProcessing(final InputProcessor inputProcessor, final String graphName,
            final ProjectModel projectModel, final File projectDirectory, final File outputDirectory) {
        
        assert this.workThread == null;

        this.runAnalysisButton.setDisable(true);

        this.workThread = new AnalysisThread(projectModel, projectDirectory, outputDirectory, inputProcessor, graphName);
        this.workThread.start();
    }

    private void onAnalysisFinished(final boolean analysisSuccess) {
        if (analysisSuccess) {
            MessageBox.information(this.stage, "Analysis", "Analysis finished with success");
        } else {
            MessageBox.warning(this.stage, "Analysis", "Analysis failed");
        }

        this.workThread = null;
        this.runAnalysisButton.setDisable(false);
    }

    private boolean performAnalysis(final ProjectModel projectModel, final File workDirectory,
            final File outputDirectory, final InputProcessor inputProcessor, final String userName) {

        InputGraph[] graphs = inputProcessor.process(projectModel, workDirectory, outputDirectory,
                this.model.getLogModel().getWriter("Analyzer[" + inputProcessor.getName() + ']'));
        if (graphs == null) {
            return false;
        }

        // TODO: cleanup
        if (graphs.length > 0) {
            filterFsGraph(graphs[0]);
        }
        
        // add to model
        final InputGraphsModel inputGraphsModel = this.model.getInputGraphsModel();
        for (final InputGraph graph : graphs) {
            final String graphName = getGraphName(inputProcessor, graph.getLocator());
            inputGraphsModel.getInputGraphs().add(
                    new InputGraphEntry(graph, userName, graphName, inputProcessor.getName()));
        }

        return true;
    }

    private String getGraphName(InputProcessor inputProcessor, InputGraphLocator locator) {
        for (final InputGraphDescriptor descriptor : inputProcessor.getGraphDescriptors()) {
            if (descriptor.getLocator().equals(locator)) {
                return descriptor.getName();
            }
        }
        return "-unknown-";
    }

    private void filterFsGraph(final InputGraph inputGraph) {
        DependencyGraph fsGraph = inputGraph.getData();

        final ArrayList<DependencyGraphFilter> graphFilters = new ArrayList<>();

        graphFilters.add(new VertexRemoveFilter() {
            @Override
            protected boolean accept(final DependencyGraph graph, final Vertex vertex) {
                return "SDL_config.h".equals(vertex.getName());
            }
        });
        graphFilters.add(new VertexRemoveFilter() {
            @Override
            protected boolean accept(final DependencyGraph graph, final Vertex vertex) {
                return "string.h".equals(vertex.getName())
                        || "stdlib.h".equals(vertex.getName())
                        || "stdio.h".equals(vertex.getName())
                        || "stdarg.h".equals(vertex.getName())
                        || "stdint.h".equals(vertex.getName())
                        || "time.h".equals(vertex.getName())
                        || "errno.h".equals(vertex.getName());
            }
        });
        graphFilters.add(new VertexCollapseFilter() {
            @Override
            protected boolean accept(final DependencyGraph graph, final Vertex vertex) {
                return "SDL2-2.0.1/include".equals(vertex.getName());
            }
        });

        for (final DependencyGraphFilter filter : graphFilters) {
            filter.apply(fsGraph);
        }
    }
}
