package info.octaedr.depchecker.ui.javafx;

import java.io.File;
import java.io.IOException;

import info.octaedr.depchecker.model.DepCheckerModel;
import info.octaedr.depchecker.model.RecentFilesModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;
import info.octaedr.depchecker.ui.javafx.common.MessageBox;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

/**
 * Controller for main window menu bar.
 */
public class MainWindowMenuBarController implements Controller<DepCheckerModel> {

    /**
     * Data model assigned to this controller.
     */
    private DepCheckerModel model;

    /**
     * Stage for which this controller was created.
     */
    private Stage stage;

    /**
     * Project I/O wrapper object.
     */
    private ProjectIOWrapper wrapper;

    /**
     * Open recent file 1 - menu item.
     */
    @FXML
    private MenuItem openRecentItem1;

    /**
     * Open recent file 2 - menu item.
     */
    @FXML
    private MenuItem openRecentItem2;

    /**
     * Open recent file 3 - menu item.
     */
    @FXML
    private MenuItem openRecentItem3;

    /**
     * Open recent file 4 - menu item.
     */
    @FXML
    private MenuItem openRecentItem4;

    /**
     * Open recent file 5 - menu item.
     */
    @FXML
    private MenuItem openRecentItem5;

    /**
     * Open recent file 6 - menu item.
     */
    @FXML
    private MenuItem clearRecentItem;

    /**
     * Array of open recent file menu items.
     */
    private MenuItem[] openRecentItems;

    /**
     * Constructs the controller.
     */
    public MainWindowMenuBarController() {
        // nothing to do
    }

    /**
     * This method is called by the FXMLLoader when initialization is complete
     */
    @Override
    @FXML
    public void initialize() {
        assert this.openRecentItem1 != null;
        assert this.openRecentItem2 != null;
        assert this.openRecentItem3 != null;
        assert this.openRecentItem4 != null;
        assert this.openRecentItem5 != null;
        assert this.clearRecentItem != null;

        this.openRecentItems = new MenuItem[] { this.openRecentItem1, this.openRecentItem2,
                this.openRecentItem3, this.openRecentItem4, this.openRecentItem5 };
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    /**
     * Sets object that wraps project I/O operations.
     * 
     * @param wrapper
     *            Project I/O wrapper object.
     */
    public void setProjectIOWrapper(final ProjectIOWrapper wrapper) {
        this.wrapper = wrapper;
    }

    @Override
    public void setModel(final DepCheckerModel model) {
        this.model = model;

        final RecentFilesModel recentFilesModel = this.model.getRecentFilesModel();
        recentFilesModel.getRecentPaths().addListener(new ListChangeListener<String>() {
            @Override
            public void onChanged(final ListChangeListener.Change<? extends String> change) {
                refreshRecentFilesMenu();
            }
        });
        refreshRecentFilesMenu();
    }

    /**
     * Processes the new project request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    void onFileNew(final ActionEvent event) {
        this.wrapper.onProjectNew();
    }

    /**
     * Processes the open project request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    void onFileOpen(final ActionEvent event) {
        this.wrapper.onProjectOpen();
    }

    /**
     * Processes the save project request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    void onFileSave(final ActionEvent event) {
        this.wrapper.onProjectSave();
    }

    /**
     * Processes the save project as request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    void onFileSaveAs(final ActionEvent event) {
        this.wrapper.onProjectSaveAs();
    }

    /**
     * Processes the preferences request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    protected void onFilePreferences(final ActionEvent event) {
        PreferencesDialog preferencesDialog = new PreferencesDialog(this.stage,
                this.model.getPreferencesDataModel());
        preferencesDialog.show();
    }

    /**
     * Processes the exit application request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    protected void onFileExit(final ActionEvent event) {
        this.stage.close();
    }

    /**
     * Processes the show about information request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    protected void onHelpAbout(final ActionEvent event) {
        AboutDialog aboutDialog = new AboutDialog(this.stage, this.model.getAboutDataModel());
        aboutDialog.show();
    }

    /**
     * Refreshes recent files menu.
     */
    protected void refreshRecentFilesMenu() {
        final ObservableList<String> recentList = this.model.getRecentFilesModel().getRecentPaths();

        int itemIndex = 0;

        for (; (itemIndex < recentList.size()) && (itemIndex < this.openRecentItems.length); ++itemIndex) {
            this.openRecentItems[itemIndex].setText(recentList.get(itemIndex));
            this.openRecentItems[itemIndex].setVisible(true);
        }

        for (; itemIndex < this.openRecentItems.length; ++itemIndex) {
            this.openRecentItems[itemIndex].setVisible(false);
        }
    }

    /**
     * Processes the recent list clear request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    protected void onClearRecent(final ActionEvent event) {
        final ObservableList<String> recentList = this.model.getRecentFilesModel().getRecentPaths();

        recentList.clear();
    }

    /**
     * Processes the open recent request.
     * 
     * @param event
     *            Event descriptor.
     * @param index
     *            The recent file index.
     */
    private void onOpenRecent(final ActionEvent event, final int index) {
        final ObservableList<String> recentList = this.model.getRecentFilesModel().getRecentPaths();

        if ((index >= 0) && (index < recentList.size())) {
            final String recentFilePath = recentList.get(index);

            try {
                this.model.getProjectModel().load(new File(recentFilePath));
            } catch (final IOException e) {
                MessageBox.warning(this.stage, "I/O Error",
                        "Error during the operation:\n" + e.getLocalizedMessage());
            }
        }
    }

    /**
     * Processes the open recent file 1 request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    protected void onOpenRecent1(final ActionEvent event) {
        onOpenRecent(event, 0);
    }

    /**
     * Processes the open recent file 1 request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    protected void onOpenRecent2(final ActionEvent event) {
        onOpenRecent(event, 1);
    }

    /**
     * Processes the open recent file 1 request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    protected void onOpenRecent3(final ActionEvent event) {
        onOpenRecent(event, 2);
    }

    /**
     * Processes the open recent file 1 request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    protected void onOpenRecent4(final ActionEvent event) {
        onOpenRecent(event, 3);
    }

    /**
     * Processes the open recent file 1 request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    protected void onOpenRecent5(final ActionEvent event) {
        onOpenRecent(event, 4);
    }

}
