package info.octaedr.depchecker.ui.javafx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import info.octaedr.depchecker.model.DepCheckerModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;

/**
 * Controller for the main window tool bar.
 */
public class MainWindowToolBarController implements Controller<DepCheckerModel> {

    /**
     * Project I/O wrapper object.
     */
    private ProjectIOWrapper wrapper;

    /**
     * Constructs the controller.
     */
    public MainWindowToolBarController() {
        // nothing to do
    }

    @Override
    @FXML
    public void initialize() {
        // nothing to do
    }

    @Override
    public void setStage(Stage stage) {
        // nothing to do
    }

    @Override
    public void setModel(DepCheckerModel model) {
        // nothing to do
    }

    /**
     * Sets object that wraps project I/O operations.
     * 
     * @param wrapper
     *            Project I/O wrapper object.
     */
    public void setProjectIOWrapper(final ProjectIOWrapper wrapper) {
        this.wrapper = wrapper;
    }

    /**
     * Processes the open project request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    void onFileOpen(final ActionEvent event) {
        this.wrapper.onProjectOpen();
    }

    /**
     * Processes the save project request.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    void onFileSave(final ActionEvent event) {
        this.wrapper.onProjectSave();
    }

}
