package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.engine.DotGraphSerializer;
import info.octaedr.depchecker.engine.InputProcessor;
import info.octaedr.depchecker.engine.doxygen.DoxygenInputProcessor;
import info.octaedr.depchecker.engine.filter.DependencyGraphFilter;
import info.octaedr.depchecker.engine.filter.VertexCollapseFilter;
import info.octaedr.depchecker.engine.filter.VertexRemoveFilter;
import info.octaedr.depchecker.engine.graph.DependencyGraph;
import info.octaedr.depchecker.engine.graph.RelationEdge;
import info.octaedr.depchecker.engine.graph.Vertex;
import info.octaedr.depchecker.engine.simple.SimpleInputProcessor;
import info.octaedr.depchecker.model.DepCheckerModel;
import info.octaedr.depchecker.model.InputGraph;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.tools.Path;
import info.octaedr.depchecker.ui.javafx.common.Controller;
import info.octaedr.depchecker.ui.javafx.common.MessageBox;
import info.octaedr.depchecker.ui.javafx.common.MessageBoxButton;
import info.octaedr.graphviz.GraphvizExecutor;
import info.octaedr.graphviz.LayoutCommand;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import javax.swing.JFrame;

import org.jgrapht.ext.JGraphXAdapter;

import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.layout.mxOrganicLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxConstants;

/**
 * Controller for the project editor control.
 */
public class AnalyzerViewController implements Controller<DepCheckerModel> {

    private class AnalysisThread extends Thread {

        private final ProjectModel projectModel;
        private final File workDirectory;
        private final File outputDirectory;
        private final InputProcessor inputProcessor;

        public AnalysisThread(final ProjectModel projectModel, final File projectDirectory,
                final File outputDirectory, final InputProcessor inputProcessor) {
            this.projectModel = projectModel;
            this.workDirectory = projectDirectory;
            this.outputDirectory = outputDirectory;
            this.inputProcessor = inputProcessor;
        }

        @Override
        public void run() {
            boolean result = false;

            try {
                result = performAnalysis(this.projectModel, this.workDirectory,
                        this.outputDirectory, this.inputProcessor);
            } catch (final Throwable t) {
                // catch everything & notify failure
                t.printStackTrace();
            }

            notifyThreadFinished(result);
        }

        private void notifyThreadFinished(final boolean result) {
            javafx.application.Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    onAnalysisFinished(result);
                }
            });
        }
    }

    @FXML
    private Button runAnalysis;

    private DepCheckerModel model;
    private Stage stage;
    private Thread workThread;

    /**
     * Constructs the controller.
     */
    public AnalyzerViewController() {
        // nothing to do
    }

    @Override
    @FXML
    public void initialize() {
        // nothing to do
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setModel(final DepCheckerModel model) {
        this.model = model;
    }

    /**
     * Performs the analysis.
     * 
     * @param event
     *            UI event descriptor.
     */
    @FXML
    void onRunAnalysis(final ActionEvent event) {
        if (isThreadStarted()) {
            MessageBox.warning(this.stage, "Analysis", "Analysis already started");
            return;
        }

        final ProjectModel projectModel = this.model.getProjectModel();

        final File projectFile = projectModel.getProjectFile();
        if (projectFile == null) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - project file not saved");
            return;
        }
        if (!projectFile.isFile()) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - project path does not represent a file");
            return;
        }

        final File projectDirectory = projectFile.getParentFile();
        if ((projectDirectory == null) || (!projectDirectory.isDirectory())) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - cannot calculate project directory");
            return;
        }

        final String outputPath = projectModel.getOutputDirectory();
        if ((outputPath == null) || (outputPath.isEmpty())) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - output directory not set");
            return;
        }

        final File outputDirectory = Path.resolve(projectDirectory, outputPath);
        if (!outputDirectory.exists()) {
            if (!outputDirectory.mkdir()) {
                MessageBox.warning(this.stage, "Analysis",
                        "Cannot perform analysis - cannot create output directory");
                return;
            }
        }

        if (!outputDirectory.isDirectory()) {
            MessageBox.warning(this.stage, "Analysis",
                    "Cannot perform analysis - output path is not a directory");
            return;
        }

        startAnalysis(projectModel, projectDirectory, outputDirectory);
    }

    private void startAnalysis(final ProjectModel projectModel, final File workDirectory,
            final File outputDirectory) {
        InputProcessor inputProcessor = null;

        final MessageBoxButton doxygenButton = new MessageBoxButton("Doxygen");
        final MessageBoxButton simpleButton = new MessageBoxButton("Simple");

        final MessageBoxButton selectedButton = MessageBox.question(null, "Select Input Processor",
                "Select which input processor to use.", simpleButton, simpleButton, doxygenButton);
        if (doxygenButton.equals(selectedButton)) {
            inputProcessor = new DoxygenInputProcessor();
        } else if (simpleButton.equals(selectedButton)) {
            inputProcessor = new SimpleInputProcessor();
        }
        
        if (inputProcessor == null) {
            return;
        }
        
        assert this.workThread == null;

        this.runAnalysis.setDisable(true);

        this.workThread = new AnalysisThread(projectModel, workDirectory, outputDirectory, inputProcessor);
        this.workThread.start();
    }

    private void onAnalysisFinished(final boolean analysisSuccess) {
        if (analysisSuccess) {
            MessageBox.information(this.stage, "Analysis", "Analysis finished with success");
        } else {
            MessageBox.warning(this.stage, "Analysis", "Analysis failed");
        }

        this.workThread = null;
        this.runAnalysis.setDisable(false);
    }

    private boolean performAnalysis(final ProjectModel projectModel, final File workDirectory,
            final File outputDirectory, final InputProcessor inputProcessor) {

        InputGraph[] graphs = inputProcessor.process(projectModel, workDirectory, outputDirectory,
                this.model.getLogModel().getWriter("Analyzer[" + inputProcessor.getName() + ']'));
        if (graphs == null) {
            return false;
        }

        return postProcessData(outputDirectory, graphs);
    }

    private boolean isThreadStarted() {
        return this.workThread != null;
    }

    private boolean postProcessData(final File outputDirectory, final InputGraph[] graphs) {
        try {
            DependencyGraph fsGraph = graphs[0].getData();

            final ArrayList<DependencyGraphFilter> graphFilters = new ArrayList<>();

            graphFilters.add(new VertexRemoveFilter() {
                @Override
                protected boolean accept(final DependencyGraph graph, final Vertex vertex) {
                    return "SDL_config.h".equals(vertex.getName());
                }
            });
            graphFilters.add(new VertexRemoveFilter() {
                @Override
                protected boolean accept(final DependencyGraph graph, final Vertex vertex) {
                    return "string.h".equals(vertex.getName())
                            || "stdlib.h".equals(vertex.getName())
                            || "stdio.h".equals(vertex.getName())
                            || "stdarg.h".equals(vertex.getName())
                            || "stdint.h".equals(vertex.getName())
                            || "time.h".equals(vertex.getName())
                            || "errno.h".equals(vertex.getName());
                }
            });
            graphFilters.add(new VertexCollapseFilter() {
                @Override
                protected boolean accept(final DependencyGraph graph, final Vertex vertex) {
                    return "SDL2-2.0.1/include".equals(vertex.getName());
                }
            });

            for (final DependencyGraphFilter filter : graphFilters) {
                filter.apply(fsGraph);
            }

            /* prepare graph serializer */
            final DotGraphSerializer dotGraphSerializer = new DotGraphSerializer();

            /* serialize file system graph to standard output */
            // if (!dotGraphSerializer.serialize(fsGraph, System.out)) {
            // throw new IOException("Cannot store FS graph using console");
            // }

            /* serialize file system graph to output file */
            final File fsGraphDotFile = new File(outputDirectory, "graph-fs.dot");
            if (!dotGraphSerializer.serialize(fsGraph, fsGraphDotFile)) {
                throw new IOException("Cannot store FS graph in file");
            }

            /* run graphviz tool to generate graph */
            final GraphvizExecutor fdpExecutor = new GraphvizExecutor(LayoutCommand.FDP);
            fdpExecutor.setLogWriter(this.model.getLogModel().getWriter("Graphviz"));
            fdpExecutor.setWorkingDirectory(outputDirectory);
            fdpExecutor.setInputFile(fsGraphDotFile);
            fdpExecutor.setOutputFormat("svg");
            if (!fdpExecutor.run()) {
                System.err.println("Graphviz execution failed");
            }

            final boolean DO_NOT_EXECUTE = true;
            if (DO_NOT_EXECUTE) {
                showVisualizationWindow(fsGraph);
            }

            return true;
        } catch (final Throwable t) {
            t.printStackTrace();

            return false;
        }
    }

    private void showVisualizationWindow(final DependencyGraph graph) {
        /* visualization stuff */
        final JGraphXAdapter<Vertex, RelationEdge> graphAdapter = new JGraphXAdapter<>(
                graph.getRelationGraph());
        graphAdapter.setAllowDanglingEdges(false);
        graphAdapter.setCellsDisconnectable(false);
        graphAdapter.setCellsEditable(false);
        graphAdapter.setSplitEnabled(false);
        graphAdapter.setDropEnabled(false);
        graphAdapter.getStylesheet().getDefaultEdgeStyle().put(mxConstants.STYLE_NOLABEL, "1");

        final mxGraphComponent graphComponent = new mxGraphComponent(graphAdapter);
        graphComponent.setConnectable(false);

        // final mxIGraphLayout layout = new mxHierarchicalLayout(graphAdapter);
        // final mxIGraphLayout layout = new mxFastOrganicLayout(graphAdapter);
        // final mxIGraphLayout layout = new mxOrthogonalLayout(graphAdapter);
        final mxIGraphLayout layout = new mxOrganicLayout(graphAdapter);
        layout.execute(graphAdapter.getDefaultParent());

        final JFrame visualizationFrame = new JFrame("Graph visualization");
        visualizationFrame.setLayout(new BorderLayout());
        visualizationFrame.setSize(800, 600);
        visualizationFrame.getContentPane().add(graphComponent, BorderLayout.CENTER);
        visualizationFrame.setVisible(true);
        visualizationFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

}
