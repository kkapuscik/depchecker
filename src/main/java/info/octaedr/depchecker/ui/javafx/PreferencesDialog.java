package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.model.PreferencesDataModel;
import info.octaedr.depchecker.ui.javafx.common.AppDialog;
import javafx.stage.Modality;
import javafx.stage.Window;

/**
 * Preferences dialog.
 * 
 * <p>
 * Preferences dialog gives the user ability to change configuration
 * of the DepChecker application
 */
public class PreferencesDialog extends AppDialog {

    /**
     * Constructs the dialog.
     * 
     * @param owner
     *            Dialog owner window.
     * @param model
     *            Model to use.
     */
    public PreferencesDialog(final Window owner, final PreferencesDataModel model) {
        super(owner, Modality.APPLICATION_MODAL);

        createStage();
        setFxmlLocation(getClass().getResource("PreferencesDialog.fxml"));
        setTitle("Preferences");

        initialize(model);
    }

}
