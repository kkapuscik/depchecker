package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.model.DepCheckerModel;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringExpression;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

/**
 * Controller for DepCheckerFrame.
 */
public class MainWindowController2 implements Controller<DepCheckerModel> {

    private abstract class ModelTreeItemData {

        public abstract ObservableValue<String> getText();

        public ObservableValue<Node> getIcon() {
            return null;
        }

        public void onDoubleClick() {
            // do nothing
        }

    }

    private class SimpleModelTreeItemData extends ModelTreeItemData {

        private final ObservableValue<String> textValue;

        public SimpleModelTreeItemData(final ObservableValue<String> textValue) {
            this.textValue = textValue;
        }

        public SimpleModelTreeItemData(final String textValue) {
            this.textValue = new SimpleStringProperty(textValue);
        }

        @Override
        public ObservableValue<String> getText() {
            return this.textValue;
        }

    }

    private class ShowNodeModelTreeItemData extends SimpleModelTreeItemData {

        private final Node editorNode;

        public ShowNodeModelTreeItemData(final ObservableValue<String> textValue,
                final Node editorNode) {
            super(textValue);
            this.editorNode = editorNode;
        }

        public ShowNodeModelTreeItemData(final String textValue, final Node editorNode) {
            super(textValue);
            this.editorNode = editorNode;
        }

        @Override
        public void onDoubleClick() {
            switchContentElement(this.editorNode);
        }
    }

    private Node currentContentNode;

    private void switchContentElement(final Node newContentNode) {
        if (this.currentContentNode != null) {
            this.currentContentNode.setVisible(false);
            this.currentContentNode = null;
        }

        if (newContentNode != null) {
            this.currentContentNode = newContentNode;
            this.currentContentNode.toFront();
            this.currentContentNode.setVisible(true);
        }
    }

    private static class ModelTreeCell extends TreeCell<ModelTreeItemData> {
        @Override
        protected void updateItem(final ModelTreeItemData item, final boolean empty) {
            super.updateItem(item, empty);
            if (empty) {
                setUserData(null);
                textProperty().unbind();
                graphicProperty().unbind();
            } else {
                setUserData(item);

                textProperty().bind(item.getText());

                if (item.getIcon() != null) {
                    graphicProperty().bind(item.getIcon());
                }
            }
        }
    }

    /**
     * ResourceBundle that was given to the FXMLLoader
     */
    @FXML
    private ResourceBundle resources;

    /**
     * URL location of the FXML file that was given to the FXMLLoader
     */
    @FXML
    private URL location;

    /**
     * fx:id="mainPane"
     */
    @FXML
    private BorderPane mainPane;

    /**
     * fx:id="menuBar" controller
     */
    @FXML
    private MainWindowMenuBarController menuBarController;

    /**
     * fx:id="toolBar" controller
     */
    @FXML
    private MainWindowToolBarController toolBarController;

    /**
     * fx:id="logView" controller
     */
    @FXML
    private LogViewController logViewController;

    @FXML
    private Node projectInfoEditor;

    @FXML
    private ProjectInfoEditorController projectInfoEditorController;

    @FXML
    private Node projectInputEditor;

    @FXML
    private ProjectInputEditorController projectInputEditorController;

    @FXML
    private Node projectOutputEditor;

    @FXML
    private ProjectOutputEditorController projectOutputEditorController;

    @FXML
    private Node projectPreprocessorEditor;

    @FXML
    private ProjectPreprocessorEditorController projectPreprocessorEditorController;

    @FXML
    private Node projectDoxygenEditor;

    @FXML
    private ProjectDoxygenEditorController projectDoxygenEditorController;

    @FXML
    private Node analyzerView;

    @FXML
    private AnalyzerViewController analyzerViewController;

    @FXML
    private Node inputProcessorsView;

    @FXML
    private InputProcessorsViewController inputProcessorsViewController;

    @FXML
    private Node inputGraphsView;
    
    @FXML
    private InputGraphsViewController inputGraphsViewController;

    @FXML
    private TreeView<ModelTreeItemData> modelTree;

    /**
     * Application data model.
     */
    private DepCheckerModel model;

    /**
     * Title string binding.
     */
    private StringExpression titleBinding;

    /**
     * Stage for which this controller was created.
     */
    private Stage stage;

    /**
     * Wrapper over project I/O operations.
     */
    private final ProjectIOWrapper projectIOWrapper;

    /**
     * Constructs the controller.
     */
    public MainWindowController2() {
        this.projectIOWrapper = new ProjectIOWrapper();
    }

    /**
     * This method is called by the FXMLLoader when initialization is complete
     */
    @Override
    @FXML
    public void initialize() {
        assert this.menuBarController != null;
        assert this.toolBarController != null;

        assert this.logViewController != null;

        assert this.projectInfoEditorController != null;
        assert this.projectInputEditorController != null;
        assert this.projectOutputEditorController != null;
        assert this.projectPreprocessorEditorController != null;
        assert this.projectDoxygenEditorController != null;

        assert this.analyzerViewController != null;
        assert this.inputGraphsViewController != null;
        assert this.inputProcessorsViewController != null;

        assert this.modelTree != null;

        this.projectIOWrapper.initialize();
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
        this.projectIOWrapper.setStage(stage);

        this.menuBarController.setStage(stage);
        this.toolBarController.setStage(stage);

        this.logViewController.setStage(stage);

        this.projectInfoEditorController.setStage(stage);
        this.projectInputEditorController.setStage(stage);
        this.projectOutputEditorController.setStage(stage);
        this.projectPreprocessorEditorController.setStage(stage);
        this.projectDoxygenEditorController.setStage(stage);

        this.analyzerViewController.setStage(stage);
        this.inputGraphsViewController.setStage(stage);
        this.inputProcessorsViewController.setStage(stage);

        this.stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(final WindowEvent e) {
                if (!MainWindowController2.this.projectIOWrapper.onAppExit()) {
                    // consume so it is not continued
                    e.consume();
                }
            }
        });

        this.modelTree.setEditable(false);
        this.modelTree
                .setCellFactory(new Callback<TreeView<ModelTreeItemData>, TreeCell<ModelTreeItemData>>() {
                    @Override
                    public TreeCell<ModelTreeItemData> call(TreeView<ModelTreeItemData> p) {
                        return new ModelTreeCell();
                    }
                });
    }

    @Override
    public void setModel(final DepCheckerModel model) {
        this.model = model;

        final ProjectModel projectModel = this.model.getProjectModel();

        this.projectIOWrapper.setModel(projectModel);

        this.menuBarController.setModel(this.model);
        this.menuBarController.setProjectIOWrapper(this.projectIOWrapper);

        this.toolBarController.setModel(this.model);
        this.toolBarController.setProjectIOWrapper(this.projectIOWrapper);

        this.logViewController.setModel(this.model.getLogModel());

        this.projectInfoEditorController.setModel(projectModel);
        this.projectInputEditorController.setModel(projectModel);
        this.projectOutputEditorController.setModel(projectModel);
        this.projectPreprocessorEditorController.setModel(projectModel);
        this.projectDoxygenEditorController.setModel(projectModel);

        this.analyzerViewController.setModel(this.model);

        this.inputGraphsViewController.setModel(this.model.getInputGraphsModel());
        this.inputProcessorsViewController.setModel(this.model);

        this.titleBinding = Bindings.concat("DepChecker - ",
                Bindings.when(projectModel.modifiedFlagProperty()).then("* ").otherwise(""),
                projectModel.projectFileProperty());

        this.stage.titleProperty().bind(this.titleBinding);

        buildModelTree();
    }

    private void buildModelTree() {
        this.modelTree.setRoot(createProjectTreeItem());
    }

    private TreeItem<ModelTreeItemData> createProjectTreeItem() {
        final SimpleModelTreeItemData projectItemData = new ShowNodeModelTreeItemData(
                Bindings.concat("Project: ", this.model.getProjectModel().getProjectName()),
                this.projectInfoEditor);
        final TreeItem<ModelTreeItemData> projectItem = new TreeItem<ModelTreeItemData>(
                projectItemData);

        projectItem.getChildren().add(createProjectSettingsTreeItem());
        projectItem.getChildren().add(createAnalyzerTreeItem());
        projectItem.getChildren().add(createInputProcessorsTreeItem());
        projectItem.getChildren().add(createInputGraphsTreeItem());

        return projectItem;
    }

    private TreeItem<ModelTreeItemData> createProjectSettingsTreeItem() {
        final SimpleModelTreeItemData settingsItemData = new ShowNodeModelTreeItemData("Settings",
                this.projectInfoEditor);
        final TreeItem<ModelTreeItemData> settingsItem = new TreeItem<ModelTreeItemData>(
                settingsItemData);

        settingsItem.getChildren().add(createProjectEditorTreeItem("Info", this.projectInfoEditor));
        settingsItem.getChildren().add(
                createProjectEditorTreeItem("Input", this.projectInputEditor));
        settingsItem.getChildren().add(
                createProjectEditorTreeItem("Output", this.projectOutputEditor));
        settingsItem.getChildren().add(
                createProjectEditorTreeItem("Preprocessor", this.projectPreprocessorEditor));
        settingsItem.getChildren().add(
                createProjectEditorTreeItem("Doxygen", this.projectDoxygenEditor));

        return settingsItem;
    }

    private TreeItem<ModelTreeItemData> createProjectEditorTreeItem(final String title,
            final Node editorNode) {
        final ShowNodeModelTreeItemData editorItemData = new ShowNodeModelTreeItemData(title,
                editorNode);
        final TreeItem<ModelTreeItemData> editorItem = new TreeItem<ModelTreeItemData>(
                editorItemData);

        return editorItem;
    }

    private TreeItem<ModelTreeItemData> createAnalyzerTreeItem() {
        final ShowNodeModelTreeItemData itemData = new ShowNodeModelTreeItemData("Analyzers",
                this.analyzerView);
        final TreeItem<ModelTreeItemData> viewItem = new TreeItem<ModelTreeItemData>(itemData);

        return viewItem;
    }

    private TreeItem<ModelTreeItemData> createInputProcessorsTreeItem() {
        final ShowNodeModelTreeItemData itemData = new ShowNodeModelTreeItemData(
                "Input Processors", this.inputProcessorsView);
        final TreeItem<ModelTreeItemData> viewItem = new TreeItem<ModelTreeItemData>(itemData);

        return viewItem;
    }

    private TreeItem<ModelTreeItemData> createInputGraphsTreeItem() {
        final ShowNodeModelTreeItemData itemData = new ShowNodeModelTreeItemData(
                "Input Graphs", this.inputGraphsView);
        final TreeItem<ModelTreeItemData> viewItem = new TreeItem<ModelTreeItemData>(itemData);

        return viewItem;
    }

    @FXML
    private void onTreeMouseClicked(final MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() > 0) {
            final TreeItem<ModelTreeItemData> item = this.modelTree.getSelectionModel()
                    .getSelectedItem();

            if (item != null) {
                item.getValue().onDoubleClick();
            }
        }

    }

}
