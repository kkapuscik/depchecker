package info.octaedr.depchecker.ui.javafx.common;

/**
 * Base class for application main windows.
 */
public class AppMainWindow extends AppWindow {

    /**
     * Constructs main window.
     */
    protected AppMainWindow() {
        // nothing to do
    }

}
