package info.octaedr.depchecker.ui.javafx;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;

public class ProjectInfoEditorController implements Controller<ProjectModel> {

    @FXML
    private TextField projectName;
    
    public ProjectInfoEditorController() {
        // nothing to do
    }
    
    @Override
    @FXML
    public void initialize() {
        // nothing to do
    }

    @Override
    public void setStage(final Stage stage) {
        // nothing to do
    }

    @Override
    public void setModel(final ProjectModel model) {
        this.projectName.textProperty().bindBidirectional(model.projectNameProperty());
    }

}
