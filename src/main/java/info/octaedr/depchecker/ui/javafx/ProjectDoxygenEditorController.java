package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

public class ProjectDoxygenEditorController implements Controller<ProjectModel> {

    @FXML
    private CheckBox builtinStlSupport;
    
    public ProjectDoxygenEditorController() {
        // nothing to do
    }
    
    @Override
    @FXML
    public void initialize() {
        // nothing to do
    }

    @Override
    public void setStage(final Stage stage) {
        // nothing to do
    }

    @Override
    public void setModel(final ProjectModel model) {
        this.builtinStlSupport.selectedProperty().bindBidirectional(model.builtinStlSupportProperty());
    }

}
