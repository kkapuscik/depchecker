package info.octaedr.depchecker.ui.javafx.common;

import javafx.fxml.FXML;
import javafx.stage.Stage;

/**
 * Interface the JavaFX scene contents controllers.
 * 
 * @param <ModelType>
 *            Type of the data model.
 */
public interface Controller<ModelType> {

    /**
     * This method is called by the FXMLLoader when initialization is complete
     */
    @FXML
    void initialize();

    /**
     * Sets the stage within which the controller will be used.
     * 
     * @param stage
     *            Stage object.
     */
    void setStage(Stage stage);

    /**
     * Sets the model to be used.
     * 
     * @param model
     *            Model object.
     */
    void setModel(ModelType model);

}
