package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.tools.Path;
import info.octaedr.depchecker.ui.javafx.common.Controller;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class ProjectOutputEditorController implements Controller<ProjectModel> {

    @FXML
    private TextField outputDirectory;

    @FXML
    private Button outputDirectoryBrowse;
     
    private ProjectModel model;

    private Stage stage;

    /**
     * Directory chooser dialog.
     */
    private DirectoryChooser directoryChooser;

    /**
     * Constructs the controller.
     */
    public ProjectOutputEditorController() {
        // nothing to do
    }

    @Override
    @FXML
    public void initialize() {
        this.directoryChooser = new DirectoryChooser();
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setModel(final ProjectModel model) {
        this.model = model;

        this.outputDirectory.textProperty().bindBidirectional(this.model.outputDirectoryProperty());

        this.outputDirectoryBrowse.disableProperty().bind(
                this.model.projectFileProperty().isNull());
    }

    @FXML
    void onOutputDirBrowse(final ActionEvent event) {
        if (this.model.getProjectFile() != null) {
            this.directoryChooser.setTitle("Select output directory");
            this.directoryChooser.setInitialDirectory(this.model.getProjectFile().getParentFile());
            final File chosenDir = this.directoryChooser.showDialog(this.stage);
            if (chosenDir != null) {
                this.model.setOutputDirectory(Path.relativize(this.model.getProjectFile(), chosenDir));
            }
        }
    }

}
