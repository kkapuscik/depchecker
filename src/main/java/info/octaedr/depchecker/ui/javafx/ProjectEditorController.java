package info.octaedr.depchecker.ui.javafx;

import javafx.fxml.FXML;
import javafx.stage.Stage;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;

/**
 * Controller for the project editor control.
 */
public class ProjectEditorController implements Controller<ProjectModel> {

    /**
     * Project info editor controller.
     */
    @FXML
    private ProjectInfoEditorController infoEditorController;

    /**
     * Project input settings editor controller.
     */
    @FXML
    private ProjectInputEditorController inputEditorController;

    /**
     * Project output settings editor controller.
     */
    @FXML
    private ProjectOutputEditorController outputEditorController;

    /**
     * Project preprocessor settings editor controller.
     */
    @FXML
    private ProjectPreprocessorEditorController preprocessorEditorController;

    /**
     * Project doxygen settings editor controller.
     */
    @FXML
    private ProjectDoxygenEditorController doxygenEditorController;

    /**
     * Constructs the controller.
     */
    public ProjectEditorController() {
        // nothing to do
    }

    @Override
    @FXML
    public void initialize() {
        assert this.infoEditorController != null;
        assert this.inputEditorController != null;
        assert this.outputEditorController != null;
        assert this.preprocessorEditorController != null;
        assert this.doxygenEditorController != null;
    }

    @Override
    public void setStage(final Stage stage) {
        this.infoEditorController.setStage(stage);
        this.inputEditorController.setStage(stage);
        this.outputEditorController.setStage(stage);
        this.preprocessorEditorController.setStage(stage);
        this.doxygenEditorController.setStage(stage);
    }

    @Override
    public void setModel(final ProjectModel model) {
        this.infoEditorController.setModel(model);
        this.inputEditorController.setModel(model);
        this.outputEditorController.setModel(model);
        this.preprocessorEditorController.setModel(model);
        this.doxygenEditorController.setModel(model);
    }

}
