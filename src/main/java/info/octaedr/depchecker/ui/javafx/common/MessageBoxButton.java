package info.octaedr.depchecker.ui.javafx.common;

/**
 * Descriptor of the message box.
 * 
 * <p>
 * There is a set of predefined button descriptors used for built-in buttons.
 * These descriptors could be used for direct object comparison.
 * 
 * <p>
 * The predefined {@link #NULL} button is used for buttons 
 */
public class MessageBoxButton {

    /**
     * Default message button - Null.
     */
    public static final MessageBoxButton NULL = new MessageBoxButton(true, null);

    /**
     * Default message button - OK.
     */
    public static final MessageBoxButton OK = new MessageBoxButton(true, "OK");

    /**
     * Default message button - Cancel.
     */
    public static final MessageBoxButton CANCEL = new MessageBoxButton(true, "Cancel");
    
    /**
     * Default message button - Yes.
     */
    public static final MessageBoxButton YES = new MessageBoxButton(true, "Yes");
    
    /**
     * Default message button - No.
     */
    public static final MessageBoxButton NO = new MessageBoxButton(true, "No");

    /**
     * Default message button - Close.
     */
    public static final MessageBoxButton CLOSE = new MessageBoxButton(true, "Close");

    /**
     * Button text.
     */
    private String text;

    /**
     * Constructs the user-defined button.
     * 
     * @param text
     *            Button text.
     */
    public MessageBoxButton(final String text) {
        this(false, text);
    }

    /**
     * Constructor that handles the builtin flag.
     * 
     * <p>
     * Internal implementation use only.
     * 
     * @param isBuiltin
     *            Flag indicating this is the built-in button.
     * @param text
     *            Button text.
     */
    private MessageBoxButton(final boolean isBuiltin, final String text) {
        if ((text == null) && !isBuiltin) {
            throw new NullPointerException("Null text given for non-builtin button");
        }
        this.text = text;
    }

    /**
     * Returns button text.
     * 
     * @return
     * Button text.
     */
    public String getText() {
        return this.text != null ? this.text : "";
    }

    /**
     * Checks if this instance represents NULL button.
     * 
     * @return
     * True if it is a NULL button, false otherwise.
     */
    public boolean isNull() {
        return this.text == null;
    }
}
