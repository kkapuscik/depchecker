package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.model.AboutDataModel;
import info.octaedr.depchecker.model.ThirdPartyInfo;
import info.octaedr.depchecker.ui.javafx.common.Controller;
import info.octaedr.depchecker.ui.javafx.common.SimpleListCellFactory;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Controller for AboutDialog.
 **/
public class AboutDialogController implements Controller<AboutDataModel> {

    /**
     * ResourceBundle that was given to the FXMLLoader
     */
    @FXML
    private ResourceBundle resources;

    /**
     * URL location of the FXML file that was given to the FXMLLoader
     */
    @FXML
    private URL location;

    /**
     * fx:id="appNameLabel"
     */
    @FXML
    private Label appNameLabel;

    /**
     * fx:id="appVersionLabel"
     */
    @FXML
    private Label appVersionLabel;

    /**
     * fx:id="creditsTextArea"
     */
    @FXML
    private TextArea creditsTextArea;

    /**
     * fx:id="dialogPane"
     */
    @FXML
    private BorderPane dialogPane;

    /**
     * fx:id="licenseTextArea"
     */
    @FXML
    private TextArea licenseTextArea;

    /**
     * fx:id="partyListView"
     */
    @FXML
    private ListView<ThirdPartyInfo> partyListView;

    /**
     * fx:id="partyTextArea"
     */
    @FXML
    private TextArea partyTextArea;

    /**
     * Data model.
     */
    private AboutDataModel model;

    /**
     * Stage for which this controller was created.
     */
    private Stage stage;

    /**
     * Constructs the controller.
     */
    public AboutDialogController() {
        // nothing to do
    }

    /**
     * This method is called by the FXMLLoader when initialization is complete
     */
    @Override
    @FXML
    public void initialize() {
        // prepare cell factory
        this.partyListView.setCellFactory(new SimpleListCellFactory<ThirdPartyInfo>() {
            @Override
            protected void updateItem(final ListCell<ThirdPartyInfo> cell,
                    final ThirdPartyInfo item, final boolean empty) {
                if (empty) {
                    cell.textProperty().unbind();
                } else {
                    cell.textProperty().bind(item.nameProperty());
                }
            }
        });

        // make sure single selection is enabled
        this.partyListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setModel(final AboutDataModel model) {
        this.model = model;

        // bind the model
        this.appNameLabel.textProperty().bind(this.model.appNameProperty());
        this.appVersionLabel.textProperty().bind(this.model.appVersionProperty());
        this.creditsTextArea.textProperty().bind(this.model.creditsProperty());
        this.licenseTextArea.textProperty().bind(this.model.licenseProperty());
        this.partyListView.itemsProperty().bind(this.model.thirdPartyInfosProperty());

        this.partyTextArea.textProperty().bind(
                Bindings.selectString(
                        this.partyListView.getSelectionModel().selectedItemProperty(), "license"));
    }

    /**
     * Processes the close dialog event.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    void closeDialog(final ActionEvent event) {
        this.stage.close();
    }

}
