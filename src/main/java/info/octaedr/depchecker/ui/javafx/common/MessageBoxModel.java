package info.octaedr.depchecker.ui.javafx.common;

/**
 * Model of the message box data.
 */
public class MessageBoxModel {

    /**
     * Maximum number of buttons.
     */
    private static final int MAX_BUTTON_COUNT = 4;

    /**
     * Message box title.
     */
    private String title;

    /**
     * The text to be presented in message box.
     */
    private String message;

    /**
     * Buttons to be placed in message box.
     */
    private final MessageBoxButton buttons[];

    /**
     * Default button.
     */
    private MessageBoxButton defaultButton;

    /**
     * Button selected by the user.
     */
    private MessageBoxButton selectedButton;

    /**
     * Constructs message box.
     */
    public MessageBoxModel() {
        this.buttons = new MessageBoxButton[MAX_BUTTON_COUNT];
        for (int i = 0; i < this.buttons.length; ++i) {
            this.buttons[i] = MessageBoxButton.NULL;
        }
    }

    /**
     * Returns title of the message box.
     * 
     * @return Title string or null if not set.
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Sets title of the message box.
     * 
     * @param title
     *            Title of the message box window.
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Returns message box message.
     * 
     * @return Message box message or null if not set.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Sets message box message.
     * 
     * @param message
     *            Text to be presented in message box.
     */
    public void setMessage(final String message) {
        this.message = message;
    }

    /**
     * Sets the buttons.
     * 
     * <p>
     * Call to this function resets the default button and selected button
     * values.
     * 
     * @param newButtons
     *            Button to be present in message box.
     */
    public void setButtons(final MessageBoxButton... newButtons) {
        if (newButtons.length > this.buttons.length) {
            throw new IllegalArgumentException("Too many buttons");
        }

        for (int i = 0; i < newButtons.length; ++i) {
            assert newButtons[i] != null;

            this.buttons[i] = newButtons[i];
        }

        this.defaultButton = null;
        this.selectedButton = null;
    }

    /**
     * Sets default button.
     * 
     * @param defaultButton
     *            Button which should be selected by default. If not null then
     *            it must be previously set using setButtons. It must not be a
     *            {@link MessageBoxButton#NULL}.
     */
    public void setDefaultButton(final MessageBoxButton defaultButton) {
        ensureValidOrNull(defaultButton);

        this.defaultButton = defaultButton;
    }

    /**
     * Returns total number of buttons.
     * 
     * @return
     * Number of buttons defined.
     */
    public int getButtonCount() {
        for (int i = MAX_BUTTON_COUNT - 1; i >= 0; --i) {
            if (!this.buttons[i].isNull()) {
                return i + 1;
            }
        }
        return 0;
    }

    /**
     * Returns button at given index.
     * 
     * @param index
     *            Index of the button.
     * 
     * @return Button at given index.
     */
    public MessageBoxButton getButton(final int index) {
        return this.buttons[index];
    }

    /**
     * Returns number of buttons which are not NULL.
     * 
     * @return Number of buttons.
     */
    public int getValidButtonsCount() {
        int count = 0;
        for (MessageBoxButton button : this.buttons) {
            if (!button.isNull()) {
                ++count;
            }
        }
        return count;
    }

    /**
     * Checks if button at given index is the default one.
     * 
     * @param index
     *            Index of the button.
     * 
     * @return True if button at given index is the default one, false
     *         otherwise.
     */
    public boolean isDefaultButton(final int index) {
        return this.buttons[index] == this.defaultButton;
    }

    /**
     * Sets button selected by the user.
     * 
     * @param selectedButton
     *            Button selected by the user. If not null then it must be
     *            previously set using setButtons. It must not be a
     *            {@link MessageBoxButton#NULL}.
     */
    public void setSelectedButton(final MessageBoxButton selectedButton) {
        ensureValidOrNull(selectedButton);

        this.selectedButton = selectedButton;
    }

    /**
     * Returns button selected by the user.
     * 
     * @return
     * Value previously set using {@link #setSelectedButton(MessageBoxButton)} or
     * {@value MessageBoxButton#NULL}.
     */
    public MessageBoxButton getSelectedButton() {
        if (this.selectedButton != null) {
            return this.selectedButton;
        } else {
            return MessageBoxButton.NULL;
        }
    }

    /**
     * Ensures that the given button is valid or null.
     * 
     * <p>
     * Button is considered valid if it is not {@link MessageBoxButton#NULL} and
     * was previously added to the message box.
     * 
     * @param buttonToCheck
     *            Button to be checked.
     */
    private void ensureValidOrNull(final MessageBoxButton buttonToCheck) {
        if (buttonToCheck != null) {
            if (buttonToCheck.isNull()) {
                throw new IllegalArgumentException("NULL button is not acceptable");
            }

            boolean found = false;

            for (MessageBoxButton button : this.buttons) {
                if (button == buttonToCheck) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                throw new IllegalArgumentException("Button was not previously set");
            }
        }
    }

}
