package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.model.DirectoryInfo;
import info.octaedr.depchecker.model.FileInfo;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.tools.Path;
import info.octaedr.depchecker.ui.javafx.common.Controller;
import info.octaedr.depchecker.ui.javafx.common.SimpleListCellFactory;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class ProjectInputEditorController implements Controller<ProjectModel> {

    @FXML
    private ListView<DirectoryInfo> dirList;

    @FXML
    private Button dirAdd;

    @FXML
    private Button dirRemove;

    @FXML
    private Button dirRemoveAll;

    @FXML
    private ListView<FileInfo> fileList;

    @FXML
    private Button fileAdd;

    @FXML
    private Button fileRemove;

    @FXML
    private Button fileRemoveAll;

    @FXML
    private CheckBox inputRecursive;

    private ProjectModel model;

    private Stage stage;

    /**
     * Extension filter - "All Files".
     */
    private static final ExtensionFilter EXTENSION_FILTER_ALL_FILES = new ExtensionFilter(
            "All Files", "*.*");

    /**
     * File chooser dialog.
     */
    private FileChooser fileChooser;

    /**
     * Directory chooser dialog.
     */
    private DirectoryChooser directoryChooser;

    public ProjectInputEditorController() {
        // nothing to do
    }

    @Override
    @FXML
    public void initialize() {
        this.fileChooser = new FileChooser();
        this.fileChooser.getExtensionFilters().add(EXTENSION_FILTER_ALL_FILES);

        this.directoryChooser = new DirectoryChooser();

        // prepare cell factory
        this.dirList.setCellFactory(new SimpleListCellFactory<DirectoryInfo>() {

            @Override
            protected void updateItem(final ListCell<DirectoryInfo> cell, final DirectoryInfo item,
                    final boolean empty) {
                if (empty) {
                    cell.textProperty().unbind();
                } else {
                    cell.textProperty().bind(item.pathProperty());
                }
            }

        });
        this.fileList.setCellFactory(new SimpleListCellFactory<FileInfo>() {

            @Override
            protected void updateItem(final ListCell<FileInfo> cell, final FileInfo item,
                    final boolean empty) {
                if (empty) {
                    cell.textProperty().unbind();
                } else {
                    cell.textProperty().bind(item.pathProperty());
                }
            }

        });

        // make sure single selection is enabled
        this.dirList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.fileList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setModel(final ProjectModel model) {
        this.model = model;

        this.dirList.itemsProperty().bind(model.inputDirectoriesProperty());
        this.fileList.itemsProperty().bind(model.inputFilesProperty());

        this.inputRecursive.selectedProperty().bindBidirectional(model.inputRecursiveProperty());

        this.dirRemove.disableProperty().bind(
                this.dirList.getSelectionModel().selectedItemProperty().isNull());
        this.fileRemove.disableProperty().bind(
                this.fileList.getSelectionModel().selectedItemProperty().isNull());

        this.dirAdd.disableProperty().bind(this.model.projectFileProperty().isNull());
        this.fileAdd.disableProperty().bind(this.model.projectFileProperty().isNull());
    }

    @FXML
    void onDirAdd(final ActionEvent event) {
        if (this.model.getProjectFile() != null) {
            this.directoryChooser.setTitle("Add input directory");
            this.directoryChooser.setInitialDirectory(this.model.getProjectFile().getParentFile());
            final File chosenDir = this.directoryChooser.showDialog(this.stage);
            if (chosenDir != null) {
                final DirectoryInfo directoryInfo = new DirectoryInfo(Path.relativize(
                        this.model.getProjectFile(), chosenDir));

                this.model.getInputDirectories().add(directoryInfo);
            }
        }
    }

    @FXML
    void onDirRemove(final ActionEvent event) {
        final DirectoryInfo selectedDir = this.dirList.getSelectionModel().selectedItemProperty()
                .get();
        if (selectedDir != null) {
            this.model.getInputDirectories().remove(selectedDir);
        }
    }

    @FXML
    void onDirRemoveAll(final ActionEvent event) {
        this.model.getInputDirectories().clear();
    }

    @FXML
    void onFileAdd(final ActionEvent event) {
        if (this.model.getProjectFile() != null) {
            this.fileChooser.setTitle("Add input file");
            this.fileChooser.setInitialDirectory(this.model.getProjectFile().getParentFile());

            final File chosenFile = this.fileChooser.showOpenDialog(this.stage);
            if (chosenFile != null) {
                final FileInfo fileInfo = new FileInfo(Path.relativize(this.model.getProjectFile(),
                        chosenFile));

                this.model.getInputFiles().add(fileInfo);
            }
        }
    }

    @FXML
    void onFileRemove(final ActionEvent event) {
        final FileInfo selectedFile = this.fileList.getSelectionModel().selectedItemProperty()
                .get();
        if (selectedFile != null) {
            this.model.getInputFiles().remove(selectedFile);
        }
    }

    @FXML
    void onFileRemoveAll(final ActionEvent event) {
        this.model.getInputFiles().clear();
    }

}
