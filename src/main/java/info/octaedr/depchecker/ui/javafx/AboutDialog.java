package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.model.AboutDataModel;
import info.octaedr.depchecker.ui.javafx.common.AppDialog;
import javafx.stage.Modality;
import javafx.stage.Window;

/**
 * About dialog.
 * 
 * <p>
 * About dialog shows information about the application, authors and user 3rd
 * party elements.
 */
public class AboutDialog extends AppDialog {

    /**
     * Constructs the dialog.
     * 
     * @param owner
     *            Dialog owner window.
     * @param model
     *            Model to use.
     */
    public AboutDialog(final Window owner, final AboutDataModel model) {
        super(owner, Modality.APPLICATION_MODAL);

        createStage();
        setFxmlLocation(getClass().getResource("AboutDialog.fxml"));
        setTitle("About");

        initialize(model);
    }

}
