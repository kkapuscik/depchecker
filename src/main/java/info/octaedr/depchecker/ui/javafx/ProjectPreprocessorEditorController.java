package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.model.DirectoryInfo;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.tools.Path;
import info.octaedr.depchecker.ui.javafx.common.Controller;
import info.octaedr.depchecker.ui.javafx.common.SimpleListCellFactory;

import java.io.File;

import javafx.beans.binding.BooleanBinding;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class ProjectPreprocessorEditorController implements Controller<ProjectModel> {

    @FXML
    private ListView<DirectoryInfo> incList;

    @FXML
    private Button incAdd;

    @FXML
    private Button incRemove;

    @FXML
    private Button incRemoveAll;

    @FXML
    private Button incMoveUp;

    @FXML
    private Button incMoveDown;

    private ProjectModel model;

    private Stage stage;

    /**
     * Directory chooser dialog.
     */
    private DirectoryChooser directoryChooser;

    public ProjectPreprocessorEditorController() {
        // nothing to do
    }

    @Override
    @FXML
    public void initialize() {
        this.directoryChooser = new DirectoryChooser();

        // prepare cell factory
        this.incList.setCellFactory(new SimpleListCellFactory<DirectoryInfo>() {

            @Override
            protected void updateItem(final ListCell<DirectoryInfo> cell, final DirectoryInfo item,
                    final boolean empty) {
                if (empty) {
                    cell.textProperty().unbind();
                } else {
                    cell.textProperty().bind(item.pathProperty());
                }
            }
            
        });

        // make sure single selection is enabled
        this.incList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setModel(final ProjectModel model) {
        this.model = model;

        this.incList.itemsProperty().bind(model.includeDirectoriesProperty());

        final BooleanBinding isIncDirSelectedBind = this.incList.getSelectionModel()
                .selectedItemProperty().isNull();
        this.incRemove.disableProperty().bind(isIncDirSelectedBind);
        this.incMoveUp.disableProperty().bind(isIncDirSelectedBind);
        this.incMoveDown.disableProperty().bind(isIncDirSelectedBind);

        this.incAdd.disableProperty().bind(this.model.projectFileProperty().isNull());
    }

    @FXML
    void onIncAdd(final ActionEvent event) {
        if (this.model.getProjectFile() != null) {
            this.directoryChooser.setTitle("Add include directory");
            this.directoryChooser.setInitialDirectory(this.model.getProjectFile().getParentFile());
            final File chosenDir = this.directoryChooser.showDialog(this.stage);
            if (chosenDir != null) {
                final DirectoryInfo directoryInfo = new DirectoryInfo(Path.relativize(
                        this.model.getProjectFile(), chosenDir));

                this.model.getIncludeDirectories().add(directoryInfo);
            }
        }
    }

    @FXML
    void onIncRemove(final ActionEvent event) {
        final DirectoryInfo selectedDir = this.incList.getSelectionModel().selectedItemProperty()
                .get();
        if (selectedDir != null) {
            this.model.getIncludeDirectories().remove(selectedDir);
        }
    }

    @FXML
    void onIncRemoveAll(final ActionEvent event) {
        this.model.getIncludeDirectories().clear();
    }

    @FXML
    void onIncMoveUp(final ActionEvent event) {
        final DirectoryInfo selectedDir = this.incList.getSelectionModel().selectedItemProperty()
                .get();
        if (selectedDir != null) {
            final ObservableList<DirectoryInfo> dirs = this.model.getIncludeDirectories();

            final int selectedIndex = dirs.indexOf(selectedDir);
            if (selectedIndex > 0) {
                dirs.remove(selectedIndex);
                dirs.add(selectedIndex - 1, selectedDir);

                this.incList.getSelectionModel().select(selectedIndex - 1);
            }
        }
    }

    @FXML
    void onIncMoveDown(final ActionEvent event) {
        final DirectoryInfo selectedDir = this.incList.getSelectionModel().selectedItemProperty()
                .get();
        if (selectedDir != null) {
            final ObservableList<DirectoryInfo> dirs = this.model.getIncludeDirectories();

            final int selectedIndex = dirs.indexOf(selectedDir);
            if (selectedIndex < dirs.size() - 1) {
                dirs.remove(selectedIndex);
                dirs.add(selectedIndex + 1, selectedDir);

                this.incList.getSelectionModel().select(selectedIndex + 1);
            }
        }
    }

}
