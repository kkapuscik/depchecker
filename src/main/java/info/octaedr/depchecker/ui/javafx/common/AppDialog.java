package info.octaedr.depchecker.ui.javafx.common;

import javafx.stage.Modality;
import javafx.stage.Window;

/**
 * Base class for application dialog windows.
 */
public abstract class AppDialog extends AppWindow {

    /**
     * Constructs the dialog window.
     * 
     * @param owner
     *            Owner window.
     * @param modality
     *            Requested modality.
     */
    protected AppDialog(final Window owner, final Modality modality) {
        setOwner(owner);
        setModality(modality);
    }

}
