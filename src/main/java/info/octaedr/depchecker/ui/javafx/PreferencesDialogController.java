package info.octaedr.depchecker.ui.javafx;

import info.octaedr.cmd.PathExecutableFinder;
import info.octaedr.depchecker.model.PreferencesDataModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.FadeTransition;
import javafx.animation.SequentialTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Controller for AboutDialog.
 **/
public class PreferencesDialogController implements Controller<PreferencesDataModel> {

    /**
     * Default name of the doxygen executable file.
     */
    private static final String DEFAULT_DOXYGEN_EXE_NAME = "doxygen";

    /**
     * Default name of the graphviz executable file.
     */
    private static final String DEFAULT_GRAPHVIZ_EXE_NAME = "dot";

    /**
     * ResourceBundle that was given to the FXMLLoader
     */
    @FXML
    private ResourceBundle resources;

    /**
     * URL location of the FXML file that was given to the FXMLLoader
     */
    @FXML
    private URL location;

    /**
     * Data model.
     */
    private PreferencesDataModel model;

    /**
     * Directory chooser dialog.
     */
    private DirectoryChooser directoryChooser;

    /**
     * Executable chooser dialog.
     */
    private FileChooser executableChooser;

    /**
     * Stage for which this controller was created.
     */
    private Stage stage;

    /**
     * fx:id="prefDoxygenPath"
     */
    @FXML
    private TextField prefDoxygenFilePath;

    /**
     * fx:id="prefGraphvizPath"
     */
    @FXML
    private TextField prefGraphvizDirPath;

    /**
     * fx:id="prefStatus"
     */
    @FXML
    private Label prefStatus;

    /**
     * Constructs the controller.
     */
    public PreferencesDialogController() {
        // nothing to do
    }

    /**
     * This method is called by the FXMLLoader when initialization is complete
     */
    @Override
    @FXML
    public void initialize() {
        this.executableChooser = new FileChooser();
        this.executableChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("*.exe", "*.exe"));

        this.directoryChooser = new DirectoryChooser();

        this.prefStatus.setText(null);
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setModel(final PreferencesDataModel model) {
        this.model = model;

        this.prefDoxygenFilePath.setText(this.model.getDoxygenFilePath());
        this.prefGraphvizDirPath.setText(this.model.getGraphvizDirPath());
    }

    @FXML
    void onPrefDoxygenPathBrowse(final ActionEvent event) {
        this.executableChooser.setTitle("Select doxygen executable");
        if (this.model.getDoxygenFilePath() != null) {
            final File initialFile = new File(this.prefDoxygenFilePath.getText()).getParentFile();
            this.executableChooser.setInitialDirectory(initialFile);
        }
        final File chosenExe = this.executableChooser.showOpenDialog(this.stage);
        if (chosenExe != null) {
            this.prefDoxygenFilePath.setText(chosenExe.getAbsolutePath());
        }
    }

    @FXML
    void onPrefDoxygenPathSave(final ActionEvent event) {
        final String doxygenFilePath = this.prefDoxygenFilePath.getText();
        if (this.model.checkDoxygenPath(doxygenFilePath)) {
            this.model.setDoxygenFilePath(doxygenFilePath);
            showSucecessfulStatus();
        } else {
            MessageDialogBox.showMessageDialog(this.stage, "Doxygen path is incorrect", "Error");
        }
    }
    
    @FXML
    void onPrefDoxygenPathFind(final ActionEvent event) {
        final String plainName = DEFAULT_DOXYGEN_EXE_NAME;
        final String exeName = DEFAULT_DOXYGEN_EXE_NAME + PathExecutableFinder.DEFAULT_WINDOWS_EXE_EXTENSION;
        
        final File doxyFile = new PathExecutableFinder().find(exeName, plainName);
        if (doxyFile != null) {
            this.prefDoxygenFilePath.setText(doxyFile.getAbsolutePath());
        }
    }

    @FXML
    void onPrefGraphvizPathBrowse(final ActionEvent event) {
        this.directoryChooser.setTitle("Select graphviz directory");
        if (this.model.getGraphvizDirPath() != null) {
            final File initialDir = new File(this.prefGraphvizDirPath.getText());
            this.directoryChooser.setInitialDirectory(initialDir);
        }

        final File chosenDir = this.directoryChooser.showDialog(this.stage);
        if (chosenDir != null) {
            this.prefGraphvizDirPath.setText(chosenDir.getAbsolutePath());
        }
    }

    @FXML
    void onPrefGraphvizPathSave(final ActionEvent event) {
        final String graphvizDirPath = this.prefGraphvizDirPath.getText();
        if (this.model.checkGraphvizPath(graphvizDirPath)) {
            this.model.setGraphvizDirPath(graphvizDirPath);
            showSucecessfulStatus();
        } else {
            MessageDialogBox.showMessageDialog(this.stage, "Graphviz path is incorrect", "Error");
        }
    }

    @FXML
    void onPrefGraphvizPathFind(final ActionEvent event) {
        final String plainName = DEFAULT_GRAPHVIZ_EXE_NAME;
        final String exeName = DEFAULT_GRAPHVIZ_EXE_NAME + PathExecutableFinder.DEFAULT_WINDOWS_EXE_EXTENSION;

        final File graphvizFile = new PathExecutableFinder().find(exeName, plainName);
        if (graphvizFile != null) {
            final File graphvizDir = graphvizFile.getParentFile();
            this.prefGraphvizDirPath.setText(graphvizDir.getAbsolutePath());
        }
    }
    
    private void showSucecessfulStatus() {
        this.prefStatus.setText("Operation Successful");

        FadeTransition fader = createFader(this.prefStatus);

        SequentialTransition Fade = new SequentialTransition(this.prefStatus, fader);

        Fade.play();
    }

    private FadeTransition createFader(Node node) {
        FadeTransition fade = new FadeTransition(Duration.seconds(3), node);
        fade.setFromValue(1);
        fade.setToValue(0);

        return fade;
    }

    /**
     * Processes the close dialog event.
     * 
     * @param event
     *            Event descriptor.
     */
    @FXML
    void closeDialog(final ActionEvent event) {
        this.stage.close();
    }

}
