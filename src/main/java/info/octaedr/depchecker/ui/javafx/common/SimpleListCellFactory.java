package info.octaedr.depchecker.ui.javafx.common;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * ListCell factory that binds selected string property to a cell text value.
 * 
 * @param <DataType>
 *            Type of the list data item.
 */
public abstract class SimpleListCellFactory<DataType> implements
        Callback<ListView<DataType>, ListCell<DataType>> {

    /**
     * ListCell class that binds selected string property to a cell text value.
     */
    private class ListCellImpl extends ListCell<DataType> {

        /**
         * Constructs the cell.
         */
        public ListCellImpl() {
            // nothing to do
        }

        @Override
        protected void updateItem(final DataType item, final boolean empty) {
            super.updateItem(item, empty);

            SimpleListCellFactory.this.updateItem(this, item, empty);
        }
    }

    /**
     * Constructs the factory.
     */
    public SimpleListCellFactory() {
        // nothing to do
    }

    @Override
    public ListCell<DataType> call(final ListView<DataType> list) {
        return new ListCellImpl();
    }

    /**
     * Updates the list cell.
     * 
     * @param cell
     *            List cell for which this method was called.
     * @param item
     *            The new item for the cell.
     * @param empty
     *            Flag whether or not cell represents data from the list. If it
     *            is empty, then it does not represent any domain data, but is a
     *            cell being used to render an "empty" row.
     */
    protected abstract void updateItem(final ListCell<DataType> cell, final DataType item,
            final boolean empty);

}
