package info.octaedr.depchecker.ui.javafx.common;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

public abstract class SimpleTableCellFactory<T, S> implements
        Callback<TableColumn<T, S>, TableCell<T, S>> {

    private class TableCellImpl extends TableCell<T, S> {

        @Override
        protected void updateItem(final S item, boolean empty) {
            super.updateItem(item, empty);

            SimpleTableCellFactory.this.updateItem(this, item, empty);
        }

    }

    /**
     * Constructs the factory.
     */
    public SimpleTableCellFactory() {
        // nothing to do
    }

    @Override
    public TableCell<T, S> call(final TableColumn<T, S> arg0) {
        return new TableCellImpl();
    }

    public abstract void updateItem(final TableCell<T, S> tableCell, final S item,
            final boolean empty);

}
