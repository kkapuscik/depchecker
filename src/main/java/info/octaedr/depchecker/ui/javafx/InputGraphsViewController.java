package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.engine.DotGraphSerializer;
import info.octaedr.depchecker.model.InputGraphEntry;
import info.octaedr.depchecker.model.InputGraphsModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;
import info.octaedr.depchecker.ui.javafx.common.MessageBox;

import java.io.File;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class InputGraphsViewController implements Controller<InputGraphsModel> {

    /**
     * Extension filter - "DepChecker Project".
     */
    private static final ExtensionFilter EXTENSION_FILTER_DOT_FILES = new ExtensionFilter(
            "DOT graph files", "*.dot");

    /**
     * Extension filter - "All Files".
     */
    private static final ExtensionFilter EXTENSION_FILTER_ALL_FILES = new ExtensionFilter(
            "All Files", "*.*");

    @FXML
    private TableView<InputGraphEntry> inputGraphsTable;

    private Stage stage;

    private InputGraphsModel model;

    @FXML
    Button saveDotButton;

    @FXML
    Button removeGraphButton;

    @FXML
    Button removeAllGraphsButton;

    private FileChooser dotFileChooser;

    @Override
    public void initialize() {
        assert this.inputGraphsTable != null;

        this.dotFileChooser = new FileChooser();
        this.dotFileChooser.getExtensionFilters().add(
                EXTENSION_FILTER_DOT_FILES);
        this.dotFileChooser.getExtensionFilters().add(
                EXTENSION_FILTER_ALL_FILES);

        final TableColumn<InputGraphEntry, String> userNameColumn = new TableColumn<>();
        final TableColumn<InputGraphEntry, String> graphNameColumn = new TableColumn<>();
        final TableColumn<InputGraphEntry, String> processorNameColumn = new TableColumn<>();
        final TableColumn<InputGraphEntry, String> updateDateColumn = new TableColumn<>();

        userNameColumn.setText("Name");
        graphNameColumn.setText("Graph");
        processorNameColumn.setText("Processor");
        updateDateColumn.setText("Updated");

        userNameColumn
                .setCellValueFactory(new PropertyValueFactory<InputGraphEntry, String>(
                        "userName"));
        graphNameColumn
                .setCellValueFactory(new PropertyValueFactory<InputGraphEntry, String>(
                        "graphName"));
        processorNameColumn
                .setCellValueFactory(new PropertyValueFactory<InputGraphEntry, String>(
                        "processorName"));
        updateDateColumn
                .setCellValueFactory(new PropertyValueFactory<InputGraphEntry, String>(
                        "updateDate"));

        this.inputGraphsTable.getColumns().add(userNameColumn);
        this.inputGraphsTable.getColumns().add(graphNameColumn);
        this.inputGraphsTable.getColumns().add(processorNameColumn);
        this.inputGraphsTable.getColumns().add(updateDateColumn);

        this.inputGraphsTable.getSelectionModel().selectedIndexProperty()
                .addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> arg0,
                            Number oldValue, Number newValue) {
                        refreshButtonsState();
                    }
                });
    }

    private void refreshButtonsState() {
        final int index = this.inputGraphsTable.getSelectionModel().getSelectedIndex();
        final boolean nothingSelected = (index < 0);
        
        this.saveDotButton.setDisable(nothingSelected);
        this.removeGraphButton.setDisable(nothingSelected);
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setModel(final InputGraphsModel model) {
        assert model != null;

        this.model = model;

        this.inputGraphsTable.setItems(model.getInputGraphs());

        refreshButtonsState();
    }

    @FXML
    public void onSaveDot(final ActionEvent event) {
        final InputGraphEntry entry = this.inputGraphsTable.getSelectionModel()
                .getSelectedItem();
        if (entry != null) {
            this.dotFileChooser.setTitle("Save DOT Graph");

            final File selectedFile = this.dotFileChooser
                    .showSaveDialog(this.stage);
            if (selectedFile != null) {
                /* prepare graph serializer */
                final DotGraphSerializer dotGraphSerializer = new DotGraphSerializer();

                /* serialize file system graph to output file */
                if (!dotGraphSerializer.serialize(entry.getGraph().getData(),
                        selectedFile)) {
                    MessageBox.warning(this.stage, "Save DOT Graph",
                            "Cannot serialize graph to DOT file.");
                }
            }
        }
    }

    @FXML
    void onGraphRemove(final ActionEvent event) {
        final InputGraphEntry entry = this.inputGraphsTable.getSelectionModel()
                .getSelectedItem();
        if (entry != null) {
            this.model.getInputGraphs().remove(entry);
        }
    }

    @FXML
    void onGraphRemoveAll(final ActionEvent event) {
        this.model.getInputGraphs().clear();
    }

}
