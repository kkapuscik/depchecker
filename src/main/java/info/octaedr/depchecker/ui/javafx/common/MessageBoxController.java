package info.octaedr.depchecker.ui.javafx.common;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * MessageBox controller.
 */
public class MessageBoxController implements Controller<MessageBoxModel> {

    /**
     * ResourceBundle that was given to the FXMLLoader.
     */
    @FXML
    private ResourceBundle resources;

    /**
     * URL location of the FXML file that was given to the FXMLLoader.
     */
    @FXML
    private URL location;

    /**
     * fx:id="button4"
     */
    @FXML
    private Button button4;

    /**
     * fx:id="button4"
     */
    @FXML
    private Button button3;

    /**
     * fx:id="button4"
     */
    @FXML
    private Button button2;

    /**
     * fx:id="button4"
     */
    @FXML
    private Button button1;

    /**
     * fx:id="messageText"
     */
    @FXML
    private TextArea messageText;
    
    /**
     * fx:id="messageIcon"
     */
    @FXML
    private ImageView messageIcon;
    
    /**
     * Stage in which this controller is working.
     */
    private Stage stage;

    /**
     * Model used by this dialog.
     */
    private MessageBoxModel model;

    /**
     * This method is called by the FXMLLoader when initialization is complete
     */
    @Override
    @FXML
    public void initialize() {
        // nothing to do
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
    }

    @Override
    public void setModel(final MessageBoxModel model) {
        assert model != null;

        this.model = model;
        this.model.setSelectedButton(null);

        final Button[] uiButtons = new Button[] { this.button4, this.button3, this.button2,
                this.button1 };
        final int buttonCount = model.getButtonCount();
        if (buttonCount > uiButtons.length) {
            throw new IllegalStateException("Too many buttons");
        }
        int currentUiIndex = uiButtons.length - 1;

        for (int i = buttonCount - 1; i >= 0; --i) {
            initUiButton(uiButtons[currentUiIndex], model.getButton(i),
                    model.isDefaultButton(i));
            --currentUiIndex;
        }
        while (currentUiIndex >= 0) {
            initUiButton(uiButtons[currentUiIndex], null, false);
            --currentUiIndex;
        }

        this.messageText.setText(this.model.getMessage());
        this.stage.setTitle(this.model.getTitle());
    }

    /**
     * Processes action triggered by message box button.
     * 
     * @param event
     *            UI event descriptor.
     */
    @FXML
    void onButtonAction(final ActionEvent event) {
        if (event.getSource() instanceof Button) {
            final Button uiButton = (Button) event.getSource();

            if (uiButton.getUserData() instanceof MessageBoxButton) {
                final MessageBoxButton selectedButton = (MessageBoxButton) uiButton.getUserData();

                this.model.setSelectedButton(selectedButton);
                this.stage.close();
            }
        }
    }

    /**
     * Initializes UI button with button data.
     * 
     * @param uiButton
     *            UI button to be initialized.
     * @param button
     *            Button data to set, null to hide & disable the button.
     * @param isDefault
     *            Flag indicating that the button will be the default dialog
     *            control.
     */
    private void initUiButton(final Button uiButton, final MessageBoxButton button,
            final boolean isDefault) {
        uiButton.setUserData(button);
        uiButton.setDefaultButton(isDefault);
        uiButton.setVisible(button != null);

        if (button != null) {
            uiButton.setText(button.getText());
        }
    }

}