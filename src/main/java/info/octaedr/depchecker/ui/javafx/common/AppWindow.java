package info.octaedr.depchecker.ui.javafx.common;

import java.net.URL;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * Application window.
 */
public abstract class AppWindow {

    /**
     * Flag indicating if window was already initialized (set up).
     */
    private boolean initialized = false;

    /**
     * Stage assigned to the window.
     */
    private Stage stage;

    /**
     * Window title.
     */
    private String title;

    /**
     * JavaFX owner window.
     */
    private Window owner;

    /**
     * Location of the FXML file.
     */
    private URL fxmlLocation;

    /**
     * Window modality.
     */
    private Modality modality;

    /**
     * Constructs the window.
     */
    protected AppWindow() {
        // nothing to do
    }

    /**
     * Sets window modality.
     * 
     * @param modality
     *            Modality to set.
     */
    protected void setModality(Modality modality) {
        ensureNotInitialized();

        this.modality = modality;
    }

    /**
     * Sets window title.
     * 
     * @param title
     *            Title to set.
     */
    protected void setTitle(final String title) {
        ensureNotInitialized();

        this.title = title;
    }

    /**
     * Sets owner window.
     * 
     * @param owner
     *            Owner window to set.
     */
    protected void setOwner(final Window owner) {
        ensureNotInitialized();

        this.owner = owner;
    }

    /**
     * Sets FXML file location.
     * 
     * @param fxmlLocation
     *            Location of the FXML file.
     */
    protected void setFxmlLocation(final URL fxmlLocation) {
        assert fxmlLocation != null;

        ensureNotInitialized();

        this.fxmlLocation = fxmlLocation;
    }

    /**
     * Sets stage assigned to window.
     * 
     * @param stage
     *            Stage instance.
     */
    protected void setStage(final Stage stage) {
        assert stage != null;

        ensureNotInitialized();
        if (this.stage != null) {
            throw new IllegalStateException("Stage already set");
        }

        this.stage = stage;
    }

    /**
     * Constructs new stage and assigns it to window.
     */
    protected void createStage() {
        ensureNotInitialized();
        if (this.stage != null) {
            throw new IllegalStateException("Stage already set");
        }

        this.stage = new Stage();
    }

    /**
     * Initializes the window.
     * 
     * <p>
     * Sets up the stage, scene, controller and other window elements.
     * 
     * @param model
     *            Model to be set for created controller.
     */
    public <ModelType> void initialize(final ModelType model) {
        if (this.stage == null) {
            throw new IllegalStateException("Stage not set");
        }
        if (this.fxmlLocation == null) {
            throw new IllegalStateException("FXML location not set");
        }

        try {
            final FXMLLoader loader = new FXMLLoader();

            loader.setLocation(this.fxmlLocation);

            final Parent contentsRoot = (Parent) loader.load();
            assert contentsRoot == loader.getRoot();

            this.stage.setScene(new Scene(contentsRoot));

            if (this.title != null) {
                this.stage.setTitle(this.title);
            }

            if (this.owner != null) {
                this.stage.initOwner(this.owner);
            }

            if (this.modality != null) {
                this.stage.initModality(this.modality);
            }

            final Controller<ModelType> controller = loader.getController();
            controller.setStage(this.stage);
            controller.setModel(model);

            this.initialized = true;
        } catch (final Exception e) {
            throw new IllegalStateException("Scene setup failed", e);
        }
    }

    /**
     * Shows the window.
     */
    public void show() {
        ensureInitialized();

        this.stage.show();
    }

    /**
     * Shows the window and wait until it is closed.
     */
    public void showAndWait() {
        ensureInitialized();

        this.stage.showAndWait();
    }

    /**
     * Hides the window.
     */
    public void hide() {
        ensureInitialized();

        this.stage.hide();
    }

    /**
     * Ensures the window was initialized.
     * 
     * <p>
     * Throws IllegalStateException is window was not initialized.
     */
    private void ensureInitialized() {
        if (!this.initialized) {
            throw new IllegalStateException("Window not initialized");
        }
    }

    /**
     * Ensures the window was not yet initialized.
     * 
     * <p>
     * Throws IllegalStateException is window was already initialized.
     */
    private void ensureNotInitialized() {
        if (this.initialized) {
            throw new IllegalStateException("Window already initialized");
        }
    }

}
