package info.octaedr.depchecker.ui.javafx.common;

import javafx.stage.Modality;
import javafx.stage.Window;

/**
 * About dialog.
 * 
 * <p>
 * About dialog shows information about the application, authors and user 3rd
 * party elements.
 */
public class MessageBox extends AppDialog {

    /**
     * Shows information dialog.
     * 
     * <p>
     * Information dialog have only "close" button.
     * 
     * @param owner
     *            Window owning this dialog.
     * @param title
     *            Dialog title window.
     * @param message
     *            Dialog message.
     */
    public static void information(final Window owner, final String title, final String message) {
        closeOnly(owner, title, message);
    }

    /**
     * Shows warning dialog.
     * 
     * <p>
     * Warning dialog have only "close" button.
     * 
     * @param owner
     *            Window owning this dialog.
     * @param title
     *            Dialog title window.
     * @param message
     *            Dialog message.
     */
    public static void warning(final Window owner, final String title, final String message) {
        closeOnly(owner, title, message);
    }

    /**
     * Shows question dialog.
     * 
     * 
     * @param owner
     *            Window owning this dialog.
     * @param title
     *            Dialog title window.
     * @param message
     *            Dialog message.
     * @param defaultButton
     *            Button to be marked as the default button.
     * @param buttons
     *            Button to be present in the dialog.
     * 
     * @return Selected button. May be {@link MessageBoxButton#NULL} if dialog
     *         was closed without selecting any button.
     * 
     * @see MessageBoxButton
     */
    public static MessageBoxButton question(final Window owner, final String title,
            final String message, final MessageBoxButton defaultButton,
            final MessageBoxButton... buttons) {
        final MessageBoxModel model = new MessageBoxModel();
        model.setButtons(buttons);
        model.setDefaultButton(defaultButton);
        model.setTitle(title);
        model.setMessage(message);

        final MessageBox messageBox = new MessageBox(owner, model);
        messageBox.showAndWait();

        return model.getSelectedButton();
    }

    /**
     * Shows message box with close button only.
     * 
     * @param owner
     *            Window owning this dialog.
     * @param title
     *            Dialog title window.
     * @param message
     *            Dialog message.
     */
    private static void closeOnly(Window owner, String title, String message) {
        final MessageBoxModel model = new MessageBoxModel();
        model.setButtons(MessageBoxButton.CLOSE);
        model.setDefaultButton(MessageBoxButton.CLOSE);
        model.setTitle(title);
        model.setMessage(message);

        final MessageBox messageBox = new MessageBox(owner, model);
        messageBox.showAndWait();
    }

    /**
     * Constructs the dialog.
     * 
     * @param owner
     *            Dialog owner window.
     * @param model
     *            Model describing the message box.
     */
    public MessageBox(final Window owner, final MessageBoxModel model) {
        super(owner, Modality.APPLICATION_MODAL);

        createStage();
        setFxmlLocation(getClass().getResource("MessageBox.fxml"));
        setTitle("DepChecker");

        initialize(model);
    }

}
