package info.octaedr.depchecker.ui.javafx;

import info.octaedr.depchecker.model.DepCheckerModel;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringExpression;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Controller for DepCheckerFrame.
 */
public class MainWindowController implements Controller<DepCheckerModel> {
        
    /**
     * ResourceBundle that was given to the FXMLLoader
     */
    @FXML
    private ResourceBundle resources;

    /**
     * URL location of the FXML file that was given to the FXMLLoader
     */
    @FXML
    private URL location;

    /**
     * fx:id="mainPane"
     */
    @FXML
    private BorderPane mainPane;

    /**
     * fx:id="menuBar" controller
     */
    @FXML
    private MainWindowMenuBarController menuBarController;

    /**
     * fx:id="toolBar" controller
     */
    @FXML
    private MainWindowToolBarController toolBarController;

    /**
     * fx:id="projectEditor" controller
     */
    @FXML
    private ProjectEditorController projectEditorController;

    /**
     * fx:id="analyzerView" controller
     */
    @FXML
    private AnalyzerViewController analyzerViewController;
    
    /**
     * fx:id="inputGraphsView" controller
     */
    @FXML
    private InputGraphsViewController inputGraphsViewController;
    
    /**
     * fx:id="logView" controller
     */
    @FXML
    private LogViewController logViewController;
    
    /**
     * Application data model.
     */
    private DepCheckerModel model;

    /**
     * Title string binding.
     */
    private StringExpression titleBinding;

    /**
     * Stage for which this controller was created.
     */
    private Stage stage;

    /**
     * Wrapper over project I/O operations.
     */
    private final ProjectIOWrapper projectIOWrapper;
    
    /**
     * Constructs the controller.
     */
    public MainWindowController() {
        this.projectIOWrapper= new ProjectIOWrapper(); 
    }
    
    /**
     * This method is called by the FXMLLoader when initialization is complete
     */
    @Override
    @FXML
    public void initialize() {
        assert this.menuBarController != null;
        assert this.toolBarController != null;
        assert this.projectEditorController != null;
        assert this.analyzerViewController != null;
        assert this.inputGraphsViewController != null;
        assert this.logViewController != null;

        this.projectIOWrapper.initialize();
    }

    @Override
    public void setStage(final Stage stage) {
        this.stage = stage;
        this.projectIOWrapper.setStage(stage);

        this.menuBarController.setStage(stage);
        this.toolBarController.setStage(stage);
        this.projectEditorController.setStage(stage);
        this.analyzerViewController.setStage(stage);
        this.inputGraphsViewController.setStage(stage);
        this.logViewController.setStage(stage);

        this.stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(final WindowEvent e) {
                if (!MainWindowController.this.projectIOWrapper.onAppExit()) {
                    // consume so it is not continued
                    e.consume();                    
                }
            }
        });
    }

    @Override
    public void setModel(final DepCheckerModel model) {
        this.model = model;

        final ProjectModel projectModel = this.model.getProjectModel();

        this.projectIOWrapper.setModel(projectModel);
        
        this.menuBarController.setModel(this.model);
        this.menuBarController.setProjectIOWrapper(this.projectIOWrapper);

        this.toolBarController.setModel(this.model);
        this.toolBarController.setProjectIOWrapper(this.projectIOWrapper);

        this.logViewController.setModel(this.model.getLogModel());
        
        this.projectEditorController.setModel(projectModel);
        this.analyzerViewController.setModel(this.model);

        this.inputGraphsViewController.setModel(this.model.getInputGraphsModel());
        
        this.titleBinding = Bindings.concat(
                "DepChecker - ",
                Bindings.when(projectModel.modifiedFlagProperty()).then("* ").otherwise(""),
                projectModel.projectFileProperty());

        this.stage.titleProperty().bind(this.titleBinding);
    }

}
