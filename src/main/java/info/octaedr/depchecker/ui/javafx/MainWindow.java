package info.octaedr.depchecker.ui.javafx;

import javafx.stage.Stage;
import info.octaedr.depchecker.model.DepCheckerModel;
import info.octaedr.depchecker.ui.javafx.common.AppMainWindow;

/**
 * Main DepChecker application window.
 */
public class MainWindow extends AppMainWindow {

    /**
     * Constructs the window.
     * 
     * @param stage
     *            Application stage.
     * @param model
     *            Application model to be used.
     */
    public MainWindow(final Stage stage, final DepCheckerModel model) {
        setStage(stage);
        //setFxmlLocation(getClass().getResource("MainWindow.fxml"));
        setFxmlLocation(getClass().getResource("MainWindow2.fxml"));
        setTitle("DepChecker");

        initialize(model);
    }

}
