package info.octaedr.depchecker.ui.javafx;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import info.octaedr.depchecker.model.LogLevel;
import info.octaedr.depchecker.model.LogListener;
import info.octaedr.depchecker.model.LogModel;
import info.octaedr.depchecker.ui.javafx.common.Controller;

public class LogViewController implements Controller<LogModel>, LogListener {

    private static final int LOG_TEXT_LENGTH_LIMIT = 16 * 1024;
    private static final int LOG_TEXT_LENGTH_CUTOFF = 4 * 1024;

    private final StringBuilder logLineBuilder = new StringBuilder();
    
    private final ObjectProperty<LogLevel> currentLevel = new SimpleObjectProperty<>();
    
    @FXML
    private TextArea logTextArea;
    
    @FXML
    private Button clearButton;
    
    @FXML
    private ComboBox<LogLevel> currentLevelCombo;
    
    @Override
    public void initialize() {
        assert this.logTextArea != null;
        assert this.clearButton != null;
        assert this.currentLevelCombo != null;
        
        this.logTextArea.setEditable(false);
    }

    @Override
    public void setStage(final Stage stage) {
        // nothing to do
    }

    @Override
    public void setModel(final LogModel model) {
        assert model != null;
        
        model.addListener(this);
        model.getWriter("App").info("Log ready!");

        this.currentLevelCombo.getItems().add(LogLevel.ERROR);
        this.currentLevelCombo.getItems().add(LogLevel.WARNING);
        this.currentLevelCombo.getItems().add(LogLevel.INFO);
        this.currentLevelCombo.getItems().add(LogLevel.DEBUG);
        
        this.currentLevelCombo.valueProperty().bindBidirectional(this.currentLevel);

        this.currentLevel.setValue(LogLevel.INFO);
    }

    @Override
    public void logCleared() {
        this.logTextArea.clear();
    }

    @FXML
    void onClearLog(final ActionEvent event) {
        this.logTextArea.clear();
    }

    @Override
    public void lineAdded(final String source, final LogLevel level, final String message) {
        if (!this.currentLevel.getValue().includes(level)){
            return;
        }

        this.logLineBuilder.setLength(0);

        this.logLineBuilder.append(source);
        this.logLineBuilder.append(": ");
        switch (level) {
            case DEBUG:
                this.logLineBuilder.append("DEBUG - ");
                break;
            case INFO:
                this.logLineBuilder.append("INFO  - ");
                break;
            case WARNING:
                this.logLineBuilder.append("WARN  - ");
                break;
            case ERROR:
                this.logLineBuilder.append("ERROR - ");
                break;
            default:
                this.logLineBuilder.append("????? - ");
                break;
        }
        this.logLineBuilder.append(message);
        this.logLineBuilder.append('\n');
        
        this.logTextArea.appendText(this.logLineBuilder.toString());
        
        if (this.logTextArea.getText().length() > LOG_TEXT_LENGTH_LIMIT) {
            this.logTextArea.setText(this.logTextArea.getText().substring(LOG_TEXT_LENGTH_CUTOFF));
        }
    }

}
