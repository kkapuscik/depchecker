package info.octaedr.depchecker.engine.simple;

import info.octaedr.depchecker.model.DirectoryInfo;
import info.octaedr.depchecker.model.LogWriter;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.tools.Path;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Processor utility for resolving the includes.
 */
public class IncludeResolver {

    public void resolveIncludes(final ProjectEntry projectEntry, final ProjectModel projectModel,
            final File baseDirectory, final LogWriter logWriter) {
        final File[] includeDirs = resolveIncludeDirectories(projectModel, baseDirectory);

        final Map<File, InputFileEntry> inputFilesMap = new HashMap<>();
        collectInputFiles(projectEntry, inputFilesMap);

        for (final InputFileEntry inputFile : inputFilesMap.values()) {
            logWriter.debug("Processing: " + inputFile.getInputFile().getAbsolutePath());

            for (final IncludeEntry includeEntry : inputFile.getIncludes()) {
                final File includedFile = resolveInclude(includeEntry, includeDirs,
                        inputFile.getInputFile());

                if (includedFile != null) {
                    final InputFileEntry includedFileEntry = inputFilesMap.get(includedFile);
                    if (includedFileEntry != null) {
                        includeEntry.setIncludedEntry(includedFileEntry);
                    } else {
                        // TODO: store info
                        addExternalInclude(projectEntry, includeEntry);
                    }
                } else {
                    // TODO: store warning
                    addExternalInclude(projectEntry, includeEntry);
                }
            }
        }
    }

    private void addExternalInclude(final ProjectEntry projectEntry, final IncludeEntry includeEntry) {
        ExternalFileEntry externalEntry = projectEntry.getExternalFile(includeEntry.getName());
        if (externalEntry == null) {
            externalEntry = new ExternalFileEntry(includeEntry.getName());
            projectEntry.addExternalFile(externalEntry);
        }

        includeEntry.setIncludedEntry(externalEntry);
    }

    private File resolveInclude(final IncludeEntry includeEntry, final File[] includeDirs,
            final File sourceFile) {
        /* try to resolve against source file */
        if (!includeEntry.isSystemInclude()) {
            final File includeFile = Path.resolve(sourceFile, includeEntry.getName());
            if (includeFile.isFile()) {
                return includeFile;
            }
        }

        /* first resolve against include directories */
        for (final File includeDir : includeDirs) {
            final File includeFile = Path.resolve(includeDir, includeEntry.getName());
            if (includeFile.isFile()) {
                return includeFile;
            }
        }

        /* try to resolve against source file */
        if (includeEntry.isSystemInclude()) {
            final File includeFile = Path.resolve(sourceFile, includeEntry.getName());
            if (includeFile.isFile()) {
                return includeFile;
            }
        }

        return null;
    }

    private File[] resolveIncludeDirectories(final ProjectModel projectModel,
            final File baseDirectory) {
        final ArrayList<File> resolvedIncludeDirs = new ArrayList<>();

        // TODO: hack, but seems that it is Doxygen compatible

        for (final DirectoryInfo includeEntry : projectModel.getIncludeDirectories()) {
            final File includeDir = Path.resolve(baseDirectory, includeEntry.getPath());
            if (includeDir.isDirectory()) {
                resolvedIncludeDirs.add(includeDir);
            } else {
                // TODO: store warning
            }
        }

        for (final DirectoryInfo includeEntry : projectModel.getInputDirectories()) {
            final File includeDir = Path.resolve(baseDirectory, includeEntry.getPath());
            if (includeDir.isDirectory()) {
                resolvedIncludeDirs.add(includeDir);
            } else {
                // TODO: store warning
            }
        }

        return resolvedIncludeDirs.toArray(new File[resolvedIncludeDirs.size()]);
    }

    private void collectInputFiles(final InputContainerEntry inputEntry,
            final Map<File, InputFileEntry> inputFilesMap) {
        for (final InputDirEntry inputDirEntry : inputEntry.getInputDirectories()) {
            collectInputFiles(inputDirEntry, inputFilesMap);
        }

        for (final InputFileEntry inputFileEntry : inputEntry.getInputFiles()) {
            collectInputFiles(inputFileEntry, inputFilesMap);
        }
    }

    private void collectInputFiles(final InputFileEntry inputEntry,
            final Map<File, InputFileEntry> inputFilesMap) {
        final File inputFile = inputEntry.getInputFile();

        if (!inputFilesMap.containsKey(inputFile)) {
            inputFilesMap.put(inputFile, inputEntry);
        } else {
            // TODO: store warning
        }
    }

}
