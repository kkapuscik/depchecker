package info.octaedr.depchecker.engine.simple;

import info.octaedr.depchecker.model.LogWriter;

import java.util.ArrayList;

/**
 * Input data entry being container of input files and directories.
 */
class InputContainerEntry extends Entry {

    final ArrayList<InputDirEntry> inputDirs = new ArrayList<>();

    final ArrayList<InputFileEntry> inputFiles = new ArrayList<>();

    protected InputContainerEntry(final String name) {
        super(name);
    }

    public void addSubItem(final InputDirEntry itemEntry) {
        this.inputDirs.add(itemEntry);
    }

    public void addSubItem(final InputFileEntry itemEntry) {
        this.inputFiles.add(itemEntry);
    }

    public Iterable<InputDirEntry> getInputDirectories() {
        return this.inputDirs;
    }
    
    public Iterable<InputFileEntry> getInputFiles() {
        return this.inputFiles;
    }

    @Override
    protected void dumpContents(final LogWriter logWriter, final int indent) {
        super.dumpContents(logWriter, indent);
        
        logWriter.debug(getDumpIndent(indent) + "- Input Directories:");
        for (final InputDirEntry entry : this.inputDirs) {
            entry.dump(logWriter, indent + 1);
        }

        logWriter.debug(getDumpIndent(indent) + "- Input Files:");
        for (final InputFileEntry entry : this.inputFiles) {
            entry.dump(logWriter, indent + 1);
        }
    }

}
