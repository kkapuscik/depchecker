package info.octaedr.depchecker.engine.simple;

import info.octaedr.depchecker.model.LogWriter;

import java.io.File;
import java.util.ArrayList;

class InputFileEntry extends Entry {

    private final File inputFile;

    private ArrayList<IncludeEntry> includes = new ArrayList<>();

    public InputFileEntry(final String name, final File inputFile) {
        super(name);
        this.inputFile = inputFile;
    }

    public File getInputFile() {
        return this.inputFile;
    }

    public void addIncludes(final IncludeEntry[] newIncludes) {
        for (final IncludeEntry entry : newIncludes) {
            this.includes.add(entry);
        }
    }

    @Override
    protected void dumpContents(final LogWriter logWriter, final int indent) {
        super.dumpContents(logWriter, indent);
        
        logWriter.debug(getDumpIndent(indent) + "- Path:");
        logWriter.debug(getDumpIndent(indent + 1) + this.inputFile.getAbsolutePath());
        
        logWriter.debug(getDumpIndent(indent) + "- Includes:");
        for (final IncludeEntry entry : this.includes) {
            entry.dump(logWriter, indent + 1);
        }
    }

    public Iterable<IncludeEntry> getIncludes() {
        return this.includes;
    }
}
