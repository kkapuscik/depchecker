package info.octaedr.depchecker.engine.simple;

import info.octaedr.depchecker.model.LogWriter;

/**
 * Base class for input data entries.
 */
class Entry {

    /**
     * Size of the dump output indentation in number of space characters.
     */
    private static final int INDENT_SIZE = 4;

    /**
     * Name of the entry.
     */
    private final String name;

    /**
     * Constructs the entry.
     * 
     * @param name
     *            Name of the entry.
     */
    protected Entry(final String name) {
        assert name != null;

        this.name = name;
    }

    /**
     * Returns name of the entry.
     * 
     * @return Name of the entry.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Dumps the debug information about the entry.
     * 
     * @param logWriter
     *            Output writer object.
     */
    public final void dump(final LogWriter logWriter) {
        dump(logWriter, 0);
    }

    /**
     * Dumps the debug information about the entry.
     * 
     * @param logWriter
     *            Output writer object.
     * @param indent
     *            Indentation level.
     */
    protected final void dump(final LogWriter logWriter, final int indent) {
        logWriter.debug(getDumpIndent(indent) + "[" + getClass().getSimpleName() + " name=" + getName() + "]");
        dumpContents(logWriter, indent);
    }

    /**
     * Dumps contents of this entry.
     * 
     * <p>
     * This method is expected to be overridden by derived classes. The dump
     * shall not include name of the entry or class information as it is printed
     * automatically.
     * 
     * @param logWriter
     *            Output writer object.
     * @param indent
     *            Indentation level.
     */
    protected void dumpContents(final LogWriter logWriter, final int indent) {
        // nothing to do
    }

    /**
     * Returns dump indentation string.
     * 
     * @param level
     *            Indentation level.
     * 
     * @return Indentation string (spaces are used as the indentation
     *         character).
     */
    protected final String getDumpIndent(final int level) {
        final StringBuffer outputBuffer = new StringBuffer(level * INDENT_SIZE);
        for (int i = 0; i < level * INDENT_SIZE; ++i) {
            outputBuffer.append(' ');
        }
        return outputBuffer.toString();
    }

}
