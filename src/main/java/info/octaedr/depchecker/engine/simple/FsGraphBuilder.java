package info.octaedr.depchecker.engine.simple;

import info.octaedr.depchecker.engine.graph.DependencyGraph;
import info.octaedr.depchecker.engine.graph.DependencyGraphException;
import info.octaedr.depchecker.engine.graph.Vertex;
import info.octaedr.depchecker.model.InputGraph;

import java.util.HashMap;
import java.util.Map;

public class FsGraphBuilder {

    private static final String INPUT_VERTEX_NAME = "Input";
    private static final String EXTERNAL_VERTEX_NAME = "External";

    public InputGraph buildGraph(final ProjectEntry projectEntry) {
        try {
            final Map<Entry, Vertex> vertexMap = new HashMap<>();

            final DependencyGraph graph = new DependencyGraph();
            graph.setName(projectEntry.getName());

            addVertices(graph, projectEntry, vertexMap);
            addRelations(graph, projectEntry, vertexMap);

            return new InputGraph(Constants.GRAPH_LOCATOR_INCLUDE_DEPS, graph);
        } catch (final DependencyGraphException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void addVertices(final DependencyGraph graph, final ProjectEntry projectEntry,
            final Map<Entry, Vertex> vertexMap) throws DependencyGraphException {
        final Vertex inputVertex = createVertex(graph, INPUT_VERTEX_NAME, vertexMap);
        addInputVertices(graph, projectEntry, vertexMap, inputVertex);

        final Vertex externalVertex = createVertex(graph, EXTERNAL_VERTEX_NAME, vertexMap);
        addExternalVertices(graph, projectEntry, vertexMap, externalVertex);
    }

    private void addExternalVertices(final DependencyGraph graph, final ProjectEntry projectEntry,
            final Map<Entry, Vertex> vertexMap, final Vertex parentVertex)
            throws DependencyGraphException {
        for (final ExternalFileEntry entry : projectEntry.getExternalFiles()) {
            /* add vertex */
            final Vertex vertex = createVertex(false, graph, entry, vertexMap);

            /* add structure relation */
            graph.addStructureEdge(parentVertex, vertex);
        }
    }

    private void addInputVertices(final DependencyGraph graph,
            final InputContainerEntry containerEntry, final Map<Entry, Vertex> vertexMap,
            final Vertex parentVertex) throws DependencyGraphException {
        for (final InputDirEntry entry : containerEntry.getInputDirectories()) {
            /* add vertex */
            final Vertex vertex = createVertex(true, graph, entry, vertexMap);

            /* add structure relation */
            graph.addStructureEdge(parentVertex, vertex);

            /*
             * add structure
             * 
             * /* process recursively
             */
            addInputVertices(graph, entry, vertexMap, vertex);
        }

        for (final InputFileEntry entry : containerEntry.getInputFiles()) {
            /* add vertex */
            final Vertex vertex = createVertex(false, graph, entry, vertexMap);

            /* add structure relation */
            graph.addStructureEdge(parentVertex, vertex);
        }
    }

    private Vertex createVertex(final DependencyGraph graph, final String name,
            final Map<Entry, Vertex> vertexMap) throws DependencyGraphException {
        /* create the vertex */
        final Vertex vertex = new Vertex("root" + name);
        vertex.setCluster(true);
        vertex.setName(name);

        /* add to graph */
        graph.addVertex(vertex);

        return vertex;
    }

    private Vertex createVertex(final boolean clusterVertex, final DependencyGraph graph,
            final Entry entry, final Map<Entry, Vertex> vertexMap) throws DependencyGraphException {
        /* create the vertex */
        final Vertex vertex = new Vertex("" + vertexMap.size());
        vertex.setCluster(clusterVertex);
        vertex.setName(entry.getName());

        /* add to graph */
        graph.addVertex(vertex);
        vertexMap.put(entry, vertex);

        return vertex;
    }

    private void addRelations(final DependencyGraph graph, final ProjectEntry projectEntry,
            final Map<Entry, Vertex> vertexMap) {
        addInputRelations(graph, projectEntry, vertexMap);
    }

    private void addInputRelations(final DependencyGraph graph,
            final InputContainerEntry containerEntry, final Map<Entry, Vertex> vertexMap) {
        for (final InputDirEntry entry : containerEntry.getInputDirectories()) {
            addInputRelations(graph, entry, vertexMap);
        }

        for (final InputFileEntry entry : containerEntry.getInputFiles()) {
            /* add structure relation */
            addInputRelations(graph, entry, vertexMap);
        }
    }

    private void addInputRelations(final DependencyGraph graph, final InputFileEntry entry,
            final Map<Entry, Vertex> vertexMap) {

        final Vertex sourceVertex = vertexMap.get(entry);
        if (sourceVertex != null) {
            for (final IncludeEntry incEntry : entry.getIncludes()) {
                final Entry targetEntry = incEntry.getIncludedEntry();
                if (targetEntry != null) {
                    final Vertex targetVertex = vertexMap.get(targetEntry);
                    if (targetVertex != null) {
                        graph.addRelationEdge(sourceVertex, targetVertex);
                    }
                }
            }
        }
    }

}
