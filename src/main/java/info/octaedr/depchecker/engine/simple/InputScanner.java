package info.octaedr.depchecker.engine.simple;

import info.octaedr.depchecker.model.DirectoryInfo;
import info.octaedr.depchecker.model.FileInfo;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.tools.Path;

import java.io.File;

/**
 * Processor utility for scanning the input directories.
 */
class InputScanner {
    
    public void listInput(final ProjectEntry projectEntry, final ProjectModel projectModel,
            final File baseDirectory) {
        final boolean isInputRecursive = projectModel.getInputRecursive();
        
        for (final DirectoryInfo inputDirectory : projectModel.getInputDirectories()) {
            final InputDirEntry entry = listInputDirectory(inputDirectory, isInputRecursive,
                    baseDirectory);
            
            if (entry != null) {
                projectEntry.addSubItem(entry);
            }
        }
        
        for (final FileInfo inputFile : projectModel.getInputFiles()) {
            final InputFileEntry entry = listInputFile(inputFile, baseDirectory);
            
            if (entry != null) {
                projectEntry.addSubItem(entry);
            }
        }
    }

    private InputFileEntry listInputFile(final FileInfo inputFile, final File baseDirectory) {
        final File inputFilePath = Path.resolve(baseDirectory, inputFile.getPath());
        
        return listInputFile(inputFilePath);
    }

    private InputDirEntry listInputDirectory(final DirectoryInfo inputDirectory,
            final boolean recursive, final File baseDirectory) {
        final File inputDirectoryPath = Path.resolve(baseDirectory, inputDirectory.getPath());
        
        return listInputDirectory(inputDirectoryPath, recursive);
    }

    private InputDirEntry listInputDirectory(final File inputDirectory,
            final boolean recursive) {
        if (!inputDirectory.exists() || !inputDirectory.isDirectory()) {
            // TODO: store warning
            return null;
        }
        
        final InputDirEntry directoryEntry = new InputDirEntry(inputDirectory.getName());
        
        final File[] directoryItems = inputDirectory.listFiles();
        for (final File item : directoryItems) {
            if (item.isDirectory() && recursive) {
                final InputDirEntry itemEntry = listInputDirectory(item, recursive); 
                if (itemEntry != null) {
                    directoryEntry.addSubItem(itemEntry);
                }
            } else if (item.isFile()) {
                final InputFileEntry itemEntry = listInputFile(item);
                if (itemEntry != null) {
                    directoryEntry.addSubItem(itemEntry);
                }
            }
        }
        
        return directoryEntry;
    }

    private InputFileEntry listInputFile(final File inputFile) {
        if (!inputFile.exists() || !inputFile.isFile()) {
            // TODO: store warning
            return null;
        }
        
        final InputFileEntry fileEntry = new InputFileEntry(inputFile.getName(), inputFile);
        
        return fileEntry;
    }

}
