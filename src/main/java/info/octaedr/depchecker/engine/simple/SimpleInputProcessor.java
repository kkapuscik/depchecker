package info.octaedr.depchecker.engine.simple;

import info.octaedr.depchecker.engine.InputProcessor;
import info.octaedr.depchecker.model.InputGraph;
import info.octaedr.depchecker.model.InputGraphDescriptor;
import info.octaedr.depchecker.model.LogWriter;
import info.octaedr.depchecker.model.ProjectModel;

import java.io.File;
import java.util.ArrayList;

/**
 * Simple processor for input data.
 */
public class SimpleInputProcessor implements InputProcessor {

    /**
     * Array of locators to produced graphs.
     */
    private static final InputGraphDescriptor[] PRODUCED_GRAPH_DESCRIPTORS = new InputGraphDescriptor[] { new InputGraphDescriptor(
            Constants.GRAPH_LOCATOR_INCLUDE_DEPS, Constants.GRAPH_NAME_INCLUDE_DEPS) };

    /**
     * Utility for scanning input directories and files.
     */
    private final InputScanner inputScanner;

    /**
     * Utility for extracting include entries in files.
     */
    private final IncludeExtractor includeExtractor;

    /**
     * Utility for resolving include entries.
     */
    private final IncludeResolver includeResolver;

    /**
     * Utility for building file system dependencies graphs.
     */
    private final FsGraphBuilder fsGraphBuilder;

    /**
     * Constructs the processor.
     */
    public SimpleInputProcessor() {
        this.inputScanner = new InputScanner();
        this.includeExtractor = new IncludeExtractor();
        this.includeResolver = new IncludeResolver();
        this.fsGraphBuilder = new FsGraphBuilder();
    }

    @Override
    public String getUniqueId() {
        return Constants.PROCESSOR_UNIQUE_ID;
    }

    @Override
    public String getName() {
        return Constants.PROCESSOR_NAME;
    }

    @Override
    public InputGraphDescriptor[] getGraphDescriptors() {
        return PRODUCED_GRAPH_DESCRIPTORS;
    }

    @Override
    public InputGraph[] process(final ProjectModel projectModel, final File baseDirectory,
            final File outputDirectory, final LogWriter logWriter) {

        logWriter.info("Processing started");
        logWriter.info("- input: " + projectModel.getProjectFile().getAbsolutePath());

        logWriter.info("Constructing project entry");

        final ProjectEntry projectEntry = new ProjectEntry(projectModel.getProjectName());

        logWriter.info("Listing input files");

        this.inputScanner.listInput(projectEntry, projectModel, baseDirectory);

        logWriter.info("Extracting includes");

        this.includeExtractor.extractIncludes(projectEntry);

        logWriter.info("Resolving includes");

        this.includeResolver.resolveIncludes(projectEntry, projectModel, baseDirectory, logWriter);

        logWriter.info("Dumping parsed contents");

        projectEntry.dump(logWriter);

        logWriter.info("Building FS graph");

        final ArrayList<InputGraph> graphs = new ArrayList<>();

        final InputGraph fsGraph = this.fsGraphBuilder.buildGraph(projectEntry);
        if (fsGraph != null) {
            graphs.add(fsGraph);
        }

        final InputGraph[] results = graphs.toArray(new InputGraph[graphs.size()]);
        
        logWriter.info("Processing complete");

        return results;
    }

}
