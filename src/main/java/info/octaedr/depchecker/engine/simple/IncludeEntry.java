package info.octaedr.depchecker.engine.simple;

import info.octaedr.depchecker.model.LogWriter;

class IncludeEntry extends Entry {

    private final boolean systemInclude;
    private Entry includedEntry;

    public IncludeEntry(final String fileName, final boolean systemInclude) {
        super(fileName);
        this.systemInclude = systemInclude;
    }

    public boolean isSystemInclude() {
        return this.systemInclude;
    }

    public void setIncludedEntry(final Entry includedEntry) {
        this.includedEntry = includedEntry;
    }

    @Override
    protected void dumpContents(final LogWriter logWriter, final int indent) {
        super.dumpContents(logWriter, indent);

        if (isSystemInclude()) {
            logWriter.debug(getDumpIndent(indent) + "- System");
        } else {
            logWriter.debug(getDumpIndent(indent) + "- Local");
        }
        logWriter.debug(getDumpIndent(indent) + "- Included Entry:");
        if (this.includedEntry != null) {
            if (this.includedEntry instanceof InputFileEntry) {
                final InputFileEntry inputFile = (InputFileEntry) this.includedEntry;
                logWriter.debug(getDumpIndent(indent + 1)
                        + inputFile.getInputFile().getAbsolutePath());
            } else {
                logWriter.debug(getDumpIndent(indent + 1) + this.includedEntry.getName());
            }
        } else {
            logWriter.debug(getDumpIndent(indent + 1) + "- none:");
        }
    }

    public Entry getIncludedEntry() {
        return this.includedEntry;
    }

}
