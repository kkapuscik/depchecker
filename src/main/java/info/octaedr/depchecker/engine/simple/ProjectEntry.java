package info.octaedr.depchecker.engine.simple;

import info.octaedr.depchecker.model.LogWriter;

import java.util.HashMap;
import java.util.Map;

/**
 * Input data entry describing the project.
 */
class ProjectEntry extends InputContainerEntry {

    private final Map<String, ExternalFileEntry> externalFiles = new HashMap<>();

    public ProjectEntry(final String name) {
        super(name);
    }

    public void addExternalFile(final ExternalFileEntry externalEntry) {
        this.externalFiles.put(externalEntry.getName(), externalEntry);
    }

    @Override
    protected void dumpContents(final LogWriter logWriter, int indent) {
        super.dumpContents(logWriter, indent);

        logWriter.debug(getDumpIndent(indent) + "- External Files:");
        for (final ExternalFileEntry entry : this.externalFiles.values()) {
            entry.dump(logWriter, indent + 1);
        }
    }

    public ExternalFileEntry getExternalFile(final String name) {
        return this.externalFiles.get(name);
    }

    public Iterable<ExternalFileEntry> getExternalFiles() {
        return this.externalFiles.values();
    }
}
