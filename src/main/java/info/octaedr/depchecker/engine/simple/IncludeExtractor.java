package info.octaedr.depchecker.engine.simple;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Utility for extracting include entries from files.
 */
class IncludeExtractor {

    public void extractIncludes(final InputContainerEntry inputEntry) {
        for (final InputDirEntry inputDir : inputEntry.getInputDirectories()) {
            extractIncludes(inputDir);
        }
        
        for (final InputFileEntry inputFile : inputEntry.getInputFiles()) {
            extractIncludes(inputFile);
        }
    }

    private void extractIncludes(final InputFileEntry inputEntry) {
        final IncludeEntry[] includes = extractIncludes(inputEntry.getInputFile()); 
        
        if (includes != null) {
            inputEntry.addIncludes(includes);
        }
    }

    private IncludeEntry[] extractIncludes(final File inputFile) {
        IncludeEntry[] includeArray = null;
        
        BufferedReader reader = null;
        
        try {
            reader = new BufferedReader(new FileReader(inputFile));
            
            final ArrayList<IncludeEntry> includeList = new ArrayList<>();

            for (;;) {
                final String line = reader.readLine();
                if (line == null) {
                    break;
                }
                
                final String trimmedLine = line.trim();
                if (trimmedLine.startsWith("#include")) {
                    final String includeData = line.substring("#include".length()).trim();
                    if (includeData.startsWith("\"")) {
                        final int endIndex = includeData.indexOf('"', 1);
                        if (endIndex > 0) {
                            final String includeName = includeData.substring(1, endIndex);
                            
                            includeList.add(new IncludeEntry(includeName, false));
                        } else {
                            // TODO: store warning
                        }
                    } else if (includeData.startsWith("<")) {
                        final int endIndex = includeData.indexOf('>', 1);
                        if (endIndex > 0) {
                            final String includeName = includeData.substring(1, endIndex);
                            
                            includeList.add(new IncludeEntry(includeName, true));
                        } else {
                            // TODO: store warning
                        }
                    } else {
                        // TODO: store warning
                    }
                }
            }
            
            includeArray = includeList.toArray(new IncludeEntry[includeList.size()]);
        } catch (final FileNotFoundException e) {
            // TODO: store warning
        } catch (final IOException e) {
            // TODO: store warning
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // TODO: store warning
                }
            }
        }
        
        return includeArray;
    }

}
