package info.octaedr.depchecker.engine.doxygen.fsgraph;

import java.util.ArrayList;

import info.octaedr.doxygen.xml.CompounddefType;

/**
 * Directory entry descriptor.
 * 
 * <p>
 * The directory entries represent directories from doxygen data and are used as
 * the dependency sub-graph data source.
 */
class DirEntry {
    private final String uniqueId;
    private final CompounddefType compound;
    private final ArrayList<FileEntry> innerFiles;
    private final ArrayList<DirEntry> innerDirs;
    private int refCount = 0;

    public DirEntry(final String uniqueId, final CompounddefType compound) {
        assert compound != null;
        assert uniqueId != null;

        this.uniqueId = uniqueId;
        this.compound = compound;
        this.innerFiles = new ArrayList<FileEntry>();
        this.innerDirs = new ArrayList<DirEntry>();
    }

    protected DirEntry(final String uniqueId) {
        assert uniqueId != null;

        this.uniqueId = uniqueId;
        this.compound = null;
        this.innerFiles = new ArrayList<FileEntry>();
        this.innerDirs = new ArrayList<DirEntry>();
    }

    public String getUniqueId() {
        return this.uniqueId;
    }

    public String getName() {
        return this.compound.getCompoundname();
    }

    public CompounddefType getCompound() {
        return this.compound;
    }

    public void incRefCount() {
        ++this.refCount;
    }

    public int getRefCount() {
        return this.refCount;
    }

    public void addInnerDir(final DirEntry refDir) {
        this.innerDirs.add(refDir);
    }

    public void addInnerFile(final FileEntry refFile) {
        this.innerFiles.add(refFile);
    }

    public Iterable<FileEntry> getInnerFiles() {
        return this.innerFiles;
    }

    public Iterable<DirEntry> getInnerDirs() {
        return this.innerDirs;
    }
}
