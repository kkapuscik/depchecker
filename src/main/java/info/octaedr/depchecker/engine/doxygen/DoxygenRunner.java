package info.octaedr.depchecker.engine.doxygen;

import info.octaedr.depchecker.model.LogWriter;
import info.octaedr.doxygen.BuildSettingsGroup;
import info.octaedr.doxygen.DiagnosticSettingsGroup;
import info.octaedr.doxygen.DoxygenExecutor;
import info.octaedr.doxygen.InputSettingsGroup;
import info.octaedr.doxygen.OutputHtmlSettingsGroup;
import info.octaedr.doxygen.OutputLatexSettingsGroup;
import info.octaedr.doxygen.OutputXmlSettingsGroup;
import info.octaedr.doxygen.PreprocessorSettingsGroup;
import info.octaedr.doxygen.ProjectSettingsGroup;
import info.octaedr.doxygen.Settings;
import info.octaedr.doxygen.SettingsFile;
import info.octaedr.doxygen.SourceBrowserSettingsGroup;

import java.io.File;
import java.util.ArrayList;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class DoxygenRunner {

    private static final String COMBINED_XML_FILE_NAME = "all.xml";

    private static final String COMBINE_XML_INDEX_FILE_NAME = "index.xml";

    private static final String COMBINE_XSLT_FILE_NAME = "combine.xslt";

    private static final String XML_DIRECTORY_NAME = "xml";

    private File workingDirectory = null;

    private String projectName = null;
    private boolean builtinStlSupport = true;
    private File outputDirectory = null;

    private boolean inputRecursive = false;
    private ArrayList<File> inputEntries = new ArrayList<File>();
    private ArrayList<File> includeEntries = new ArrayList<File>();

    private final LogWriter logWriter;

    public DoxygenRunner(final LogWriter logWriter) {
        assert logWriter != null;
        
        this.logWriter = logWriter;
    }

    public File getProducedFilePath() {
        final File xmlDirectory = computeXmlDirectory();

        return new File(xmlDirectory, COMBINED_XML_FILE_NAME);
    }

    public boolean run() {
        if (this.workingDirectory == null) {
            this.logWriter.debug("Working directory not set");
            return false;
        }

        if (!this.workingDirectory.exists()) {
            if (!this.workingDirectory.mkdir()) {
                this.logWriter.debug("Cannot create working directory: " + this.workingDirectory);
                return false;
            }
        } else if (!this.workingDirectory.isDirectory()) {
            this.logWriter.debug("Invalid working directory: " + this.workingDirectory);
        }

        final Settings settings = createDoxygenSettings();
        final SettingsFile doxyFile = new SettingsFile(new File(this.workingDirectory, "Doxyfile"));

        if (!doxyFile.saveSettings(settings)) {
            this.logWriter.debug("Cannot save settings in file: "
                    + doxyFile.getFile().getAbsolutePath());
        }

        final DoxygenExecutor executor = new DoxygenExecutor();
        executor.setLogWriter(this.logWriter);
        executor.setWorkingDirectory(this.workingDirectory);
        if (!executor.run()) {
            return false;
        }

        return combineOutput();
    }

    private boolean combineOutput() {
        boolean success = false;

        try {
            final File xmlDirectory = computeXmlDirectory();

            TransformerFactory factory = TransformerFactory.newInstance();
            Source xslt = new StreamSource(new File(xmlDirectory, COMBINE_XSLT_FILE_NAME));
            Transformer transformer = factory.newTransformer(xslt);
            
            String allPath = xmlDirectory.getAbsolutePath()+ File.separator +
                    COMBINED_XML_FILE_NAME;
            
            Source text = new StreamSource(new File(xmlDirectory, COMBINE_XML_INDEX_FILE_NAME));
            transformer.transform(text, new StreamResult(allPath));

            success = true;
        } catch (final TransformerException e) {
            this.logWriter.debug("Transformer exception: ");
            this.logWriter.debug("- location: " + e.getLocationAsString());
            if (e.getLocator() != null) {
                this.logWriter.debug("- pubid: " + e.getLocator().getPublicId());
                this.logWriter.debug("- sysid: " + e.getLocator().getSystemId());
                this.logWriter.debug("- line:  " + e.getLocator().getLineNumber());
                this.logWriter.debug("- col:   " + e.getLocator().getColumnNumber());
            }
            e.printStackTrace();
        }

        return success;
    }

    private File computeXmlDirectory() {
        File xmlDirectory;

        if (this.outputDirectory != null) {
            xmlDirectory = new File(this.outputDirectory, XML_DIRECTORY_NAME);
        } else {
            xmlDirectory = new File(this.workingDirectory, XML_DIRECTORY_NAME);
        }

        return xmlDirectory;
    }

    public void addInputEntry(final File inputPath) {
        assert inputPath != null;

        this.inputEntries.add(inputPath);
    }

    public boolean addIncludeEntry(final File includePath) {
        assert includePath != null;

        if (!includePath.isDirectory()) {
            return false;
            // TODO throw new
            // IllegalArgumentException("Path is not a directory");
        }

        this.includeEntries.add(includePath);

        return true;
    }

    public String getProjectName() {
        return this.projectName;
    }

    public void setProjectName(final String projectName) {
        this.projectName = projectName;
    }

    public boolean isInputRecursive() {
        return this.inputRecursive;
    }

    public void setInputRecursive(final boolean inputRecursive) {
        this.inputRecursive = inputRecursive;
    }

    public File getOutputDirectory() {
        return this.outputDirectory;
    }

    public void setOutputDirectory(final File outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    public boolean isBuiltinStlSupport() {
        return this.builtinStlSupport;
    }

    public void setBuiltinStlSupport(final boolean builtinStlSupport) {
        this.builtinStlSupport = builtinStlSupport;
    }

    public File getWorkingDirectory() {
        return this.workingDirectory;
    }

    public void setWorkingDirectory(final File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    private Settings createDoxygenSettings() {
        final Settings doxygenSettings = new Settings();

        setProjectSettings(doxygenSettings);

        setInputSettings(doxygenSettings);

        setOutputHtmlSettings(doxygenSettings);
        setOutputLatexSettings(doxygenSettings);
        setOutputXmlSettings(doxygenSettings);

        setBuildSettings(doxygenSettings);
        setPreprocessorSettings(doxygenSettings);
        setSourceBrowserSettings(doxygenSettings);
        setDiagnosticSettings(doxygenSettings);

        return doxygenSettings;
    }

    private void setSourceBrowserSettings(final Settings doxygenSettings) {
        final SourceBrowserSettingsGroup settingsGroup = new SourceBrowserSettingsGroup(
                doxygenSettings);

        settingsGroup.set(SourceBrowserSettingsGroup.VERBATIM_HEADERS, Settings.VALUE_NO);
    }

    private void setProjectSettings(final Settings doxygenSettings) {
        final ProjectSettingsGroup settingsGroup = new ProjectSettingsGroup(doxygenSettings);

        if (this.projectName != null) {
            settingsGroup.set(ProjectSettingsGroup.PROJECT_NAME, this.projectName);
        } else {
            settingsGroup.set(ProjectSettingsGroup.PROJECT_NAME, "-unnamed-");
        }
        // settingsGroup.set(ProjectSettingsGroup.PROJECT_NUMBER, "");
        // settingsGroup.set(ProjectSettingsGroup.PROJECT_BRIEF, "");
        if (this.outputDirectory != null) {
            settingsGroup.set(ProjectSettingsGroup.OUTPUT_DIRECTORY,
                    '"' + this.outputDirectory.getAbsolutePath() + '"');
        }
        // preprocessorSettings.set(ProjectSettingsGroup.OUTPUT_LANGUAGE, "");
        settingsGroup.set(ProjectSettingsGroup.ALWAYS_DETAILED_SEC, Settings.VALUE_YES);
        // preprocessorSettings.set(ProjectSettingsGroup.INLINE_INHERITED_MEMB,
        // Settings.VALUE_YES); - for inherited dependencies?

        // settingsGroup.set(ProjectSettingsGroup.JAVADOC_AUTOBRIEF,
        // Settings.VALUE_YES);
        // settingsGroup.set(ProjectSettingsGroup.QT_AUTOBRIEF,
        // Settings.VALUE_YES);

        // preprocessorSettings.set(ProjectSettingsGroup.OPTIMIZE_OUTPUT_FOR_C,
        // Settings.VALUE_YES);
        // preprocessorSettings.set(ProjectSettingsGroup.OPTIMIZE_OUTPUT_JAVA,
        // Settings.VALUE_YES);
        // preprocessorSettings.set(ProjectSettingsGroup.OPTIMIZE_FOR_FORTRAN,
        // Settings.VALUE_YES);
        // preprocessorSettings.set(ProjectSettingsGroup.OPTIMIZE_OUTPUT_VHDL,
        // Settings.VALUE_YES);

        // preprocessorSettings.set(ProjectSettingsGroup.EXTENSION_MAPPING, "");

        if (this.builtinStlSupport) {
            settingsGroup.set(ProjectSettingsGroup.BUILTIN_STL_SUPPORT, Settings.VALUE_YES);
        } else {
            settingsGroup.set(ProjectSettingsGroup.BUILTIN_STL_SUPPORT, Settings.VALUE_NO);
        }
    }

    private void setPreprocessorSettings(final Settings doxygenSettings) {
        final PreprocessorSettingsGroup preprocessorSettings = new PreprocessorSettingsGroup(
                doxygenSettings);

        preprocessorSettings.set(PreprocessorSettingsGroup.SEARCH_INCLUDES, Settings.VALUE_YES);
        preprocessorSettings.setSearchIncludes(this.includeEntries
                .toArray(new File[this.includeEntries.size()]));

        preprocessorSettings.set(PreprocessorSettingsGroup.PREDEFINED, "");
        // // TODO
    }

    private void setOutputXmlSettings(final Settings doxygenSettings) {
        final OutputXmlSettingsGroup outputSettings = new OutputXmlSettingsGroup(doxygenSettings);

        outputSettings.set(OutputXmlSettingsGroup.GENERATE_XML, Settings.VALUE_YES);
        outputSettings.set(OutputXmlSettingsGroup.XML_OUTPUT, XML_DIRECTORY_NAME);
        outputSettings.set(OutputXmlSettingsGroup.XML_PROGRAMLISTING, Settings.VALUE_NO);
    }

    private void setOutputLatexSettings(final Settings doxygenSettings) {
        final OutputLatexSettingsGroup outputSettings = new OutputLatexSettingsGroup(
                doxygenSettings);

        outputSettings.set(OutputLatexSettingsGroup.GENERATE_LATEX, Settings.VALUE_NO);
    }

    private void setOutputHtmlSettings(final Settings doxygenSettings) {
        final OutputHtmlSettingsGroup outputSettings = new OutputHtmlSettingsGroup(doxygenSettings);

        outputSettings.set(OutputHtmlSettingsGroup.GENERATE_HTML, Settings.VALUE_NO);
    }

    private void setInputSettings(final Settings doxygenSettings) {
        final InputSettingsGroup inputSettings = new InputSettingsGroup(doxygenSettings);

        inputSettings.setInput(this.inputEntries.toArray(new File[this.inputEntries.size()]));

        // inputSettings.set(InputSettingsGroup.FILE_PATTERNS, "");

        if (this.inputRecursive) {
            inputSettings.set(InputSettingsGroup.RECURSIVE, Settings.VALUE_YES);
        } else {
            inputSettings.set(InputSettingsGroup.RECURSIVE, Settings.VALUE_NO);
        }
    }

    private void setBuildSettings(final Settings doxygenSettings) {
        BuildSettingsGroup buildSettings = new BuildSettingsGroup(doxygenSettings);

        buildSettings.set(BuildSettingsGroup.EXTRACT_ALL, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.EXTRACT_PRIVATE, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.EXTRACT_PACKAGE, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.EXTRACT_STATIC, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.EXTRACT_LOCAL_CLASSES, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.EXTRACT_LOCAL_METHODS, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.EXTRACT_ANON_NSPACES, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.INTERNAL_DOCS, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.SHOW_INCLUDE_FILES, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.SHOW_USED_FILES, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.SHOW_FILES, Settings.VALUE_YES);
        buildSettings.set(BuildSettingsGroup.SHOW_NAMESPACES, Settings.VALUE_YES);
    }

    private void setDiagnosticSettings(final Settings doxygenSettings) {
        final DiagnosticSettingsGroup diagnosticSettings = new DiagnosticSettingsGroup(
                doxygenSettings);

        diagnosticSettings.set(DiagnosticSettingsGroup.WARNINGS, Settings.VALUE_YES);
        diagnosticSettings.set(DiagnosticSettingsGroup.WARN_NO_PARAMDOC, Settings.VALUE_YES);
    }

}
