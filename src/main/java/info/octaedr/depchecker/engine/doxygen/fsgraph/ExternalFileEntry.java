package info.octaedr.depchecker.engine.doxygen.fsgraph;

import info.octaedr.depchecker.engine.graph.Vertex;
import info.octaedr.doxygen.xml.IncType;

/**
 * External include file descriptor.
 */
class ExternalFileEntry {

    /**
     * Unique identifier of the entry.
     */
    private final String uniqueId;

    /**
     * Object for which this entry was created.
     */
    private final IncType include;

    private Vertex vertex;

    /**
     * Constructs the entry.
     * 
     * @param uniqueId
     *            Unique identifier
     * @param include
     *            Object for which entry is created.
     */
    public ExternalFileEntry(final String uniqueId, final IncType include) {
        assert uniqueId != null;
        assert include != null;

        this.uniqueId = uniqueId;
        this.include = include;
    }

    /**
     * Returns unique identifier of the entry.
     * 
     * @return The unique identifier.
     */
    public String getUniqueId() {
        return this.uniqueId;
    }

    /**
     * Returns entry name.
     * 
     * @return Name of the entry.
     */
    public String getName() {
        return this.include.getValue();
    }

    public void setVertex(final Vertex vertex) {
        this.vertex = vertex;
    }

    public Vertex getVertex() {
        return this.vertex;
    }

}
