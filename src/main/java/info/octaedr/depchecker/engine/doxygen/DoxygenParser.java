package info.octaedr.depchecker.engine.doxygen;

import info.octaedr.depchecker.FatalException;
import info.octaedr.doxygen.xml.DoxygenType;

import java.io.File;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class DoxygenParser {

    private final SchemaFactory schemaFactory;
    private final JAXBContext jaxbContext;

    public DoxygenParser() {
        this.schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try {
            this.jaxbContext = JAXBContext.newInstance(DoxygenType.class.getPackage().getName());
        } catch (JAXBException e) {
            throw new FatalException("Cannot create JAXB context");
        }
    }

    public DoxygenType parse(final File inputFile) {
        assert inputFile != null;

        DoxygenType result = null;

        try {
            final DoxygenType doxygenData = deserialize(inputFile);
            if (doxygenData != null) {
                result = doxygenData;
            }
        } catch (final Throwable t) {
            t.printStackTrace();
        }

        return result;
    }

    private DoxygenType deserialize(final File inputFile) {
        assert inputFile != null;

        DoxygenType result = null;

        try {
            final URL schemaUrl = getClass().getClassLoader().getResource("doxygen-compound.xsd");
            if (schemaUrl == null) {
                throw new JAXBException("Schema resource is not available");
            }

            final Schema xmlSchema = this.schemaFactory.newSchema(schemaUrl);

            final Unmarshaller unmarshaller = this.jaxbContext.createUnmarshaller();
            unmarshaller.setSchema(xmlSchema);

            final Object unmarshalResult = unmarshaller.unmarshal(inputFile);
            if (!(unmarshalResult instanceof JAXBElement)) {
                throw new JAXBException("Invalid unmarshaling result (not a JAXBElement)");
            }

            final JAXBElement<?> projectElement = (JAXBElement<?>) unmarshalResult;
            if (!(projectElement.getValue() instanceof DoxygenType)) {
                throw new JAXBException("Invalid unmarshaling result (not a DoxygenType)");
            }

            result = (DoxygenType) projectElement.getValue();
        } catch (final JAXBException e) {
            // TODO
            e.printStackTrace();
        } catch (final SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }

}
