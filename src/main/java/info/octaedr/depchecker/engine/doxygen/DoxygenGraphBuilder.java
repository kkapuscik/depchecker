package info.octaedr.depchecker.engine.doxygen;

import info.octaedr.depchecker.engine.graph.DependencyGraph;
import info.octaedr.doxygen.xml.DoxygenType;

public interface DoxygenGraphBuilder {

    DependencyGraph buildGraph(DoxygenType doxygenData);
    
}
