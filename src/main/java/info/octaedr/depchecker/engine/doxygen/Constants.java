package info.octaedr.depchecker.engine.doxygen;

import info.octaedr.depchecker.model.InputGraphLocator;

/**
 * Collection of Doxygen Input Processor related constants.
 */
class Constants {

    /**
     * Unique identifier of the processor.
     */
    static final String PROCESSOR_UNIQUE_ID = "DOXYGEN";

    /**
     * Name of the processor.
     */
    static final String PROCESSOR_NAME = "Doxygen";

    /**
     * Include dependencies unique graph id.
     */
    static final String GRAPH_ID_INCLUDE_DEPS = "INC_DEPS";

    /**
     * Include dependencies graph name.
     */
    static final String GRAPH_NAME_INCLUDE_DEPS = "Include Dependencies";

    /**
     * Include dependencies graph locator.
     */
    static final InputGraphLocator GRAPH_LOCATOR_INCLUDE_DEPS = new InputGraphLocator(
            PROCESSOR_UNIQUE_ID, GRAPH_ID_INCLUDE_DEPS);

}
