package info.octaedr.depchecker.engine.doxygen;

import info.octaedr.depchecker.engine.InputProcessor;
import info.octaedr.depchecker.engine.doxygen.fsgraph.DoxygenFsGraphBuilder;
import info.octaedr.depchecker.engine.graph.DependencyGraph;
import info.octaedr.depchecker.model.DirectoryInfo;
import info.octaedr.depchecker.model.FileInfo;
import info.octaedr.depchecker.model.InputGraph;
import info.octaedr.depchecker.model.InputGraphDescriptor;
import info.octaedr.depchecker.model.LogWriter;
import info.octaedr.depchecker.model.ProjectModel;
import info.octaedr.depchecker.tools.Path;
import info.octaedr.doxygen.xml.DoxygenType;

import java.io.File;

/**
 * Input processor using doxygen for parsing.
 */
public class DoxygenInputProcessor implements InputProcessor {

    /**
     * Array of locators to produced graphs.
     */
    private static final InputGraphDescriptor[] PRODUCED_GRAPH_DESCRIPTORS = new InputGraphDescriptor[] { new InputGraphDescriptor(
            Constants.GRAPH_LOCATOR_INCLUDE_DEPS, Constants.GRAPH_NAME_INCLUDE_DEPS) };

    /**
     * Constructs the processor.
     */
    public DoxygenInputProcessor() {
        // nothing to do
    }

    @Override
    public String getUniqueId() {
        return Constants.PROCESSOR_UNIQUE_ID;
    }

    @Override
    public String getName() {
        return Constants.PROCESSOR_NAME;
    }

    @Override
    public InputGraphDescriptor[] getGraphDescriptors() {
        return PRODUCED_GRAPH_DESCRIPTORS;
    }

    @Override
    public InputGraph[] process(final ProjectModel projectModel, final File workDirectory,
            final File outputDirectory, final LogWriter logWriter) {
        try {
            final DoxygenRunner doxygenRunner = new DoxygenRunner(logWriter);
            // set runner settings
            doxygenRunner.setWorkingDirectory(workDirectory);
            // set info settings
            doxygenRunner.setProjectName(projectModel.getProjectName());
            // set input settings
            doxygenRunner.setInputRecursive(projectModel.getInputRecursive());
            for (final DirectoryInfo inputDir : projectModel.getInputDirectories()) {
                doxygenRunner.addInputEntry(Path.resolve(workDirectory, inputDir.getPath()));
            }
            for (final FileInfo inputFile : projectModel.getInputFiles()) {
                doxygenRunner.addInputEntry(Path.resolve(workDirectory, inputFile.getPath()));
            }
            // set output settings
            doxygenRunner.setOutputDirectory(outputDirectory);
            // set preprocessor settings
            for (final DirectoryInfo includeDir : projectModel.getIncludeDirectories()) {
                doxygenRunner.addIncludeEntry(Path.resolve(workDirectory, includeDir.getPath()));
            }
            // set analyzer settings
            doxygenRunner.setBuiltinStlSupport(projectModel.getBuiltinStlSupport());

            /* run doxygen to generate output */
            if (!doxygenRunner.run()) {
                return null;
            }

            /* parse doxygen output */
            final DoxygenParser doxygenParser = new DoxygenParser();

            final DoxygenType doxygenData = doxygenParser
                    .parse(doxygenRunner.getProducedFilePath());
            if (doxygenData == null) {
                return null;
            }

            /* build file system graph */
            final DependencyGraph fsGraph = new DoxygenFsGraphBuilder().buildGraph(doxygenData);

            InputGraph[] graphs = new InputGraph[] { new InputGraph(
                    Constants.GRAPH_LOCATOR_INCLUDE_DEPS, fsGraph) };

            return graphs;
        } catch (final Throwable t) {
            t.printStackTrace();

            return null;
        }
    }

}
