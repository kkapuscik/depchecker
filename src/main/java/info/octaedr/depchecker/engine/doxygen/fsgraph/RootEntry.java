package info.octaedr.depchecker.engine.doxygen.fsgraph;

import info.octaedr.doxygen.xml.CompounddefType;

/**
 * File system root entry descriptor.
 * 
 * <p>
 * The file system root entries group all top level elements from doxygen data
 * and are used as the dependency graph data source.
 */
class RootEntry extends DirEntry {

    /**
     * Unique identifier of the FS graph.
     */
    private static final String GRAPH_UNIQUE_ID = "fs-graph";

    /**
     * Name of the graph entry.
     */
    private static final String GRAPH_NAME = "ROOT";

    /**
     * Constructs the entry.
     */
    public RootEntry() {
        super(GRAPH_UNIQUE_ID);
    }

    @Override
    public CompounddefType getCompound() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        return GRAPH_NAME;
    }
}
