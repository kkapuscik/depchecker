package info.octaedr.depchecker.engine.doxygen.fsgraph;

import java.util.ArrayList;

import info.octaedr.depchecker.engine.graph.Vertex;
import info.octaedr.doxygen.xml.CompounddefType;

/**
 * File entry descriptor.
 * 
 * <p>
 * The file entries represent files from doxygen data and are used as the
 * dependency graph vertices.
 */
class FileEntry {
    private final String uniqueId;
    private final CompounddefType compound;
    private int refCount = 0;
    private final ArrayList<FileEntry> includedFiles;
    private Vertex vertex;
    private final ArrayList<ExternalFileEntry> externalFiles;

    public FileEntry(final String uniqueId, final CompounddefType compound) {
        assert uniqueId != null;
        assert compound != null;

        this.uniqueId = uniqueId;
        this.compound = compound;
        this.includedFiles = new ArrayList<>();
        this.externalFiles = new ArrayList<>();
    }

    public String getUniqueId() {
        return this.uniqueId;
    }

    public String getName() {
        return this.compound.getCompoundname();
    }

    public CompounddefType getCompound() {
        return this.compound;
    }

    public void incRefCount() {
        ++this.refCount;
    }

    public int getRefCount() {
        return this.refCount;
    }

    public void addLocalInclude(final FileEntry includedEntry) {
        assert includedEntry != null;

        this.includedFiles.add(includedEntry);
    }

    public void addExternalInclude(final ExternalFileEntry extEntry) {
        assert extEntry != null;

        this.externalFiles.add(extEntry);
    }

    public Iterable<FileEntry> getLocalIncludes() {
        return this.includedFiles;
    }

    public Iterable<ExternalFileEntry> getExternalIncludes() {
        return this.externalFiles;
    }


    public Vertex getVertex() {
        return this.vertex;
    }

    public void setVertex(final Vertex vertex) {
        this.vertex = vertex;
    }

}
