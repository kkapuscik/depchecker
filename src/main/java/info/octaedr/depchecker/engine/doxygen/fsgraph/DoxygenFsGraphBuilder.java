package info.octaedr.depchecker.engine.doxygen.fsgraph;

import info.octaedr.depchecker.engine.IdentifierDictionary;
import info.octaedr.depchecker.engine.IdentifierDictionaryException;
import info.octaedr.depchecker.engine.doxygen.DoxygenGraphBuilder;
import info.octaedr.depchecker.engine.graph.DependencyGraph;
import info.octaedr.depchecker.engine.graph.DependencyGraphException;
import info.octaedr.depchecker.engine.graph.Vertex;
import info.octaedr.doxygen.xml.CompounddefType;
import info.octaedr.doxygen.xml.DoxygenType;
import info.octaedr.doxygen.xml.IncType;
import info.octaedr.doxygen.xml.RefType;

import java.util.HashMap;
import java.util.Map;

/**
 * Builder for the Fiel System graph.
 */
public class DoxygenFsGraphBuilder implements DoxygenGraphBuilder {

    private static final String EXTERNAL_INCLUDE_ID_PREFIX = "extinc";
    private static final String EXTERNAL_GROUP_ID = "extinc-group";

    /**
     * Constructs the builder.
     */
    public DoxygenFsGraphBuilder() {
        // nothing to do
    }

    @Override
    public DependencyGraph buildGraph(final DoxygenType doxygenData) {
        assert doxygenData != null;

        DependencyGraph graph = null;

        try {

            final IdentifierDictionary idDictionary = new IdentifierDictionary();
            final Map<String, FileEntry> fileEntries = new HashMap<>();
            final Map<String, DirEntry> dirEntries = new HashMap<>();
            final Map<String, ExternalFileEntry> externalEntries = new HashMap<>();

            collectEntries(doxygenData, idDictionary, fileEntries, dirEntries);
            processInnerElements(idDictionary, fileEntries, dirEntries);
            processIncludes(idDictionary, fileEntries, externalEntries);

            final RootEntry rootEntry = buildRootElement(fileEntries, dirEntries);

            graph = constructGraph(idDictionary, rootEntry, externalEntries);
        } catch (final IdentifierDictionaryException e) {
            // TODO: message/propagate
            e.printStackTrace();
        } catch (DependencyGraphException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return graph;
    }

    /**
     * Constructs graph for given entry.
     * 
     * @param idDictionary
     * 
     * @param graphEntry
     *            Entry for which graph should be built.
     * @param externalEntries
     *            External entries to be added to graph.
     * 
     * @return Constructed graph.
     * 
     * @throws DependencyGraphException
     *             when correct graph could not be constructed.
     * @throws IdentifierDictionaryException
     *             when there was a problem with any identifier.
     */
    private DependencyGraph constructGraph(final IdentifierDictionary idDictionary,
            final DirEntry graphEntry, final Map<String, ExternalFileEntry> externalEntries)
            throws DependencyGraphException, IdentifierDictionaryException {
        /* create the graph */
        final DependencyGraph graph = new DependencyGraph();
        graph.setName(graphEntry.getName());

        addGraphVertices(graph, null, graphEntry);
        addExternalEntries(idDictionary, graph, externalEntries);
        addGraphEdges(graph, graphEntry);

        return graph;
    }

    private void addExternalEntries(final IdentifierDictionary idDictionary,
            final DependencyGraph graph, final Map<String, ExternalFileEntry> externalEntries)
            throws IdentifierDictionaryException, DependencyGraphException {
        if (externalEntries.size() > 0) {
            final Vertex extGroupVertex = createExternalGroupVertex(idDictionary);
            
            graph.addVertex(extGroupVertex);
    
            for (final ExternalFileEntry entry : externalEntries.values()) {
                final Vertex extFileVertex = createVertex(entry);

                graph.addVertex(extFileVertex);

                graph.addStructureEdge(extGroupVertex, extFileVertex);
            }
        }    
    }

    private void addGraphEdges(final DependencyGraph graph, final DirEntry dirEntry) {
        /* process all files */
        for (final FileEntry fileEntry : dirEntry.getInnerFiles()) {
            final Vertex fileVertex = fileEntry.getVertex();

            /* process local includes */
            for (final FileEntry includedEntry : fileEntry.getLocalIncludes()) {
                final Vertex includedVertex = includedEntry.getVertex();

                if ((fileVertex != null) && (includedVertex != null)) {
                    graph.addRelationEdge(fileVertex, includedVertex);
                } else {
                    // TODO: warning
                }
            }
            
            /* add external includes */
            for (final ExternalFileEntry extEntry : fileEntry.getExternalIncludes()) {
                final Vertex externalVertex = extEntry.getVertex();
                
                if ((fileVertex != null) && (externalVertex != null)) {
                    graph.addRelationEdge(fileVertex, externalVertex);
                } else {
                    // TODO: warning
                }
            }
        }
        
        /* process all directories */
        for (final DirEntry subEntry : dirEntry.getInnerDirs()) {
            addGraphEdges(graph, subEntry);
        }
    }

    private void addGraphVertices(final DependencyGraph graph, final Vertex parentVertex,
            final DirEntry dirEntry) throws DependencyGraphException {
        /* add all file (simple) vertices */
        for (final FileEntry fileEntry : dirEntry.getInnerFiles()) {
            final Vertex fileVertex = createVertex(fileEntry);

            graph.addVertex(fileVertex);

            if (parentVertex != null) {
                graph.addStructureEdge(parentVertex, fileVertex);
            }
        }

        /* build all subgraphs */
        for (final DirEntry subEntry : dirEntry.getInnerDirs()) {
            final Vertex dirVertex = createVertex(subEntry);

            graph.addVertex(dirVertex);

            if (parentVertex != null) {
                graph.addStructureEdge(parentVertex, dirVertex);
            }

            addGraphVertices(graph, dirVertex, subEntry);
        }
    }

    private Vertex createExternalGroupVertex(final IdentifierDictionary idDictionary)
            throws IdentifierDictionaryException {
        /* prepare external entry */
        final String uniqueId = idDictionary.addMapping(EXTERNAL_GROUP_ID);

        final Vertex vertex = new Vertex(uniqueId);
        vertex.setName("External");
        vertex.setCluster(true);

        return vertex;
    }

    private Vertex createVertex(final ExternalFileEntry entry) {
        final Vertex vertex = new Vertex(entry.getUniqueId());
        vertex.setName(entry.getName());

        entry.setVertex(vertex);

        return vertex;
    }

    private Vertex createVertex(final FileEntry fileEntry) {
        final Vertex vertex = new Vertex(fileEntry.getUniqueId());
        vertex.setName(fileEntry.getName());

        fileEntry.setVertex(vertex);

        return vertex;
    }

    private Vertex createVertex(final DirEntry dirEntry) {
        final Vertex vertex = new Vertex(dirEntry.getUniqueId());
        vertex.setName(dirEntry.getName());
        vertex.setCluster(true);

        return vertex;
    }

    /**
     * Collects directory and file entries from doxygen data.
     * 
     * @param doxygenData
     *            Doxygen data to process.
     * @param idDictionary
     *            Dictionary of the identifiers.
     * @param fileEntries
     *            Collection to be filed with file entries found.
     * @param dirEntries
     *            Collection to be filled with dir entries found.
     * 
     * @throws IdentifierDictionaryException
     *             when identifier conflict is detected.
     */
    private void collectEntries(final DoxygenType doxygenData,
            final IdentifierDictionary idDictionary, final Map<String, FileEntry> fileEntries,
            final Map<String, DirEntry> dirEntries) throws IdentifierDictionaryException {
        for (final CompounddefType compound : doxygenData.getCompounddef()) {
            final String id = compound.getId();
            // silently skip invalid nodes
            if (id == null) {
                // TODO: msg
                continue;
            }

            switch (compound.getKind()) {
                case FILE: {
                    final String uniqueId = idDictionary.addMapping(id);

                    fileEntries.put(uniqueId, new FileEntry(uniqueId, compound));
                    break;
                }
                case DIR: {
                    final String uniqueId = idDictionary.addMapping(id);

                    dirEntries.put(uniqueId, new DirEntry(uniqueId, compound));
                    break;
                }
                default:
                    break;
            }
        }
    }

    /**
     * Processes inner elements detecting one level dependencies.
     * 
     * @param idDictionary
     *            Dictionary of the identifiers.
     * @param fileEntries
     *            Collection of file entries to be processed.
     * @param dirEntries
     *            Collection of dir entries to be processed.
     */
    private void processInnerElements(final IdentifierDictionary idDictionary,
            final Map<String, FileEntry> fileEntries, final Map<String, DirEntry> dirEntries) {
        /* process all directories */
        for (final DirEntry dir : dirEntries.values()) {
            /* for each directory process subdirectories */
            for (final RefType innerDir : dir.getCompound().getInnerdir()) {
                final String refId = innerDir.getRefid();

                final String translatedId = idDictionary.getMapping(refId);
                if (translatedId != null) {
                    /* try to find entry for subdirectory */
                    final DirEntry refDir = dirEntries.get(translatedId);
                    if (refDir != null) {
                        /* add subdirectory object */
                        dir.addInnerDir(refDir);
                        /* increment number of references to subdirectory */
                        refDir.incRefCount();
                    }
                }
            }

            /* for each directory process files */
            for (final RefType innerFile : dir.getCompound().getInnerfile()) {
                final String refId = innerFile.getRefid();

                final String translatedId = idDictionary.getMapping(refId);
                if (translatedId != null) {
                    /* try to find entry for file */
                    final FileEntry refFile = fileEntries.get(translatedId);
                    if (refFile != null) {
                        /* add subfile object */
                        dir.addInnerFile(refFile);
                        /* incremenet number of references to subfile */
                        refFile.incRefCount();
                    }
                }
            }
        }
    }

    private void processIncludes(final IdentifierDictionary idDictionary,
            final Map<String, FileEntry> fileEntries,
            final Map<String, ExternalFileEntry> externalEntries)
            throws IdentifierDictionaryException {

        for (final FileEntry fileEntry : fileEntries.values()) {
            for (final IncType include : fileEntry.getCompound().getIncludes()) {
                final String doxygenIncludeId = include.getRefid();
                if (doxygenIncludeId != null) {
                    // there is a refid - it is a local include
                    final String includeId = idDictionary.getMapping(doxygenIncludeId);
                    if (includeId != null) {
                        final FileEntry includedEntry = fileEntries.get(includeId);
                        if (includedEntry != null) {
                            fileEntry.addLocalInclude(includedEntry);
                        }
                    } else {
                        // TODO: warning!
                        System.err.println("Missing include: " + doxygenIncludeId);
                    }
                } else {
                    /* prepare id for the external include */
                    final String externalIncludeId = EXTERNAL_INCLUDE_ID_PREFIX
                            + include.getValue();

                    /* prepare id mapping */
                    String includeId = idDictionary.getMapping(externalIncludeId);
                    if (includeId == null) {
                        includeId = idDictionary.addMapping(externalIncludeId);
                    }

                    /* prepare external element if needed */
                    ExternalFileEntry extEntry = externalEntries.get(includeId);
                    if (extEntry == null) {
                        extEntry = new ExternalFileEntry(includeId, include);
                        externalEntries.put(includeId, extEntry);
                    }

                    // add relation
                    fileEntry.addExternalInclude(extEntry);
                }
            }
        }
    }

    /**
     * Builds file system root element.
     * 
     * @param fileEntries
     *            Collection of file entries to be processed.
     * @param dirEntries
     *            Collection of directory entries to be processed.
     * 
     * @return Created root element.
     */
    private RootEntry buildRootElement(final Map<String, FileEntry> fileEntries,
            final Map<String, DirEntry> dirEntries) {
        /* create root entry */
        final RootEntry rootEntry = new RootEntry();

        /* add all not references directories to the root */
        for (final DirEntry dirEntry : dirEntries.values()) {
            if (dirEntry.getRefCount() == 0) {
                rootEntry.addInnerDir(dirEntry);
                dirEntry.incRefCount();
            }
        }

        /* add all not references files to the root */
        for (final FileEntry fileEntry : fileEntries.values()) {
            if (fileEntry.getRefCount() == 0) {
                rootEntry.addInnerFile(fileEntry);
                fileEntry.incRefCount();
            }
        }

        return rootEntry;
    }

}
