package info.octaedr.depchecker.engine;

/**
 * Exception thrown by IdentifierDictionary.
 */
public class IdentifierDictionaryException extends Exception {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = 2573223827795175694L;

    /**
     * Constructs the exception.
     * 
     * @param message
     *            The detail message.
     */
    public IdentifierDictionaryException(final String message) {
        super(message);
    }
}
