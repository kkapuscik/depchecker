package info.octaedr.depchecker.engine;

/**
 * Name of the analyzer.
 */
public interface DataAnalyzer {

    /**
     * Returns name of the analyzer.
     * 
     * @return Name of the analyzer.
     */
    String getName();

    // TODO
}
