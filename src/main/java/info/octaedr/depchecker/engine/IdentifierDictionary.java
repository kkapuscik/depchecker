package info.octaedr.depchecker.engine;

import java.util.HashMap;
import java.util.Map;

/**
 * Dictionary of identifiers.
 * 
 * <p>
 * It is used to map any-string identifiers to a identifiers using limited set
 * of characters (alphanumeric and underscore).
 */
public class IdentifierDictionary {

    /**
     * Prefix of the generated identifiers.
     */
    private static final String IDENTIFIER_PREFIX = "id";

    /**
     * Collection of mapped identifiers.
     */
    final private Map<String, String> identifierDictionary = new HashMap<>();

    /**
     * Next identifier index.
     */
    private int nextId = 1;

    /**
     * Constructs the dictionary.
     */
    public IdentifierDictionary() {
        // nothing to do
    }

    /**
     * Adds mapping to the dictionary for the given doxygen id.
     * 
     * @param doxygenId
     *            The id to be mapped.
     * 
     * @return The mapped id.
     * 
     * @throws IdentifierDictionaryException
     *             when given identifier is already mapped.
     */
    public String addMapping(final String doxygenId) throws IdentifierDictionaryException {
        assert doxygenId != null;
        
        if (getMapping(doxygenId) != null) {
            throw new IdentifierDictionaryException("Identifier already mapped: " + doxygenId);
        }

        final String mappedId = IDENTIFIER_PREFIX + (this.nextId++);

        this.identifierDictionary.put(doxygenId, mappedId);

        return mappedId;
    }

    /**
     * Returns mapping to the dictionary for the given doxygen id.
     * 
     * @param doxygenId
     *            The id to be mapped.
     * 
     * @return The mapped id if the given identifier was mapped or null
     *         otherwise.
     */
    public String getMapping(final String doxygenId) {
        assert doxygenId != null;

        return this.identifierDictionary.get(doxygenId);
    }

}
