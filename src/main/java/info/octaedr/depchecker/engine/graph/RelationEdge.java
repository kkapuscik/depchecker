package info.octaedr.depchecker.engine.graph;

import org.jgrapht.graph.DefaultEdge;

/**
 * Graph edge for describing relation between elements.
 * 
 * <p>
 * Relation edges are used in relations subgraph that describes the
 * dependency relation between vertices.
 */
public class RelationEdge extends DefaultEdge {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -3726206191469680975L;

}
