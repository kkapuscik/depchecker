package info.octaedr.depchecker.engine.graph;

import java.util.HashSet;
import java.util.Set;

import org.jgrapht.experimental.dag.DirectedAcyclicGraph;

/**
 * Forest graph implementation.
 * 
 * @param <V>
 *            Vertex class.
 * @param <E>
 *            Edge class.
 */
public class Forest<V, E> extends DirectedAcyclicGraph<V, E> {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -3894763575716301266L;

    /**
     * Constructs the graph.
     * 
     * @param edgeClass
     *            Class on which to base factory for edges.
     */
    public Forest(Class<? extends E> edgeClass) {
        super(edgeClass);
    }

    /**
     * Returns set of root vertices.
     * 
     * @return Collection of vertices being roots of the forest trees.
     */
    public Set<V> getRootVertices() {
        final Set<V> roots = new HashSet<V>();

        for (final V vertex : vertexSet()) {
            final Set<E> incommingEdges = incomingEdgesOf(vertex);
            if (incommingEdges.size() == 0) {
                roots.add(vertex);
            }
        }

        return roots;
    }

    @Override
    public boolean addVertex(final V vertex) {
        if (!checkVertex(vertex)) {
            return false;
        }

        return super.addVertex(vertex);
    }

    @Override
    public boolean addVertex(final V vertex, boolean addToTop) {
        if (!checkVertex(vertex)) {
            return false;
        }

        return super.addVertex(vertex, addToTop);
    }

    @Override
    public E addEdge(final V sourceVertex, final V targetVertex) {
        if (!checkEdge(sourceVertex, targetVertex)) {
            return null;
        }

        return super.addEdge(sourceVertex, targetVertex);
    }

    @Override
    public boolean addEdge(final V sourceVertex, final V targetVertex, E edge) {
        if (!checkEdge(sourceVertex, targetVertex)) {
            return false;
        }

        return super.addEdge(sourceVertex, targetVertex, edge);
    }

    @Override
    public E addDagEdge(final V fromVertex, final V toVertex) throws CycleFoundException {
        if (!checkEdge(fromVertex, toVertex)) {
            return null;
        }

        return super.addDagEdge(fromVertex, toVertex);
    }

    @Override
    public boolean addDagEdge(V fromVertex, V toVertex, E e) throws CycleFoundException {
        if (!checkEdge(fromVertex, toVertex)) {
            return false;
        }

        return super.addDagEdge(fromVertex, toVertex, e);
    }

    /**
     * Checks if vertex is valid and could be added to graph.
     * 
     * @param vertex
     *            Vertex to be verified.
     * 
     * @return True if vertex could be added to graph, false otherwise.
     */
    private boolean checkVertex(final V vertex) {
        assert vertex != null;

        if (containsVertex(vertex)) {
            return false;
        }

        return true;
    }

    /**
     * Checks if edge is valid and could be added to graph.
     * 
     * @param sourceVertex
     *            Edge source vertex.
     * @param targetVertex
     *            Edge target vertex.
     * 
     * @return True if edged could be added to graph, false otherwise.
     */
    private boolean checkEdge(final V sourceVertex, final V targetVertex) {
        assert sourceVertex != null;
        assert targetVertex != null;

        if (containsEdge(sourceVertex, targetVertex)) {
            return false;
        }

        /*
         * NOTES:
         * 
         * - the cycles are checked by addDagEdge method
         * 
         * - we only have to check if graph is still a forest
         * 
         * To check if graph is a forest: the target node could only have one
         * incoming edge - the one that we are going to add. Otherwise there is
         * already a parent set and we could not add another one. Number of
         * outgoing edges does not matter (any number of children is supported).
         */
        final Set<E> incommingEdges = incomingEdgesOf(targetVertex);
        if (incommingEdges.size() > 0) {
            return false;
        }

        return true;
    }

}
