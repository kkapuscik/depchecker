package info.octaedr.depchecker.engine.graph;

import org.jgrapht.graph.DefaultEdge;

/**
 * Graph edge for describing structural organization.
 * 
 * <p>
 * Structural edges are used in structural subgraph that describes the
 * parent-child relation between vertices.
 */
public class StructureEdge extends DefaultEdge {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -4320406552051346853L;
    
}
