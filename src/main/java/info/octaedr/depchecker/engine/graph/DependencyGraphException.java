package info.octaedr.depchecker.engine.graph;

/**
 * Exception thrown on dependency graph operation errors.
 */
public class DependencyGraphException extends Exception {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -4905220409908095550L;

    /**
     * Constructs the exception.
     * 
     * @param message
     *            Exception detail message.
     */
    public DependencyGraphException(final String message) {
        super(message);
    }
}
