package info.octaedr.depchecker.engine.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jgrapht.Graph;
import org.jgrapht.graph.DirectedPseudograph;

/**
 * Construct empty graph.
 */
public class DependencyGraph {

    /**
     * Directed graph for representing relations.
     */
    private static class RelationGraph extends DirectedPseudograph<Vertex, RelationEdge> {

        /**
         * Generated serial version UID.
         */
        private static final long serialVersionUID = -5109137501484803342L;

        /**
         * Constructs the graph.
         */
        public RelationGraph() {
            super(RelationEdge.class);
        }

    }

    /**
     * ' Forest graph for representing structure.
     */
    private static class StructureGraph extends Forest<Vertex, StructureEdge> {

        /**
         * Generated serial version UID.
         */
        private static final long serialVersionUID = -2970707537179374950L;

        /**
         * Constructs the graph.
         */
        public StructureGraph() {
            super(StructureEdge.class);
        }

    }

    /**
     * Graph that holds the relations.
     */
    private final RelationGraph relationGraph = new RelationGraph();

    /**
     * Graph that holds the structure.
     */
    private final StructureGraph structureGraph = new StructureGraph();

    /**
     * Mapping from vertex identifiers to vertices.
     */
    private final Map<String, Vertex> vertices = new HashMap<>();

    /**
     * Name of the graph.
     */
    private String name;

    /**
     * Constructs empty graph.
     */
    public DependencyGraph() {
        // nothing to do
    }

    /**
     * Returns name of the graph.
     * 
     * @return Name of the graph (may be null).
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets name of the graph.
     * 
     * @param name
     *            New name of the graph (may be null to unset the name).
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Adds vertex to the graph.
     * 
     * @param vertex
     *            Vertex to be added.
     * 
     * @throws DependencyGraphException
     *             when vertex object is already in graph, vertex with same
     *             unique id is already in graph or in any other case when
     *             vertex cannot be added to graph.
     */
    public void addVertex(final Vertex vertex) throws DependencyGraphException {
        assert vertex != null;

        if (this.vertices.containsKey(vertex.getUniqueId())) {
            throw new DependencyGraphException("Vertex already present in graph: "
                    + vertex.getUniqueId());
        }

        try {
            if (!this.relationGraph.addVertex(vertex)) {
                throw new DependencyGraphException("Cannot add vertex to relation graph");
            }

            if (!this.structureGraph.addVertex(vertex)) {
                throw new DependencyGraphException("Cannot add vertex to structure graph");
            }

            this.vertices.put(vertex.getUniqueId(), vertex);
        } catch (final DependencyGraphException exception) {
            /* cleanup */
            this.relationGraph.removeVertex(vertex);
            this.structureGraph.removeVertex(vertex);

            /* re-throw */
            throw exception;
        }
    }

    /**
     * Returns structure root vertices.
     * 
     * @return Collection of vertices being roots of structure tree.
     */
    public Set<Vertex> getRootVertices() {
        return this.structureGraph.getRootVertices();
    }

    /**
     * Checks if the vertex represents a group in structure.
     * 
     * @param vertex
     *            The vertex to be checked.
     * 
     * @return True if vertex is root vertex, false if it is simple (leaf)
     *         vertex.
     * 
     * @throws DependencyGraphException
     *             if given vertex is not in graph.
     */
    public boolean isGroupVertex(final Vertex vertex) throws DependencyGraphException {
        if (!this.structureGraph.containsVertex(vertex)) {
            throw new DependencyGraphException("Vertex is not in graph");
        }

        return !this.structureGraph.outgoingEdgesOf(vertex).isEmpty();
    }

    /**
     * Returns subvertices of given vertex in structure graph.
     * 
     * @param vertex
     *            Vertex for which subvertices to get.
     * 
     * @return Collection of subvertices.
     * 
     * @throws DependencyGraphException
     *             if given vertex is not in graph.
     */
    public Set<Vertex> getSubvertices(final Vertex vertex) throws DependencyGraphException {
        if (!this.structureGraph.containsVertex(vertex)) {
            throw new DependencyGraphException("Vertex is not in graph");
        }

        final Set<Vertex> subvertices = new HashSet<>();
        for (final StructureEdge edge : this.structureGraph.outgoingEdgesOf(vertex)) {
            subvertices.add(this.structureGraph.getEdgeTarget(edge));
        }

        return subvertices;
    }

    /**
     * Adds edge to structure graph.
     * 
     * @param parentVertex
     *            Parent (source) vertex.
     * @param childVertex
     *            Child (target) vertex.
     */
    public void addStructureEdge(final Vertex parentVertex, final Vertex childVertex) {
        this.structureGraph.addEdge(parentVertex, childVertex);
    }

    /**
     * Adds edge to relations graph.
     * 
     * @param sourceVertex
     *            Dependency source (owner) object.
     * @param targetVertex
     *            Dependency target (dependent) object.
     */
    public void addRelationEdge(final Vertex sourceVertex, final Vertex targetVertex) {
        this.relationGraph.addEdge(sourceVertex, targetVertex);
    }

    /**
     * Returns all graph vertices.
     * 
     * @return Collection of graph vertices.
     */
    public Set<Vertex> getAllVertices() {
        return this.relationGraph.vertexSet();
    }

    /**
     * Returns all outgoing relation edges.
     * 
     * @param vertex
     *            Vertex for which edges are to be returned.
     * 
     * @return Collection of edges.
     */
    public Set<RelationEdge> getAllOutRelations(final Vertex vertex) {
        return this.relationGraph.outgoingEdgesOf(vertex);
    }

    /**
     * Returns all incoming relation edges.
     * 
     * @param vertex
     *            Vertex for which edges are to be returned.
     * 
     * @return Collection of edges.
     */
    public Set<RelationEdge> getAllInRelations(final Vertex vertex) {
        return this.relationGraph.incomingEdgesOf(vertex);
    }

    /**
     * Returns vertex being the source of given relation.
     * 
     * @param edge
     *            Edge describing the relation.
     * 
     * @return Source vertex object.
     */
    public Vertex getRelationSource(final RelationEdge edge) {
        return this.relationGraph.getEdgeSource(edge);
    }

    /**
     * Returns vertex being the target of given relation.
     * 
     * @param edge
     *            Edge describing the relation.
     * 
     * @return Target vertex object.
     */
    public Vertex getRelationTarget(final RelationEdge edge) {
        return this.relationGraph.getEdgeTarget(edge);
    }

    /**
     * Returns relation graph.
     * 
     * TODO: remove this method
     * 
     * @return Relation graph object.
     */
    public Graph<Vertex, RelationEdge> getRelationGraph() {
        return this.relationGraph;
    }

    /**
     * Returns structure graph.
     * 
     * TODO: remove this method
     * 
     * @return Structure graph object.
     */
    public Graph<Vertex, StructureEdge> getStructureGraph() {
        return this.structureGraph;
    }

    /**
     * Checks if vertex is in graph.
     * 
     * @param vertex
     *            Vertex to be checked.
     * 
     * @return True if vertex is in graph, false if not.
     */
    public boolean hasVertex(final Vertex vertex) {
        return this.structureGraph.containsVertex(vertex);
    }

    /**
     * Removes vertex and all edges attached to it from graph.
     * 
     * @param vertex
     *            Vertex to be removed.
     * 
     * @return True if vertex was in graph and was removed sucessfuly, false
     *         otherwise.
     */
    public boolean removeVertex(final Vertex vertex) {
        if (this.vertices.remove(vertex.getUniqueId()) != null) {
            return this.structureGraph.removeVertex(vertex)
                    && this.relationGraph.removeVertex(vertex);
        } else {
            return false;
        }
    }

}
