package info.octaedr.depchecker.engine.graph;

import java.io.Serializable;

/**
 * Dependency graph vertex.
 */
public class Vertex implements Serializable {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -8219580032568559852L;

    /**
     * Graph-unique vertex identifier.
     */
    private final String uniqueId;

    /**
     * Vertex name.
     */
    private String name;

    /**
     * Vertex is a cluster flag.
     */
    private boolean clusterFlag = false;

    /**
     * Constructs vertex.
     * 
     * @param uniqueId
     *            Graph-unique vertex identifier.
     */
    public Vertex(final String uniqueId) {
        assert uniqueId != null;

        this.uniqueId = uniqueId;
    }

    /**
     * Unique vertex identifier.
     * 
     * @return String that is unique within the dependency graph.
     */
    public final String getUniqueId() {
        return this.uniqueId;
    }

    /**
     * Name of the vertex.
     * 
     * @return Vertex's label.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets name of the vertex.
     * 
     * @param name
     *            Name to set (null to unset the name).
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Checks vertex is a cluster.
     * 
     * <p>
     * A cluster vertex is a vertex that groups other vertices in structural
     * subgraph.
     * 
     * @return True if vertex represents a cluster, false otherwise.
     */
    public boolean isCluster() {
        return this.clusterFlag;
    }

    /**
     * Sets vertex is a cluster flag.
     * 
     * @param cluster
     *            True to mark vertex as a cluser, false to unmark it.
     */
    public void setCluster(final boolean cluster) {
        this.clusterFlag = cluster;
    }

    @Override
    public String toString() {
        if (this.name != null) {
            return this.name;
        } else if (this.uniqueId != null) {
            return this.uniqueId;
        } else {
            return "Vertex";
        }
    }
}
