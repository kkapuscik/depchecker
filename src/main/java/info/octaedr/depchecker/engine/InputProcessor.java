package info.octaedr.depchecker.engine;

import info.octaedr.depchecker.model.InputGraph;
import info.octaedr.depchecker.model.InputGraphDescriptor;
import info.octaedr.depchecker.model.LogWriter;
import info.octaedr.depchecker.model.ProjectModel;

import java.io.File;

/**
 * Processor of the input data.
 * 
 * <p>
 * This is an interface implemented by all input data processors.
 */
public interface InputProcessor {

    /**
     * Returns unique identifier of the input processor.
     * 
     * @return Unique identifier of the processor.
     */
    String getUniqueId();

    /**
     * Returns name of the input processor.
     * 
     * @return Name of the input processor.
     */
    String getName();

    /**
     * Returns descriptors of produced graphs.
     * 
     * @return Array with produced graphs descriptors.
     */
    InputGraphDescriptor[] getGraphDescriptors();

    /**
     * Processes the input data.
     * 
     * @param projectModel
     *            Model with the data to be processed.
     * @param baseDirectory
     *            Work directory (base directory of the project).
     * @param outputDirectory
     *            Directory to store intermediate data and processing results.
     * @param logWriter
     *            Object for printing log messages.
     * 
     * @return Collection of graphs being the output of the processing or null
     *         if an error occurred.
     */
    InputGraph[] process(final ProjectModel projectModel, final File baseDirectory,
            final File outputDirectory, final LogWriter logWriter);

}
