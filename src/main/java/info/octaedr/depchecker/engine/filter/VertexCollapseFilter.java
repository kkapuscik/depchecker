package info.octaedr.depchecker.engine.filter;

import info.octaedr.depchecker.engine.graph.DependencyGraph;
import info.octaedr.depchecker.engine.graph.RelationEdge;
import info.octaedr.depchecker.engine.graph.StructureEdge;
import info.octaedr.depchecker.engine.graph.Vertex;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.jgrapht.traverse.BreadthFirstIterator;

/**
 * Graph filter for collapsing vertices.
 * 
 * <p>
 * The filter joins a sub-tree of vertices to a single vertex.
 */
public abstract class VertexCollapseFilter implements DependencyGraphFilter {

    @Override
    public void apply(final DependencyGraph graph) {
        /* collection of vertices to collapse */
        final ArrayList<Vertex> verticesToCollapse = new ArrayList<>();

        /* iterate over graph structure and check which vertices to collapse */
        final BreadthFirstIterator<Vertex, StructureEdge> iterator = new BreadthFirstIterator<>(
                graph.getStructureGraph());
        while (iterator.hasNext()) {
            final Vertex vertex = iterator.next();

            if (accept(graph, vertex)) {
                verticesToCollapse.add(vertex);
            }
        }

        /* collapse the vertices */
        for (final Vertex vertex : verticesToCollapse) {
            if (graph.hasVertex(vertex)) {
                collapseVertexTree(graph, vertex);
            }
        }
    }

    /**
     * Collapses structure subtree.
     * 
     * @param graph
     *            Graph being processed.
     * @param collapsedVertex
     *            Root vertex of the tree to be collapsed.
     */
    private void collapseVertexTree(final DependencyGraph graph, final Vertex collapsedVertex) {
        final Set<Vertex> vertices = new HashSet<Vertex>();

        /* iterate over graph structure and check which vertices to collapse */
        final BreadthFirstIterator<Vertex, StructureEdge> iterator = new BreadthFirstIterator<>(
                graph.getStructureGraph(), collapsedVertex);
        while (iterator.hasNext()) {
            vertices.add(iterator.next());
        }

        /* collapse vertices from graph */
        final ArrayList<Vertex> inVertices = new ArrayList<>();
        final ArrayList<Vertex> outVertices = new ArrayList<>();
        
        /* find edges to move */
        for (final Vertex vertexToCollapse : vertices) {
            if (vertexToCollapse != collapsedVertex) {
                /* process incoming edges */
                for (final RelationEdge edge : graph.getAllInRelations(vertexToCollapse)) {
                    inVertices.add(graph.getRelationSource(edge));
                }

                /* process outgoing edges */
                for (final RelationEdge edge : graph.getAllOutRelations(vertexToCollapse)) {
                    outVertices.add(graph.getRelationTarget(edge));
                }
            }
        }
        
        /* add moved edges */
        for (final Vertex inVertex : inVertices) {
            graph.addRelationEdge(inVertex, collapsedVertex);
        }
        for (final Vertex outVertex : outVertices) {
            graph.addRelationEdge(collapsedVertex, outVertex);
        }
        
        /* remove the subvertices */
        for (final Vertex vertexToCollapse : vertices) {
            if (vertexToCollapse != collapsedVertex) {
                graph.removeVertex(vertexToCollapse);
            }
        }
    }
    
    /**
     * Checks if vertex is accepted by filter.
     * 
     * @param graph
     *            Graph being processed.
     * @param vertex
     *            Vertex to be checked.
     * 
     * @return True if vertex was accepted, false otherwise.
     */
    protected abstract boolean accept(final DependencyGraph graph, final Vertex vertex);

}
