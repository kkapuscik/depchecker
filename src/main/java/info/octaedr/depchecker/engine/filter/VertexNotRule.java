package info.octaedr.depchecker.engine.filter;

import info.octaedr.depchecker.engine.graph.Vertex;

/**
 * Logical NOT vertex selection rule.
 * 
 * <p>
 * Returns negation of the child rule acceptance or false if child rule was not
 * set.
 */
public class VertexNotRule implements VertexRule {

    /**
     * Child (negated) rule.
     */
    private VertexRule childRule;

    /**
     * Returns child (negated) rule.
     * 
     * @return Child rule currently set. Null if not set.
     */
    public VertexRule getChildRule() {
        return this.childRule;
    }

    /**
     * Sets child (negated) rule.
     * 
     * @param rule
     *            Rule to set.
     */
    public void setChildRule(final VertexRule rule) {
        this.childRule = rule;
    }

    @Override
    public boolean accept(final Vertex vertex) {
        if (this.childRule != null) {
            return !this.childRule.accept(vertex);
        } else {
            return false;
        }
    }

}
