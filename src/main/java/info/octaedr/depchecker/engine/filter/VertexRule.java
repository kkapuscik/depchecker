package info.octaedr.depchecker.engine.filter;

import info.octaedr.depchecker.engine.graph.Vertex;

/**
 * Base interface for vertex selection rules.
 */
public interface VertexRule {

    /**
     * Checks if vertex is accepted by the rule.
     * 
     * @param vertex
     *            Vertex to be checked.
     * 
     * @return True if vertex is accepted by the rule, false otherwise.
     */
    boolean accept(Vertex vertex);

}
