package info.octaedr.depchecker.engine.filter;

import java.util.ArrayList;

/**
 * Base class for vertex selection logical rules.
 */
public abstract class VertexLogicRule implements VertexRule {

    /**
     * Collection of child rules.
     */
    private final ArrayList<VertexRule> childRules;

    /**
     * Constructs logic rule without any child rules.
     */
    protected VertexLogicRule() {
        this.childRules = new ArrayList<>();
    }

    /**
     * Appends new child rule.
     * 
     * @param rule
     *            Child rule to be added.
     */
    public void addChild(final VertexRule rule) {
        assert rule != null;

        this.childRules.add(rule);
    }

    /**
     * Returns number of child rules.
     * 
     * @return Number of child rules.
     */
    public int getChildCount() {
        return this.childRules.size();
    }

    /**
     * Returns specified child rule.
     * 
     * @param index
     *            Index of the child rule to get.
     * 
     * @return Child rule at given index.
     * 
     * @throws IndexOutOfBoundsException
     *             if index is invalid.
     */
    public VertexRule getChild(final int index) {
        return this.childRules.get(index);
    }

}
