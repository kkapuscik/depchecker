package info.octaedr.depchecker.engine.filter.selector;

/**
 * Logic AND operation selector.
 * 
 * @param <T>
 *            Type of the selected object.
 */
public class LogicAndSelector<T> extends LogicSelector<T> {

    /**
     * Constructs the selector.
     */
    public LogicAndSelector() {
        // nothing to do
    }

    @Override
    public boolean accept(final T object) {
        for (final Selector<T> child : getChildren()) {
            if (!child.accept(object)) {
                return false;
            }
        }

        return true;
    }

}
