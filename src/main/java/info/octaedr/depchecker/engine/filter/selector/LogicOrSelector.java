package info.octaedr.depchecker.engine.filter.selector;

/**
 * Logic OR operation selector.
 * 
 * @param <T>
 *            Type of the selected object.
 */
public class LogicOrSelector<T> extends LogicSelector<T> {

    /**
     * Constructs the selector.
     */
    public LogicOrSelector() {
        // nothing to do
    }

    @Override
    public boolean accept(final T object) {
        for (final Selector<T> child : getChildren()) {
            if (child.accept(object)) {
                return true;
            }
        }

        return false;
    }

}
