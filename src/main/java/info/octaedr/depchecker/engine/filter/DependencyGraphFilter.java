package info.octaedr.depchecker.engine.filter;

import info.octaedr.depchecker.engine.graph.DependencyGraph;

/**
 * Interface of graph content filters.
 */
public interface DependencyGraphFilter {

    /**
     * Applies the filter on graph.
     * 
     * @param graph
     *            Graph to be filtered.
     */
    void apply(final DependencyGraph graph);

}
