package info.octaedr.depchecker.engine.filter.selector;

/**
 * Logic NOT operation selector.
 * 
 * @param <T>
 *            Type of the selected object.
 */
public class LogicNotSelector<T> extends LogicSelector<T> {

    /**
     * Constructs the selector.
     */
    public LogicNotSelector() {
        // nothing to do
    }

    @Override
    public int getMaxChildCount() {
        // only one selector may be negated
        return 1;
    }
    
    @Override
    public boolean accept(final T object) {
        for (final Selector<T> child : getChildren()) {
            if (child.accept(object)) {
                return true;
            }
        }

        return false;
    }

}
