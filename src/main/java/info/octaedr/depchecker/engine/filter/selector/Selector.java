package info.octaedr.depchecker.engine.filter.selector;

/**
 * Base class for the element selectors.
 * 
 * @param <T>
 *            type of the selected object.
 */
public interface Selector<T> {

    /**
     * Checks if selector is valid.
     * 
     * @return True if selector is valid, false otherwise.
     */
    boolean validate();

    /**
     * Checks if object is acceptable by the selector.
     * 
     * @param object
     *            Object to be checked.
     * 
     * @return True if object was accepted, false otherwise.
     */
    boolean accept(T object);

}
