package info.octaedr.depchecker.engine.filter;

import info.octaedr.depchecker.engine.graph.DependencyGraph;
import info.octaedr.depchecker.engine.graph.StructureEdge;
import info.octaedr.depchecker.engine.graph.Vertex;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.jgrapht.traverse.BreadthFirstIterator;

/**
 * Remove filter for removing vertices.
 * 
 * <p>
 * The filter removes accepted vertices and all edges attached to that vertices
 * from the graph.
 */
public abstract class VertexRemoveFilter implements DependencyGraphFilter {

    @Override
    public void apply(final DependencyGraph graph) {
        /* collection of vertices to remove */
        final ArrayList<Vertex> verticesToRemove = new ArrayList<>();

        /* iterate over graph structure and check which vertices to remove */
        final BreadthFirstIterator<Vertex, StructureEdge> iterator = new BreadthFirstIterator<>(
                graph.getStructureGraph());
        while (iterator.hasNext()) {
            final Vertex vertex = iterator.next();

            if (accept(graph, vertex)) {
                verticesToRemove.add(vertex);
            }
        }

        /* remove the vertices from graph */
        for (final Vertex vertex : verticesToRemove) {
            if (graph.hasVertex(vertex)) {
                removeVertexTree(graph, vertex);
            }
        }
    }

    /**
     * Removes structure subtree from graph.
     * 
     * @param graph
     *            Graph being processed.
     * @param vertex
     *            Root vertex of the tree to be removed.
     */
    private void removeVertexTree(final DependencyGraph graph, final Vertex vertex) {
        final Set<Vertex> vertices = new HashSet<Vertex>();

        /* add itself */
        vertices.add(vertex);

        /* iterate over graph structure and check which vertices to remove */
        final BreadthFirstIterator<Vertex, StructureEdge> iterator = new BreadthFirstIterator<>(
                graph.getStructureGraph(), vertex);
        while (iterator.hasNext()) {
            vertices.add(iterator.next());
        }

        /* remove vertices from graph */
        for (final Vertex vertexToRemove : vertices) {
            graph.removeVertex(vertexToRemove);
        }
    }

    /**
     * Checks if vertex is accepted by filter.
     * 
     * @param graph
     *            Graph being processed.
     * @param vertex
     *            Vertex to be checked.
     * 
     * @return True if vertex was accepted, false otherwise.
     */
    protected abstract boolean accept(final DependencyGraph graph, final Vertex vertex);

}
