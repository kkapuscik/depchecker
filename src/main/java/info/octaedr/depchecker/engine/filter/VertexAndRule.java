package info.octaedr.depchecker.engine.filter;

import info.octaedr.depchecker.engine.graph.Vertex;

/**
 * Logical AND vertex selection rule.
 * 
 * <p>
 * Returns true only if there is at least one child rule and all of the
 * child rules returned true. 
 */
public class VertexAndRule extends VertexLogicRule {

    @Override
    public boolean accept(final Vertex vertex) {
        boolean result = false;

        final int childCount = getChildCount();
        if (childCount > 0) {
            result = true;

            for (int i = 0; result && (i < childCount); ++i) {
                result &= getChild(i).accept(vertex);
            }
        }

        return result;
    }

}
