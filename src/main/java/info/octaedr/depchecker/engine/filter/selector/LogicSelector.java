package info.octaedr.depchecker.engine.filter.selector;

import java.util.ArrayList;

/**
 * Base class for selector representing logic operators.
 * 
 * @param <T>
 *            Selected object type.
 */
public abstract class LogicSelector<T> implements Selector<T> {

    /**
     * Limit of the child count.
     * 
     * <p>
     * The operation may set even lower limit.
     */
    public static final int CHILD_COUNT_LIMIT = 10;

    /**
     * Collection of children selectors.
     */
    private final ArrayList<Selector<T>> children = new ArrayList<Selector<T>>();

    /**
     * Constructs the selector.
     */
    protected LogicSelector() {
        // nothing to do
    }

    @Override
    public boolean validate() {
        if (this.children.size() < getMinChildCount()) {
            return false;
        }
        if (this.children.size() > getMaxChildCount()) {
            return false;
        }

        for (final Selector<T> child : this.children) {
            if (!child.validate()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns minimum required number of children.
     * 
     * <p>
     * Default implementation returns 1. Operation shall override this function
     * if needed.
     * 
     * @return Number of children.
     */
    public int getMinChildCount() {
        return 1;
    }

    /**
     * Returns maximum required number of children.
     * 
     * <p>
     * Default implementation returns {@link #CHILD_COUNT_LIMIT}. Operation
     * shall override this function if needed.
     * 
     * @return Number of children.
     */
    public int getMaxChildCount() {
        return CHILD_COUNT_LIMIT;
    }

    /**
     * Returns child at given position.
     * 
     * @param index
     *            Index of the child to get.
     * 
     * @return Child at specified position.
     */
    public Selector<T> getChild(final int index) {
        return this.children.get(index);
    }

    /**
     * Adds child as the last one.
     * 
     * @param child
     *            Child to be added.
     * 
     * @return True if child was successfully added, false it there was no more
     *         space in the collection.
     */
    public boolean addChild(final Selector<T> child) {
        assert child != null;

        if (this.children.size() == getMaxChildCount()) {
            return false;
        }

        // TODO: check for circular dependencies?

        this.children.add(child);

        return true;
    }

    /**
     * Adds child at given position.
     * 
     * @param index
     *            Index at which child will be added.
     * @param child
     *            Child to be added.
     * 
     * @return True if child was successfully added, false it there was no more
     *         space in the collection.
     */
    public boolean addChild(final int index, final Selector<T> child) {
        assert child != null;

        if (this.children.size() == getMaxChildCount()) {
            return false;
        }

        // TODO: check for circular dependencies?

        this.children.add(index, child);

        return true;
    }

    /**
     * Removes specified child.
     * 
     * @param child
     *            Child to be removed.
     * 
     * @return True if the given child was successfully removed, false if the
     *         given child was invalid (not a child of this object).
     */
    public boolean removeChild(final Selector<T> child) {
        assert child != null;

        return this.children.remove(child);
    }

    /**
     * Returns interface for iterating over collection of children.
     * 
     * @return The interface for iterating.
     */
    protected Iterable<Selector<T>> getChildren() {
        return this.children;
    }

}
