package info.octaedr.depchecker.engine;

import java.util.ArrayList;

/**
 * Manager of engine elements.
 * 
 * <p>
 * This is the main class of the engine. The class task is to manage the engine
 * elements like data processors providing input graphs and analyzers providing
 * output graphs.
 */
public class EngineManager {

    /**
     * Collection of registered input processors.
     */
    private final ArrayList<InputProcessor> inputProcessors;

    /**
     * Collection of registered data analyzers.
     */
    private final ArrayList<DataAnalyzer> dataAnalyzers;

    /**
     * Constructs the manager.
     */
    public EngineManager() {
        this.inputProcessors = new ArrayList<>();
        this.dataAnalyzers = new ArrayList<>();
    }

    /**
     * Registers input processor.
     * 
     * @param processor
     *            Input processor to be registered.
     */
    public void registerProcessor(final InputProcessor processor) {
        assert processor != null;

        this.inputProcessors.add(processor);
    }

    /**
     * Returns number of input processors.
     * 
     * @return Number of available processors.
     */
    public int getProcessorCount() {
        return this.inputProcessors.size();
    }

    /**
     * Returns registered processor.
     * 
     * @param index
     *            Index of the processor to get.
     * 
     * @return Processor object.
     */
    public InputProcessor getProcessor(final int index) {
        return this.inputProcessors.get(index);
    }

    /**
     * Registers data analyzer.
     * 
     * @param analyzer
     *            Analyzer to be registered.
     */
    public void registerAnalyzer(final DataAnalyzer analyzer) {
        assert analyzer != null;

        this.dataAnalyzers.add(analyzer);
    }

    /**
     * Returns number of data analyzers.
     * 
     * @return Number of available data analyzers.
     */
    public int getAnalyzerCount() {
        return this.dataAnalyzers.size();
    }

    /**
     * Returns registered analyzer.
     * 
     * @param index
     *            Index of the analyzer to get.
     * 
     * @return Analyzer object.
     */
    public DataAnalyzer getAnalyzer(final int index) {
        return this.dataAnalyzers.get(index);
    }
}
