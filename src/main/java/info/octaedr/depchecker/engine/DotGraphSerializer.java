package info.octaedr.depchecker.engine;

import info.octaedr.depchecker.engine.graph.DependencyGraph;
import info.octaedr.depchecker.engine.graph.DependencyGraphException;
import info.octaedr.depchecker.engine.graph.RelationEdge;
import info.octaedr.depchecker.engine.graph.Vertex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * Serializer of the graph to dot format.
 */
public class DotGraphSerializer {

    /**
     * Cluster subgraph identifier prefix.
     */
    private static final String CLUSTER_PREFIX = "cluster_";

    /**
     * Builder for creating the identifiers.
     */
    private final StringBuilder idBuilder = new StringBuilder();

    /**
     * Constructs the serializer.
     */
    public DotGraphSerializer() {
        // nothing to do
    }

    /**
     * Serialize the graph.
     * 
     * @param graph
     *            Graph to be serialized.
     * @param file
     *            File to which graph in serialized form shall be written.
     * 
     * @return True if graph was successfully serialized, false otherwise.
     */
    public boolean serialize(final DependencyGraph graph, final File file) {
        boolean result = false;

        try {
            final PrintWriter writer = new PrintWriter(new FileOutputStream(file));

            serializeGraph(graph, writer);

            writer.flush();
            writer.close();

            result = true;
        } catch (final DependencyGraphException e) {
            // TODO messages
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            // TODO messages
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Serialize the graph.
     * 
     * @param graph
     *            Graph to be serialized.
     * @param stream
     *            Stream for storing serialized data.
     * 
     * @return True if graph was successfully serialized, false otherwise.
     */
    public boolean serialize(final DependencyGraph graph, final OutputStream stream) {
        boolean result = false;

        try {
            final PrintWriter writer = new PrintWriter(stream);

            serializeGraph(graph, writer);

            writer.flush();

            result = true;
        } catch (final DependencyGraphException e) {
            // TODO messages
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Serializes the graph.
     * 
     * @param graph
     *            Graph to be serialized.
     * @param writer
     *            Object for writing serialized data.
     * 
     * @throws DependencyGraphException
     *             if any graph operation has failed.
     */
    private void serializeGraph(final DependencyGraph graph, final PrintWriter writer)
            throws DependencyGraphException {
        writer.write("digraph ");
        writer.write(prepareRootId());
        writer.write(" {\n");

        for (final Vertex rootVertex : graph.getRootVertices()) {
            if (graph.isGroupVertex(rootVertex)) {
                serializeGroupVertex(graph, rootVertex, writer);
            } else {
                serializeSimpleVertex(graph, rootVertex, writer);
            }
        }

        if (graph.getName() != null) {
            writer.write("label = \"");
            writer.write(graph.getName());
            writer.write("\";\n");
        }

        serializeEdges(graph, writer);

        writer.write("}\n");
    }

    /**
     * Serializes graph data.
     * 
     * @param graph
     *            Graph being serialized.
     * @param writer
     *            Object for writing serialized data.
     */
    private void serializeEdges(final DependencyGraph graph, final PrintWriter writer) {
        for (final Vertex sourceVertex : graph.getAllVertices()) {
            for (final RelationEdge relationEdge : graph.getAllOutRelations(sourceVertex)) {
                final Vertex targetVertex = graph.getRelationTarget(relationEdge);

                serializeEdge(sourceVertex, targetVertex, writer);
            }
        }
    }

    /**
     * Serializes single graph edge.
     * 
     * <p>
     * The serialized edge is directed.
     * 
     * @param sourceVertex
     *            Edge source vertex.
     * @param targetVertex
     *            Edge target vertex.
     * @param writer
     *            Object for writing serialized data.
     */
    private void serializeEdge(final Vertex sourceVertex, final Vertex targetVertex,
            final PrintWriter writer) {
        writer.write(prepareId(sourceVertex));
        writer.write(" -> ");
        writer.write(prepareId(targetVertex));
        writer.write("\n");
    }

    /**
     * Serializes group vertex.
     * 
     * <p>
     * Group vertex is a vertex that have a subvertices in structure graph.
     * 
     * @param graph
     *            Graph being serialized.
     * @param groupVertex
     *            Vertex to be serialized.
     * @param writer
     *            Object for writing serialized data.
     * 
     * @throws DependencyGraphException
     *             if any graph operation has failed.
     */
    private void serializeGroupVertex(final DependencyGraph graph, final Vertex groupVertex,
            final PrintWriter writer) throws DependencyGraphException {
        writer.write("subgraph ");

        writer.write(prepareId(groupVertex));
        writer.write(" {\n");

        if (groupVertex.getName() != null) {
            writer.write("label = \"");
            writer.write(groupVertex.getName());
            writer.write("\";\n");
        }

        for (final Vertex subVertex : graph.getSubvertices(groupVertex)) {
            if (graph.isGroupVertex(subVertex)) {
                serializeGroupVertex(graph, subVertex, writer);
            } else {
                serializeSimpleVertex(graph, subVertex, writer);
            }
        }

        writer.write("}\n");
    }

    /**
     * Serializes simple vertex.
     * 
     * <p>
     * Simple vertex is a vertex that does not have a subvertices in structure
     * graph.
     * 
     * @param graph
     *            Graph being serialized.
     * @param simpleVertex
     *            Vertex to be serialized.
     * @param writer
     *            Object for writing serialized data.
     */
    private void serializeSimpleVertex(final DependencyGraph graph, final Vertex simpleVertex,
            final PrintWriter writer) {
        writer.write(prepareId(simpleVertex));

        if (simpleVertex.getName() != null) {
            writer.write(" [ label = \"");
            writer.write(simpleVertex.getName());
            writer.write("\" ]");
        }

        writer.write(";\n");
    }

    /**
     * Prepares root (graph) identifier.
     * 
     * @return Prepared identifier.
     */
    private String prepareRootId() {
        return encodeId(null, "root");
    }

    /**
     * Prepares dot-compatible id for the vertex.
     * 
     * @param vertex
     *            Vertex for which id to prepare.
     * 
     * @return Prepared id.
     */
    private String prepareId(final Vertex vertex) {
        if (vertex.isCluster()) {
            return encodeId(CLUSTER_PREFIX, vertex.getUniqueId());
        } else {
            return encodeId(null, vertex.getUniqueId());
        }
    }

    /**
     * Encodes identifier.
     * 
     * @param prefix
     *            Prefix to be added to identifier. May be null. It is also
     *            encoded.
     * @param id
     *            Identifier to be encoded.
     * 
     * @return Encoded identifier.
     */
    private String encodeId(final String prefix, final String id) {
        this.idBuilder.setLength(0);

        if (prefix != null) {
            for (int i = 0; i < prefix.length(); ++i) {
                this.idBuilder.append(encodeIdChar(prefix.charAt(i)));
            }
        }

        for (int i = 0; i < id.length(); ++i) {
            this.idBuilder.append(encodeIdChar(id.charAt(i)));
        }

        return this.idBuilder.toString();
    }

    /**
     * Encodes single identifier character.
     * 
     * @param c
     *            Character to encode.
     * 
     * @return Encoded character.
     */
    private Object encodeIdChar(char c) {
        if (c >= '0' && c <= '9') {
            // ok
        } else if (c >= 'a' && c <= 'z') {
            // ok
        } else if (c >= 'A' && c <= 'Z') {
            // ok
        } else if (c == '_') {
            // ok
        } else {
            c = '_';
        }

        return c;
    }

}
