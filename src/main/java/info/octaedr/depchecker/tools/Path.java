package info.octaedr.depchecker.tools;

import java.io.File;

/**
 * Path manipulation utilities.
 */
public class Path {

    /**
     * Forbid construction.
     */
    private Path() {
        // nothing to do
    }

    /**
     * Relativizes the path to element.
     * 
     * @param base
     *            Path against which element path is relativized. If represents
     *            a file the relativization is done against its parent.
     * @param element
     *            Path which shall be relativized.
     * 
     * @return Relativized path.
     */
    static public String relativize(final File base, final File element) {
        assert base != null;
        assert element != null;

        File baseDir;
        if (base.isFile()) {
            baseDir = base.getParentFile();
        } else {
            baseDir = base;
        }

        String relativizedPath;
        if (baseDir != null) {
            relativizedPath = baseDir.toURI().relativize(element.toURI()).getPath();
        } else {
            relativizedPath = element.getAbsolutePath();
        }

        return relativizedPath;
    }

    /**
     * Resolves the element path against given base.
     * 
     * @param base
     *            Base to resolve against.
     * @param element
     *            Path to be resolved.
     * 
     * @return Resolved path.
     */
    static public File resolve(final File base, final String element) {
        return new File(base.toURI().resolve(element));
    }

}
