package info.octaedr.depchecker;

/**
 * Fatal application exception.
 */
public class FatalException extends RuntimeException {

    /**
     * Generated serial version UID.
     */
    private static final long serialVersionUID = -1918974803591294904L;

    /**
     * Constructs exception.
     */
    public FatalException() {
        super();
    }

    /**
     * Constructs exception.
     * 
     * @param message
     *            Reason message.
     */
    public FatalException(final String message) {
        super(message);
    }

    /**
     * Constructs exception.
     * 
     * @param cause
     *            The cause.
     */
    public FatalException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs exception.
     * 
     * @param message
     *            Reason message.
     * @param cause
     *            The cause.
     */
    public FatalException(String message, Throwable cause) {
        super(message, cause);
    }

}
