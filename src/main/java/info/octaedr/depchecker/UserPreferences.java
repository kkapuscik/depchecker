package info.octaedr.depchecker;

import java.util.prefs.Preferences;

/**
 * Class for handling user preferences
 */
public class UserPreferences {
	
    /**
     * Persistent storage
     */
    private static Preferences storage= Preferences.userNodeForPackage(UserPreferences.class);

    /**
     * Key for doxygen path
     */
    private static final String DOXYGEN_PATH_ID = "DoxygenPath";
    
    /**
     * Key for graphviz path
     */
    private static final String GRAPHVIZ_PATH_ID = "GraphvizPath";

    /**
     * Gets doxygen path from persistent storage
     */
    public static String getDoxygenPath(){
    	return UserPreferences.storage.get(DOXYGEN_PATH_ID, null);
    }

    /**
     * Stores doxygen path persistently
     */
    public static void setDoxygenPath(String newDoxygenPath){
    	UserPreferences.storage.put(DOXYGEN_PATH_ID, newDoxygenPath);
    }
    
    /**
     * Gets graphviz path from persistent storage
     */
    public static String getGraphvizPath(){
    	return UserPreferences.storage.get(GRAPHVIZ_PATH_ID, null);
    }

    /**
     * Stores graphviz path persistently
     */
    public static void setGraphvizPath(String newDoxygenPath){
    	UserPreferences.storage.put(GRAPHVIZ_PATH_ID, newDoxygenPath);
    }
}
