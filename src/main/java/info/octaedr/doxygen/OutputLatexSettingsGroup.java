package info.octaedr.doxygen;
/** 
 * Configuration options related to the LaTeX output.
 */
public class OutputLatexSettingsGroup extends SettingsGroup {

    /**
     * If the GENERATE_LATEX tag is set to YES doxygen will generate
     * $\mbox{\LaTeX}$ output.
     * 
     * The default value is: YES.
     */
    public static final String GENERATE_LATEX = "GENERATE_LATEX";

    /**
     * The LATEX_OUTPUT tag is used to specify where the $\mbox{\LaTeX}$ docs
     * will be put. If a relative path is entered the value of OUTPUT_DIRECTORY
     * will be put in front of it.
     * 
     * The default directory is: latex.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String LATEX_OUTPUT = "LATEX_OUTPUT";

    /**
     * The LATEX_CMD_NAME tag can be used to specify the $\mbox{\LaTeX}$ command
     * name to be invoked. Note that when enabling USE_PDFLATEX this option is
     * only used for generating bitmaps for formulas in the HTML output, but not
     * in the Makefile that is written to the output directory.
     * 
     * The default file is: latex.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String LATEX_CMD_NAME = "LATEX_CMD_NAME";

    /**
     * The MAKEINDEX_CMD_NAME tag can be used to specify the command name to
     * generate index for $\mbox{\LaTeX}$.
     * 
     * The default file is: makeindex.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String MAKEINDEX_CMD_NAME = "MAKEINDEX_CMD_NAME";

    /**
     * If the COMPACT_LATEX tag is set to YES doxygen generates more compact
     * $\mbox{\LaTeX}$ documents. This may be useful for small projects and may
     * help to save some trees in general.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String COMPACT_LATEX = "COMPACT_LATEX";

    /**
     * The PAPER_TYPE tag can be used to set the paper type that is used by the
     * printer.
     * 
     * Possible values are: a4 (210 x 297 mm), letter (8.5 x 11 inches), legal
     * (8.5 x 14 inches) and executive (7.25 x 10.5 inches).
     * 
     * The default value is: a4.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String PAPER_TYPE = "PAPER_TYPE";

    /**
     * The EXTRA_PACKAGES tag can be used to specify one or more $\mbox{\LaTeX}$
     * that should be included in the $\mbox{\LaTeX}$ output. To get the times
     * font for instance you can specify EXTRA_PACKAGES=times If left blank no
     * extra packages will be included.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String EXTRA_PACKAGES = "EXTRA_PACKAGES";

    /**
     * The LATEX_HEADER tag can be used to specify a personal $\mbox{\LaTeX}$
     * header for the generated $\mbox{\LaTeX}$ document. The header should
     * contain everything until the first chapter.
     * 
     * If it is left blank doxygen will generate a standard header. See section
     * Doxygen usage for information on how to let doxygen write the default
     * header to a separate file.
     * 
     * Note: Only use a user-defined header if you know what you are doing!
     * 
     * The following commands have a special meaning inside the header: $title,
     * $datetime, $date, $doxygenversion, $projectname, $projectnumber. Doxygen
     * will replace them by respectively the title of the page, the current date
     * and time, only the current date, the version number of doxygen, the
     * project name (see PROJECT_NAME), or the project number (see
     * PROJECT_NUMBER).
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String LATEX_HEADER = "LATEX_HEADER";

    /**
     * The LATEX_FOOTER tag can be used to specify a personal $\mbox{\LaTeX}$
     * footer for the generated $\mbox{\LaTeX}$ document. The footer should
     * contain everything after the last chapter. If it is left blank doxygen
     * will generate a standard footer.
     * 
     * Note: Only use a user-defined footer if you know what you are doing!
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String LATEX_FOOTER = "LATEX_FOOTER";

    /**
     * The LATEX_EXTRA_FILES tag can be used to specify one or more extra images
     * or other source files which should be copied to the LATEX_OUTPUT output
     * directory. Note that the files will be copied as-is; there are no commands
     * or markers available.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String LATEX_EXTRA_FILES = "LATEX_EXTRA_FILES";

    /**
     * If the PDF_HYPERLINKS tag is set to YES, the $\mbox{\LaTeX}$ that is
     * generated is prepared for conversion to PDF (using ps2pdf or pdflatex).
     * The PDF file will contain links (just like the HTML output) instead of
     * page references. This makes the output suitable for online browsing using
     * a PDF viewer.
     * 
     * The default value is: YES.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String PDF_HYPERLINKS = "PDF_HYPERLINKS";

    /**
     * If the LATEX_PDFLATEX tag is set to YES, doxygen will use pdflatex to
     * generate the PDF file directly from the $\mbox{\LaTeX}$ files. Set this
     * option to YES to get a higher quality PDF documentation.
     * 
     * The default value is: YES.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String USE_PDFLATEX = "USE_PDFLATEX";

    /**
     * If the LATEX_BATCHMODE tag is set to YES, doxygen will add the \batchmode
     * command to the generated $\mbox{\LaTeX}$ files. This will instruct
     * $\mbox{\LaTeX}$ to keep running if errors occur, instead of asking the
     * user for help. This option is also used when generating formulas in HTML.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String LATEX_BATCHMODE = "LATEX_BATCHMODE";

    /**
     * If the LATEX_HIDE_INDICES tag is set to YES then doxygen will not include
     * the index chapters (such as File Index, Compound Index, etc.) in the
     * output.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String LATEX_HIDE_INDICES = "LATEX_HIDE_INDICES";

    /**
     * If the LATEX_SOURCE_CODE tag is set to YES then doxygen will include
     * source code with syntax highlighting in the $\mbox{\LaTeX}$ output. Note
     * that which sources are shown also depends on other settings such as
     * SOURCE_BROWSER.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String LATEX_SOURCE_CODE = "LATEX_SOURCE_CODE";

    /**
     * The LATEX_BIB_STYLE tag can be used to specify the style to use for the
     * bibliography, e.g. plainnat, or ieeetr. See
     * http://en.wikipedia.org/wiki/BibTeX and \cite for more info.
     * 
     * The default value is: plain.
     * 
     * This tag requires that the tag GENERATE_LATEX is set to YES.
     */
    public static final String LATEX_BIB_STYLE = "LATEX_BIB_STYLE";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public OutputLatexSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
