package info.octaedr.doxygen;
/**
 * Build related configuration options
 */
public class BuildSettingsGroup extends SettingsGroup {

    /**
     * If the EXTRACT_ALL tag is set to YES doxygen will assume all entities in
     * documentation are documented, even if no documentation was available.
     * Private class members and static file members will be hidden unless the
     * EXTRACT_PRIVATE respectively EXTRACT_STATIC tags are set to YES.
     * 
     * Note: This will also disable the warnings about undocumented members that
     * are normally produced when WARNINGS is set to YES.
     * 
     * The default value is: NO.
     */
    public static final String EXTRACT_ALL = "EXTRACT_ALL";

    /**
     * If the EXTRACT_PRIVATE tag is set to YES all private members of a class
     * will be included in the documentation.
     * 
     * The default value is: NO.
     */
    public static final String EXTRACT_PRIVATE = "EXTRACT_PRIVATE";

    /**
     * If the EXTRACT_PACKAGE tag is set to YES all members with package or
     * internal scope will be included in the documentation.
     * 
     * The default value is: NO.
     */
    public static final String EXTRACT_PACKAGE = "EXTRACT_PACKAGE";

    /**
     * If the EXTRACT_STATIC tag is set to YES all static members of a file will
     * be included in the documentation.
     * 
     * The default value is: NO.
     */
    public static final String EXTRACT_STATIC = "EXTRACT_STATIC";

    /**
     * If the EXTRACT_LOCAL_CLASSES tag is set to YES classes (and structs)
     * defined locally in source files will be included in the documentation. If
     * set to NO only classes defined in header files are included. Does not
     * have any effect for Java sources.
     * 
     * The default value is: YES.
     */
    public static final String EXTRACT_LOCAL_CLASSES = "EXTRACT_LOCAL_CLASSES";

    /**
     * This flag is only useful for Objective-C code. When set to YES local
     * methods, which are defined in the implementation section but not in the
     * interface are included in the documentation. If set to NO only methods in
     * the interface are included.
     * 
     * The default value is: NO.
     */
    public static final String EXTRACT_LOCAL_METHODS = "EXTRACT_LOCAL_METHODS";

    /**
     * If this flag is set to YES, the members of anonymous namespaces will be
     * extracted and appear in the documentation as a namespace called
     * 'anonymous_namespace{file}', where file will be replaced with the base
     * name of the file that contains the anonymous namespace. By default
     * anonymous namespace are hidden.
     * 
     * The default value is: NO.
     */
    public static final String EXTRACT_ANON_NSPACES = "EXTRACT_ANON_NSPACES";

    /**
     * If the HIDE_UNDOC_MEMBERS tag is set to YES, doxygen will hide all
     * undocumented members inside documented classes or files. If set to NO
     * these members will be included in the various overviews, but no
     * documentation section is generated. This option has no effect if
     * EXTRACT_ALL is enabled.
     * 
     * The default value is: NO.
     */
    public static final String HIDE_UNDOC_MEMBERS = "HIDE_UNDOC_MEMBERS";

    /**
     * If the HIDE_UNDOC_CLASSES tag is set to YES, doxygen will hide all
     * undocumented classes that are normally visible in the class hierarchy. If
     * set to NO these classes will be included in the various overviews. This
     * option has no effect if EXTRACT_ALL is enabled.
     * 
     * The default value is: NO.
     */
    public static final String HIDE_UNDOC_CLASSES = "HIDE_UNDOC_CLASSES";

    /**
     * If the HIDE_FRIEND_COMPOUNDS tag is set to YES, doxygen will hide all
     * friend (class|struct|union) declarations. If set to NO these declarations
     * will be included in the documentation.
     * 
     * The default value is: NO.
     */
    public static final String HIDE_FRIEND_COMPOUNDS = "HIDE_FRIEND_COMPOUNDS";

    /**
     * If the HIDE_IN_BODY_DOCS tag is set to YES, doxygen will hide any
     * documentation blocks found inside the body of a function. If set to NO
     * these blocks will be appended to the function's detailed documentation
     * block.
     * 
     * The default value is: NO.
     */
    public static final String HIDE_IN_BODY_DOCS = "HIDE_IN_BODY_DOCS";

    /**
     * The INTERNAL_DOCS tag determines if documentation that is typed after a
     * \internal command is included. If the tag is set to NO then the
     * documentation will be excluded. Set it to YES to include the internal
     * documentation.
     * 
     * The default value is: NO.
     */
    public static final String INTERNAL_DOCS = "INTERNAL_DOCS";

    /**
     * If the CASE_SENSE_NAMES tag is set to NO then doxygen will only generate
     * file names in lower-case letters. If set to YES upper-case letters are
     * also allowed. This is useful if you have classes or files whose names
     * only differ in case and if your file system supports case sensitive file
     * names. Windows and Mac users are advised to set this option to NO.
     * 
     * The default value is: system dependent.
     */
    public static final String CASE_SENSE_NAMES = "CASE_SENSE_NAMES";

    /**
     * If the HIDE_SCOPE_NAMES tag is set to NO then doxygen will show members
     * with their full class and namespace scopes in the documentation. If set
     * to YES the scope will be hidden.
     * 
     * The default value is: NO.
     */
    public static final String HIDE_SCOPE_NAMES = "HIDE_SCOPE_NAMES";

    /**
     * If the SHOW_INCLUDE_FILES tag is set to YES then doxygen will put a list
     * of the files that are included by a file in the documentation of that
     * file.
     * 
     * The default value is: YES.
     */
    public static final String SHOW_INCLUDE_FILES = "SHOW_INCLUDE_FILES";

    /**
     * If the SHOW_GROUPED_MEMB_INC tag is set to YES then Doxygen will add for
     * each grouped member an include statement to the documentation, telling
     * the reader which file to include in order to use the member.
     * 
     * The default value is: NO.
     */
    public static final String SHOW_GROUPED_MEMB_INC = "SHOW_GROUPED_MEMB_INC";

    /**
     * If the FORCE_LOCAL_INCLUDES tag is set to YES then doxygen will list
     * include files with double quotes in the documentation rather than with
     * sharp brackets.
     * 
     * The default value is: NO.
     */
    public static final String FORCE_LOCAL_INCLUDES = "FORCE_LOCAL_INCLUDES";

    /**
     * If the INLINE_INFO tag is set to YES then a tag [inline] is inserted in
     * the documentation for inline members.
     * 
     * The default value is: YES.
     */
    public static final String INLINE_INFO = "INLINE_INFO";

    /**
     * If the SORT_MEMBER_DOCS tag is set to YES then doxygen will sort the
     * (detailed) documentation of file and class members alphabetically by
     * member name. If set to NO the members will appear in declaration order.
     * 
     * The default value is: YES.
     */
    public static final String SORT_MEMBER_DOCS = "SORT_MEMBER_DOCS";

    /**
     * If the SORT_BRIEF_DOCS tag is set to YES then doxygen will sort the brief
     * descriptions of file, namespace and class members alphabetically by
     * member name. If set to NO the members will appear in declaration order.
     * Note that this will also influence the order of the classes in the class
     * list.
     * 
     * The default value is: NO.
     */
    public static final String SORT_BRIEF_DOCS = "SORT_BRIEF_DOCS";

    /**
     * If the SORT_MEMBERS_CTORS_1ST tag is set to YES then doxygen will sort
     * the (brief and detailed) documentation of class members so that
     * constructors and destructors are listed first. If set to NO the
     * constructors will appear in the respective orders defined by
     * SORT_BRIEF_DOCS and SORT_MEMBER_DOCS.
     * 
     * Note If SORT_BRIEF_DOCS is set to NO this option is ignored for sorting
     * brief member documentation. If SORT_MEMBER_DOCS is set to NO this option
     * is ignored for sorting detailed member documentation.
     * 
     * The default value is: NO.
     */
    public static final String SORT_MEMBERS_CTORS_1ST = "SORT_MEMBERS_CTORS_1ST";

    /**
     * If the SORT_GROUP_NAMES tag is set to YES then doxygen will sort the
     * hierarchy of group names into alphabetical order. If set to NO the group
     * names will appear in their defined order.
     * 
     * The default value is: NO.
     */
    public static final String SORT_GROUP_NAMES = "SORT_GROUP_NAMES";

    /**
     * If the SORT_BY_SCOPE_NAME tag is set to YES, the class list will be
     * sorted by fully-qualified names, including namespaces. If set to NO, the
     * class list will be sorted only by class name, not including the namespace
     * part.
     * 
     * Note This option is not very useful if HIDE_SCOPE_NAMES is set to YES.
     * This option applies only to the class list, not to the alphabetical list.
     * 
     * The default value is: NO.
     */
    public static final String SORT_BY_SCOPE_NAME = "SORT_BY_SCOPE_NAME";

    /**
     * If the STRICT_PROTO_MATCHING option is enabled and doxygen fails to do
     * proper type resolution of all parameters of a function it will reject a
     * match between the prototype and the implementation of a member function
     * even if there is only one candidate or it is obvious which candidate to
     * choose by doing a simple string match. By disabling STRICT_PROTO_MATCHING
     * doxygen will still accept a match between prototype and implementation in
     * such cases.
     * 
     * The default value is: NO.
     */
    public static final String STRICT_PROTO_MATCHING = "STRICT_PROTO_MATCHING";

    /**
     * The GENERATE_TODOLIST tag can be used to enable (YES) or disable (NO) the
     * todo list. This list is created by putting \todo commands in the
     * documentation.
     * 
     * The default value is: YES.
     */
    public static final String GENERATE_TODOLIST = "GENERATE_TODOLIST";

    /**
     * The GENERATE_TESTLIST tag can be used to enable (YES) or disable (NO) the
     * test list. This list is created by putting \test commands in the
     * documentation.
     * 
     * The default value is: YES.
     */
    public static final String GENERATE_TESTLIST = "GENERATE_TESTLIST";

    /**
     * The GENERATE_BUGLIST tag can be used to enable (YES) or disable (NO) the
     * bug list. This list is created by putting \bug commands in the
     * documentation.
     * 
     * The default value is: YES.
     */
    public static final String GENERATE_BUGLIST = "GENERATE_BUGLIST";

    /**
     * The GENERATE_DEPRECATEDLIST tag can be used to enable (YES) or disable
     * (NO) the deprecated list. This list is created by putting \deprecated
     * commands in the documentation.
     * 
     * The default value is: YES.
     */
    public static final String GENERATE_DEPRECATEDLIST = "GENERATE_DEPRECATEDLIST";

    /**
     * The ENABLED_SECTIONS tag can be used to enable conditional documentation
     * sections, marked by \if <section_label> ... \endif and \cond
     * <section_label> ... \endcond blocks.
     */
    public static final String ENABLED_SECTIONS = "ENABLED_SECTIONS";

    /**
     * The MAX_INITIALIZER_LINES tag determines the maximum number of lines that
     * the initial value of a variable or macro / define can have for it to
     * appear in the documentation. If the initializer consists of more lines
     * than specified here it will be hidden. Use a value of 0 to hide
     * initializers completely. The appearance of the value of individual
     * variables and macros / defines can be controlled using \showinitializer
     * or \hideinitializer command in the documentation regardless of this
     * setting.
     * 
     * Minimum value: 0, maximum value: 10000, default value: 30.
     */
    public static final String MAX_INITIALIZER_LINES = "MAX_INITIALIZER_LINES";

    /**
     * Set the SHOW_USED_FILES tag to NO to disable the list of files generated
     * at the bottom of the documentation of classes and structs. If set to YES
     * the list will mention the files that were used to generate the
     * documentation.
     * 
     * The default value is: YES.
     */
    public static final String SHOW_USED_FILES = "SHOW_USED_FILES";

    /**
     * Set the SHOW_FILES tag to NO to disable the generation of the Files page.
     * This will remove the Files entry from the Quick Index and from the Folder
     * Tree View (if specified).
     * 
     * The default value is: YES.
     */
    public static final String SHOW_FILES = "SHOW_FILES";

    /**
     * Set the SHOW_NAMESPACES tag to NO to disable the generation of the
     * Namespaces page. This will remove the Namespaces entry from the Quick
     * Index and from the Folder Tree View (if specified).
     * 
     * The default value is: YES.
     */
    public static final String SHOW_NAMESPACES = "SHOW_NAMESPACES";

    /**
     * The FILE_VERSION_FILTER tag can be used to specify a program or script
     * that doxygen should invoke to get the current version for each file
     * (typically from the version control system). Doxygen will invoke the
     * program by executing (via popen()) the command command input-file, where
     * command is the value of the FILE_VERSION_FILTER tag, and input-file is
     * the name of an input file provided by doxygen. Whatever the program
     * writes to standard output is used as the file version.Example of using a
     * shell script as a filter for Unix:
     */
    public static final String FILE_VERSION_FILTER = "FILE_VERSION_FILTER";

    /**
     * The LAYOUT_FILE tag can be used to specify a layout file which will be
     * parsed by doxygen. The layout file controls the global structure of the
     * generated output files in an output format independent way. To create the
     * layout file that represents doxygen's defaults, run doxygen with the -l
     * option. You can optionally specify a file name after the option, if
     * omitted DoxygenLayout.xml will be used as the name of the layout file.
     * Note that if you run doxygen from a directory containing a file called
     * DoxygenLayout.xml, doxygen will parse it automatically even if the
     * LAYOUT_FILE tag is left empty.
     */
    public static final String LAYOUT_FILE = "LAYOUT_FILE";

    /**
     * The CITE_BIB_FILES tag can be used to specify one or more bib files
     * containing the reference definitions. This must be a list of .bib files.
     * The .bib extension is automatically appended if omitted. This requires
     * the bibtex tool to be installed. See also
     * http://en.wikipedia.org/wiki/BibTeX for more info. For $\mbox{\LaTeX}$
     * the style of the bibliography can be controlled using LATEX_BIB_STYLE. To
     * use this feature you need bibtex and perl available in the search path.
     * Do not use file names with spaces, bibtex cannot handle them. See also
     * \cite for info how to create references.
     */
    public static final String CITE_BIB_FILES = "CITE_BIB_FILES";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public BuildSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
