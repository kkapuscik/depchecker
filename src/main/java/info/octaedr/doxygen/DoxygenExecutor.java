package info.octaedr.doxygen;

import info.octaedr.cmd.ProcessExecutor;
import info.octaedr.depchecker.UserPreferences;

import java.io.File;

/**
 * Utility for running Doxygen.
 */
public class DoxygenExecutor extends ProcessExecutor {

    /**
     * Doxygen configuration file name.
     */
    private File configFile;

    /**
     * Constructs the executor.
     * 
     * <p>
     * Tries to set the executable name using default name and searching the
     * path.
     */
    public DoxygenExecutor() {
        setExecutable(new File(UserPreferences.getDoxygenPath()));
    }

    /**
     * Constructs the executor.
     * 
     * <p>
     * Sets the executable to the one given/
     * 
     * @param exeFile
     *            Executable file to be run.
     */
    public DoxygenExecutor(final File exeFile) {
        setExecutable(exeFile);
    }

    /**
     * Returns configuration file.
     * 
     * @return Configuration file if set, null otherwise.
     */
    public File getConfigFile() {
        return this.configFile;
    }

    /**
     * Sets configuration file to be passed to doxygen.
     * 
     * @param configFile
     *            Configuration file. Null to unset.
     */
    public void setConfigFile(final File configFile) {
        this.configFile = configFile;
    }

    @Override
    public boolean run() {
        clearArguments();
        if (this.configFile != null) {
            addArgument("-g");
            addArgument(this.configFile.getAbsolutePath());
        }

        return super.run();
    }

}
