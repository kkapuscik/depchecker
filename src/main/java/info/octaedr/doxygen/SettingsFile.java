package info.octaedr.doxygen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Doxygen settings file.
 */
public class SettingsFile {

    /**
     * File in which settings are stored.
     */
    private final File file;

    /**
     * Constructs the file.
     * 
     * @param file
     *            The settings file descriptor.
     */
    public SettingsFile(final File file) {
        assert file != null;

        this.file = file;
    }

    /**
     * Returns the settings file descriptor.
     * 
     * @return The settings file.
     */
    public File getFile() {
        return this.file;
    }

    /**
     * Saves settings to a file.
     * 
     * @param settings
     *            Settings to be saved.
     * 
     * @return True if settings were successfully stored, false otherwise.
     */
    public boolean saveSettings(final Settings settings) {
        assert settings != null;

        try {
            final FileWriter writer = new FileWriter(this.file);

            for (String key : settings.getKeys()) {
                writer.write(key);
                writer.write(" = ");
                writer.write(settings.get(key));
                writer.write('\n');
            }

            writer.close();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
