package info.octaedr.doxygen;
/**
 * Configuration options related to the Perl module output
 */
public class OutputPerlModSettingsGroup extends SettingsGroup {

    /**
     * If the GENERATE_PERLMOD tag is set to YES doxygen will generate a Perl
     * module file that captures the structure of the code including all
     * documentation. Note that this feature is still experimental and incomplete
     * at the moment.
     * 
     * The default value is: NO.
     */
    public static final String GENERATE_PERLMOD = "GENERATE_PERLMOD";

    /**
     * If the PERLMOD_LATEX tag is set to YES doxygen will generate the
     * necessary Makefile rules, Perl scripts and $\mbox{\LaTeX}$ code to be
     * able to generate PDF and DVI output from the Perl module output.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_PERLMOD is set to YES.
     */
    public static final String PERLMOD_LATEX = "PERLMOD_LATEX";

    /**
     * If the PERLMOD_PRETTY tag is set to YES the Perl module output will be
     * nicely formatted so it can be parsed by a human reader. This is useful if
     * you want to understand what is going on. On the other hand, if this tag is
     * set to NO the size of the Perl module output will be much smaller and Perl
     * will parse it just the same.
     * 
     * The default value is: YES.
     * 
     * This tag requires that the tag GENERATE_PERLMOD is set to YES.
     */
    public static final String PERLMOD_PRETTY = "PERLMOD_PRETTY";

    /**
     * The names of the make variables in the generated doxyrules.make file are
     * prefixed with the string contained in PERLMOD_MAKEVAR_PREFIX. This is
     * useful so different doxyrules.make files included by the same Makefile
     * don't overwrite each other's variables.
     * 
     * This tag requires that the tag GENERATE_PERLMOD is set to YES.
     */
    public static final String PERLMOD_MAKEVAR_PREFIX = "PERLMOD_MAKEVAR_PREFIX";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public OutputPerlModSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
