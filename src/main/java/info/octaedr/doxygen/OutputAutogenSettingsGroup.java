package info.octaedr.doxygen;
/**
 * Configuration options for the AutoGen Definitions output
 */
public class OutputAutogenSettingsGroup extends SettingsGroup {

    /**
     * If the GENERATE_AUTOGEN_DEF tag is set to YES doxygen will generate an
     * AutoGen Definitions (see http://autogen.sf.net) file that captures the
     * structure of the code including all documentation. Note that this feature
     * is still experimental and incomplete at the moment.
     * 
     * The default value is: NO.
     */
    public static final String GENERATE_AUTOGEN_DEF = "GENERATE_AUTOGEN_DEF";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public OutputAutogenSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
