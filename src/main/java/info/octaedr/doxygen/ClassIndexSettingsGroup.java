package info.octaedr.doxygen;
/**
 * Configuration options related to the alphabetical class index
 */
public class ClassIndexSettingsGroup extends SettingsGroup {

    /**
     * If the ALPHABETICAL_INDEX tag is set to YES, an alphabetical index of all
     * compounds will be generated. Enable this if the project contains a lot of
     * classes, structs, unions or interfaces.
     * 
     * The default value is: YES.
     */
    public static final String ALPHABETICAL_INDEX = "ALPHABETICAL_INDEX";

    /**
     * The COLS_IN_ALPHA_INDEX tag can be used to specify the number of columns
     * in which the alphabetical index list will be split.
     * 
     * Minimum value: 1, maximum value: 20, default value: 5.
     * 
     * This tag requires that the tag ALPHABETICAL_INDEX is set to YES.
     */
    public static final String COLS_IN_ALPHA_INDEX = "COLS_IN_ALPHA_INDEX";

    /**
     * In case all classes in a project start with a common prefix, all classes
     * will be put under the same header in the alphabetical index. The
     * IGNORE_PREFIX tag can be used to specify a prefix (or a list of prefixes)
     * that should be ignored while generating the index headers.
     * 
     * This tag requires that the tag ALPHABETICAL_INDEX is set to YES.
     */
    public static final String IGNORE_PREFIX = "IGNORE_PREFIX";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public ClassIndexSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
