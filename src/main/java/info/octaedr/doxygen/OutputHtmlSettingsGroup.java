package info.octaedr.doxygen;
/**
 * Configuration options related to the HTML output
 */
public class OutputHtmlSettingsGroup extends SettingsGroup {

    /**
     * If the GENERATE_HTML tag is set to YES doxygen will generate HTML output
     * 
     * The default value is: YES.
     */
    public static final String GENERATE_HTML = "GENERATE_HTML";

    /**
     * The HTML_OUTPUT tag is used to specify where the HTML docs will be put.
     * If a relative path is entered the value of OUTPUT_DIRECTORY will be put
     * in front of it.
     * 
     * The default directory is: html.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_OUTPUT = "HTML_OUTPUT";

    /**
     * The HTML_FILE_EXTENSION tag can be used to specify the file extension for
     * each generated HTML page (for example: .htm, .php, .asp).
     * 
     * The default value is: .html.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_FILE_EXTENSION = "HTML_FILE_EXTENSION";

    /**
     * The HTML_HEADER tag can be used to specify a user-defined HTML header
     * file for each generated HTML page. If the tag is left blank doxygen will
     * generate a standard header. To get valid HTML the header file that
     * includes any scripts and style sheets that doxygen needs, which is
     * dependent on the configuration options used (e.g. the setting
     * GENERATE_TREEVIEW). It is highly recommended to start with a default
     * header using
     * 
     * doxygen -w html new_header.html new_footer.html new_stylesheet.css
     * YourConfigFile and then modify the file new_header.html.
     * 
     * See also section Doxygen usage for information on how to generate the
     * default header that doxygen normally uses.
     * 
     * Note The header is subject to change so you typically have to regenerate
     * the default header when upgrading to a newer version of doxygen. The
     * following markers have a special meaning inside the header and footer:
     * $title will be replaced with the title of the page. $datetime will be
     * replaced with current the date and time. $date will be replaced with the
     * current date. $year will be replaces with the current year.
     * $doxygenversion will be replaced with the version of doxygen $projectname
     * will be replaced with the name of the project (see PROJECT_NAME)
     * $projectnumber will be replaced with the project number (see
     * PROJECT_NUMBER) $projectbrief will be replaced with the project brief
     * description (see PROJECT_BRIEF) $projectlogo will be replaced with the
     * project logo (see PROJECT_LOGO) $treeview will be replaced with links to
     * the javascript and style sheets needed for the navigation tree (or an
     * empty string when GENERATE_TREEVIEW is disabled). $search will be
     * replaced with a links to the javascript and style sheets needed for the
     * search engine (or an empty string when SEARCHENGINE is disabled).
     * $mathjax will be replaced with a links to the javascript and style sheets
     * needed for the MathJax feature (or an empty string when USE_MATHJAX is
     * disabled). $relpath^ If CREATE_SUBDIRS is enabled, the command $relpath^
     * can be used to produce a relative path to the root of the HTML output
     * directory, e.g. use $relpath^doxygen.css, to refer to the standard style
     * sheet. To cope with differences in the layout of the header and footer
     * that depend on configuration settings, the header can also contain
     * special blocks that will be copied to the output or skipped depending on
     * the configuration. Such blocks have the following form:
     * 
     * <!--BEGIN BLOCKNAME--> Some context copied when condition BLOCKNAME holds
     * <!--END BLOCKNAME--> <!--BEGIN !BLOCKNAME--> Some context copied when
     * condition BLOCKNAME does not hold <!--END !BLOCKNAME--> The following
     * block names are supported:
     * 
     * DISABLE_INDEX Content within this block is copied to the output when the
     * DISABLE_INDEX option is enabled (so when the index is disabled).
     * GENERATE_TREEVIEW Content within this block is copied to the output when
     * the GENERATE_TREEVIEW option is enabled. SEARCHENGINE Content within this
     * block is copied to the output when the SEARCHENGINE option is enabled.
     * PROJECT_NAME Content within the block is copied to the output when the
     * PROJECT_NAME option is not empty. PROJECT_NUMBER Content within the block
     * is copied to the output when the PROJECT_NUMBER option is not empty.
     * PROJECT_BRIEF Content within the block is copied to the output when the
     * PROJECT_BRIEF option is not empty. PROJECT_LOGO Content within the block
     * is copied to the output when the PROJECT_LOGO option is not empty.
     * TITLEAREA Content within this block is copied to the output when a title
     * is visible at the top of each page. This is the case if either
     * PROJECT_NAME, PROJECT_BRIEF, PROJECT_LOGO is filled in or if both
     * DISABLE_INDEX and SEARCHENGINE are enabled. This tag requires that the
     * tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_HEADER = "HTML_HEADER";

    /**
     * The HTML_FOOTER tag can be used to specify a user-defined HTML footer for
     * each generated HTML page. If the tag is left blank doxygen will generate
     * a standard footer.
     * 
     * See HTML_HEADER for more information on how to generate a default footer
     * and what special commands can be used inside the footer.
     * 
     * See also section Doxygen usage for information on how to generate the
     * default footer that doxygen normally uses.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_FOOTER = "HTML_FOOTER";

    /**
     * The HTML_STYLESHEET tag can be used to specify a user-defined cascading
     * style sheet that is used by each HTML page. It can be used to fine-tune
     * the look of the HTML output. If left blank doxygen will generate a
     * default style sheet.
     * 
     * See also section Doxygen usage for information on how to generate the
     * style sheet that doxygen normally uses.
     * 
     * Note It is recommended to use HTML_EXTRA_STYLESHEET instead of this tag,
     * as it is more robust and this tag (HTML_STYLESHEET) will in the future
     * become obsolete. This tag requires that the tag GENERATE_HTML is set to
     * YES.
     */
    public static final String HTML_STYLESHEET = "HTML_STYLESHEET";

    /**
     * The HTML_EXTRA_STYLESHEET tag can be used to specify an additional
     * user-defined cascading style sheet that is included after the standard
     * style sheets created by doxygen. Using this option one can overrule
     * certain style aspects. This is preferred over using HTML_STYLESHEET since
     * it does not replace the standard style sheet and is therefor more robust
     * against future updates. Doxygen will copy the style sheet file to the
     * output directory. Here is an example stylesheet that gives the contents
     * area a fixed width:
     * 
     * body { background-color: #CCC; color: black; margin: 0; }
     * 
     * div.contents { margin-bottom: 10px; padding: 12px; margin-left: auto;
     * margin-right: auto; width: 960px; background-color: white; border-radius:
     * 8px; }
     * 
     * #titlearea { background-color: white; }
     * 
     * hr.footer { display: none; }
     * 
     * .footer { background-color: #AAA; } This tag requires that the tag
     * GENERATE_HTML is set to YES.
     */
    public static final String HTML_EXTRA_STYLESHEET = "HTML_EXTRA_STYLESHEET";

    /**
     * The HTML_EXTRA_FILES tag can be used to specify one or more extra images
     * or other source files which should be copied to the HTML output
     * directory. Note that these files will be copied to the base HTML output
     * directory. Use the $relpath^ marker in the HTML_HEADER and/or HTML_FOOTER
     * files to load these files. In the HTML_STYLESHEET file, use the file name
     * only. Also note that the files will be copied as-is; there are no
     * commands or markers available.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_EXTRA_FILES = "HTML_EXTRA_FILES";

    /**
     * The HTML_COLORSTYLE_HUE tag controls the color of the HTML output.
     * Doxygen will adjust the colors in the stylesheet and background images
     * according to this color. Hue is specified as an angle on a colorwheel,
     * see http://en.wikipedia.org/wiki/Hue for more information. For instance
     * the value 0 represents red, 60 is yellow, 120 is green, 180 is cyan, 240
     * is blue, 300 purple, and 360 is red again.
     * 
     * Minimum value: 0, maximum value: 359, default value: 220.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_COLORSTYLE_HUE = "HTML_COLORSTYLE_HUE";

    /**
     * The HTML_COLORSTYLE_SAT tag controls the purity (or saturation) of the
     * colors in the HTML output. For a value of 0 the output will use
     * grayscales only. A value of 255 will produce the most vivid colors.
     * 
     * Minimum value: 0, maximum value: 255, default value: 100.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_COLORSTYLE_SAT = "HTML_COLORSTYLE_SAT";

    /**
     * The HTML_COLORSTYLE_GAMMA tag controls the gamma correction applied to
     * the luminance component of the colors in the HTML output. Values below
     * 100 gradually make the output lighter, whereas values above 100 make the
     * output darker. The value divided by 100 is the actual gamma applied, so
     * 80 represents a gamma of 0.8, The value 220 represents a gamma of 2.2,
     * and 100 does not change the gamma.
     * 
     * Minimum value: 40, maximum value: 240, default value: 80.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_COLORSTYLE_GAMMA = "HTML_COLORSTYLE_GAMMA";

    /**
     * If the HTML_TIMESTAMP tag is set to YES then the footer of each generated
     * HTML page will contain the date and time when the page was generated.
     * Setting this to NO can help when comparing the output of multiple runs.
     * 
     * The default value is: YES.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_TIMESTAMP = "HTML_TIMESTAMP";

    /**
     * If the HTML_DYNAMIC_SECTIONS tag is set to YES then the generated HTML
     * documentation will contain sections that can be hidden and shown after
     * the page has loaded.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_DYNAMIC_SECTIONS = "HTML_DYNAMIC_SECTIONS";

    /**
     * With HTML_INDEX_NUM_ENTRIES one can control the preferred number of
     * entries shown in the various tree structured indices initially; the user
     * can expand and collapse entries dynamically later on. Doxygen will expand
     * the tree to such a level that at most the specified number of entries are
     * visible (unless a fully collapsed tree already exceeds this amount). So
     * setting the number of entries 1 will produce a full collapsed tree by
     * default. 0 is a special value representing an infinite number of entries
     * and will result in a full expanded tree by default.
     * 
     * Minimum value: 0, maximum value: 9999, default value: 100.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String HTML_INDEX_NUM_ENTRIES = "HTML_INDEX_NUM_ENTRIES";

    /**
     * If the GENERATE_DOCSET tag is set to YES, additional index files will be
     * generated that can be used as input for Apple's Xcode 3 integrated
     * development environment, introduced with OSX 10.5 (Leopard). To create a
     * documentation set, doxygen will generate a Makefile in the HTML output
     * directory. Running make will produce the docset in that directory and
     * running make install will install the docset in
     * ~/Library/Developer/Shared/Documentation/DocSets so that Xcode will find
     * it at startup. See
     * http://developer.apple.com/tools/creatingdocsetswithdoxygen.html for more
     * information.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String GENERATE_DOCSET = "GENERATE_DOCSET";

    /**
     * This tag determines the name of the docset feed. A documentation feed
     * provides an umbrella under which multiple documentation sets from a
     * single provider (such as a company or product suite) can be grouped.
     * 
     * The default value is: Doxygen generated docs.
     * 
     * This tag requires that the tag GENERATE_DOCSET is set to YES.
     */
    public static final String DOCSET_FEEDNAME = "DOCSET_FEEDNAME";

    /**
     * This tag specifies a string that should uniquely identify the
     * documentation set bundle. This should be a reverse domain-name style
     * string, e.g. com.mycompany.MyDocSet. Doxygen will append .docset to the
     * name.
     * 
     * The default value is: org.doxygen.Project.
     * 
     * This tag requires that the tag GENERATE_DOCSET is set to YES.
     */
    public static final String DOCSET_BUNDLE_ID = "DOCSET_BUNDLE_ID";

    /**
     * The DOCSET_PUBLISHER_ID tag specifies a string that should uniquely
     * identify the documentation publisher. This should be a reverse
     * domain-name style string, e.g. com.mycompany.MyDocSet.documentation.
     * 
     * The default value is: org.doxygen.Publisher.
     * 
     * This tag requires that the tag GENERATE_DOCSET is set to YES.
     */
    public static final String DOCSET_PUBLISHER_ID = "DOCSET_PUBLISHER_ID";

    /**
     * The DOCSET_PUBLISHER_NAME tag identifies the documentation publisher.
     * 
     * The default value is: Publisher.
     * 
     * This tag requires that the tag GENERATE_DOCSET is set to YES.
     */
    public static final String DOCSET_PUBLISHER_NAME = "DOCSET_PUBLISHER_NAME";

    /**
     * If the GENERATE_HTMLHELP tag is set to YES then doxygen generates three
     * additional HTML index files: index.hhp, index.hhc, and index.hhk. The
     * index.hhp is a project file that can be read by Microsoft's HTML Help
     * Workshop on Windows. The HTML Help Workshop contains a compiler that can
     * convert all HTML output generated by doxygen into a single compiled HTML
     * file (.chm). Compiled HTML files are now used as the Windows 98 help
     * format, and will replace the old Windows help format (.hlp) on all
     * Windows platforms in the future. Compressed HTML files also contain an
     * index, a table of contents, and you can search for words in the
     * documentation. The HTML workshop also contains a viewer for compressed
     * HTML files.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String GENERATE_HTMLHELP = "GENERATE_HTMLHELP";

    /**
     * The CHM_FILE tag can be used to specify the file name of the resulting
     * .chm file. You can add a path in front of the file if the result should
     * not be written to the html output directory.
     * 
     * This tag requires that the tag GENERATE_HTMLHELP is set to YES.
     */
    public static final String CHM_FILE = "CHM_FILE";

    /**
     * The HHC_LOCATION tag can be used to specify the location (absolute path
     * including file name) of the HTML help compiler (hhc.exe). If non-empty
     * doxygen will try to run the HTML help compiler on the generated
     * index.hhp.
     * 
     * The file has to be specified with full path.
     * 
     * This tag requires that the tag GENERATE_HTMLHELP is set to YES.
     */
    public static final String HHC_LOCATION = "HHC_LOCATION";

    /**
     * The GENERATE_CHI flag controls if a separate .chi index file is generated
     * (YES) or that it should be included in the master .chm file (NO).
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTMLHELP is set to YES.
     */
    public static final String GENERATE_CHI = "GENERATE_CHI";

    /**
     * The CHM_INDEX_ENCODING is used to encode HtmlHelp index (hhk), content
     * (hhc) and project file content.
     * 
     * This tag requires that the tag GENERATE_HTMLHELP is set to YES.
     */
    public static final String CHM_INDEX_ENCODING = "CHM_INDEX_ENCODING";

    /**
     * The BINARY_TOC flag controls whether a binary table of contents is
     * generated (YES) or a normal table of contents (NO) in the .chm file.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTMLHELP is set to YES.
     */
    public static final String BINARY_TOC = "BINARY_TOC";

    /**
     * The TOC_EXPAND flag can be set to YES to add extra items for group
     * members to the table of contents of the HTML help documentation and to
     * the tree view.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTMLHELP is set to YES.
     */
    public static final String TOC_EXPAND = "TOC_EXPAND";

    /**
     * If the GENERATE_QHP tag is set to YES and both QHP_NAMESPACE and
     * QHP_VIRTUAL_FOLDER are set, an additional index file will be generated
     * that can be used as input for Qt's qhelpgenerator to generate a Qt
     * Compressed Help (.qch) of the generated HTML documentation.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String GENERATE_QHP = "GENERATE_QHP";

    /**
     * If the QHG_LOCATION tag is specified, the QCH_FILE tag can be used to
     * specify the file name of the resulting .qch file. The path specified is
     * relative to the HTML output folder.
     * 
     * This tag requires that the tag GENERATE_QHP is set to YES.
     */
    public static final String QCH_FILE = "QCH_FILE";

    /**
     * The QHP_NAMESPACE tag specifies the namespace to use when generating Qt
     * Help Project output. For more information please see Qt Help Project /
     * Namespace.
     * 
     * The default value is: org.doxygen.Project.
     * 
     * This tag requires that the tag GENERATE_QHP is set to YES.
     */
    public static final String QHP_NAMESPACE = "QHP_NAMESPACE";

    /**
     * The QHP_VIRTUAL_FOLDER tag specifies the namespace to use when generating
     * Qt Help Project output. For more information please see Qt Help Project /
     * Virtual Folders.
     * 
     * The default value is: doc.
     * 
     * This tag requires that the tag GENERATE_QHP is set to YES.
     */
    public static final String QHP_VIRTUAL_FOLDER = "QHP_VIRTUAL_FOLDER";

    /**
     * If the QHP_CUST_FILTER_NAME tag is set, it specifies the name of a custom
     * filter to add. For more information please see Qt Help Project / Custom
     * Filters.
     * 
     * This tag requires that the tag GENERATE_QHP is set to YES.
     */
    public static final String QHP_CUST_FILTER_NAME = "QHP_CUST_FILTER_NAME";

    /**
     * The QHP_CUST_FILTER_ATTRS tag specifies the list of the attributes of the
     * custom filter to add. For more information please see Qt Help Project /
     * Custom Filters.
     * 
     * This tag requires that the tag GENERATE_QHP is set to YES.
     */
    public static final String QHP_CUST_FILTER_ATTRS = "QHP_CUST_FILTER_ATTRS";

    /**
     * The QHP_SECT_FILTER_ATTRS tag specifies the list of the attributes this
     * project's filter section matches. Qt Help Project / Filter Attributes.
     * 
     * This tag requires that the tag GENERATE_QHP is set to YES.
     */
    public static final String QHP_SECT_FILTER_ATTRS = "QHP_SECT_FILTER_ATTRS";

    /**
     * The QHG_LOCATION tag can be used to specify the location of Qt's
     * qhelpgenerator. If non-empty doxygen will try to run qhelpgenerator on
     * the generated .qhp file.
     * 
     * This tag requires that the tag GENERATE_QHP is set to YES.
     */
    public static final String QHG_LOCATION = "QHG_LOCATION";

    /**
     * If the GENERATE_ECLIPSEHELP tag is set to YES, additional index files
     * will be generated, together with the HTML files, they form an Eclipse
     * help plugin.
     * 
     * To install this plugin and make it available under the help contents menu
     * in Eclipse, the contents of the directory containing the HTML and XML
     * files needs to be copied into the plugins directory of eclipse. The name
     * of the directory within the plugins directory should be the same as the
     * ECLIPSE_DOC_ID value.
     * 
     * After copying Eclipse needs to be restarted before the help appears.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String GENERATE_ECLIPSEHELP = "GENERATE_ECLIPSEHELP";

    /**
     * A unique identifier for the Eclipse help plugin. When installing the
     * plugin the directory name containing the HTML and XML files should also
     * have this name. Each documentation set should have its own identifier.
     * 
     * The default value is: org.doxygen.Project.
     * 
     * This tag requires that the tag GENERATE_ECLIPSEHELP is set to YES.
     */
    public static final String ECLIPSE_DOC_ID = "ECLIPSE_DOC_ID";

    /**
     * If you want full control over the layout of the generated HTML pages it
     * might be necessary to disable the index and replace it with your own. The
     * DISABLE_INDEX tag can be used to turn on/off the condensed index (tabs)
     * at top of each HTML page. A value of NO enables the index and the value
     * YES disables it. Since the tabs in the index contain the same information
     * as the navigation tree, you can set this option to YES if you also set
     * GENERATE_TREEVIEW to YES.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String DISABLE_INDEX = "DISABLE_INDEX";

    /**
     * The GENERATE_TREEVIEW tag is used to specify whether a tree-like index
     * structure should be generated to display hierarchical information. If the
     * tag value is set to YES, a side panel will be generated containing a
     * tree-like index structure (just like the one that is generated for HTML
     * Help). For this to work a browser that supports JavaScript, DHTML, CSS
     * and frames is required (i.e. any modern browser). Windows users are
     * probably better off using the HTML help feature.
     * 
     * Via custom stylesheets (see HTML_EXTRA_STYLESHEET) one can further
     * fine-tune the look of the index. As an example, the default style sheet
     * generated by doxygen has an example that shows how to put an image at the
     * root of the tree instead of the PROJECT_NAME.
     * 
     * Since the tree basically has the same information as the tab index, you
     * could consider setting DISABLE_INDEX to YES when enabling this option.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String GENERATE_TREEVIEW = "GENERATE_TREEVIEW";

    /**
     * The ENUM_VALUES_PER_LINE tag can be used to set the number of enum values
     * that doxygen will group on one line in the generated HTML documentation.
     * Note that a value of 0 will completely suppress the enum values from
     * appearing in the overview section.
     * 
     * Minimum value: 0, maximum value: 20, default value: 4.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String ENUM_VALUES_PER_LINE = "ENUM_VALUES_PER_LINE";

    /**
     * If the treeview is enabled (see GENERATE_TREEVIEW) then this tag can be
     * used to set the initial width (in pixels) of the frame in which the tree
     * is shown.
     * 
     * Minimum value: 0, maximum value: 1500, default value: 250.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String TREEVIEW_WIDTH = "TREEVIEW_WIDTH";

    /**
     * When the EXT_LINKS_IN_WINDOW option is set to YES doxygen will open links
     * to external symbols imported via tag files in a separate window.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String EXT_LINKS_IN_WINDOW = "EXT_LINKS_IN_WINDOW";

    /**
     * Use this tag to change the font size of $\mbox{\LaTeX}$ formulas included
     * as images in the HTML documentation. When you change the font size after
     * a successful doxygen run you need to manually remove any form_*.png
     * images from the HTML output directory to force them to be regenerated.
     * 
     * Minimum value: 8, maximum value: 50, default value: 10.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String FORMULA_FONTSIZE = "FORMULA_FONTSIZE";

    /**
     * Use the FORMULA_TRANPARENT tag to determine whether or not the images
     * generated for formulas are transparent PNGs. Transparent PNGs are not
     * supported properly for IE 6.0, but are supported on all modern browsers.
     * Note that when changing this option you need to delete any form_*.png
     * files in the HTML output directory before the changes have effect.
     * 
     * The default value is: YES.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String FORMULA_TRANSPARENT = "FORMULA_TRANSPARENT";

    /**
     * Enable the USE_MATHJAX option to render $\mbox{\LaTeX}$ formulas using
     * MathJax (see http://www.mathjax.org) which uses client side Javascript
     * for the rendering instead of using prerendered bitmaps. Use this if you
     * do not have $\mbox{\LaTeX}$ installed or if you want to formulas look
     * prettier in the HTML output. When enabled you may also need to install
     * MathJax separately and configure the path to it using the MATHJAX_RELPATH
     * option.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String USE_MATHJAX = "USE_MATHJAX";

    /**
     * When MathJax is enabled you can set the default output format to be used
     * for the MathJax output. See the MathJax site for more details.
     * 
     * Possible values are: HTML-CSS (which is slower, but has the best
     * compatibility), NativeMML (i.e. MathML) and SVG.
     * 
     * The default value is: HTML-CSS.
     * 
     * This tag requires that the tag USE_MATHJAX is set to YES.
     */
    public static final String MATHJAX_FORMAT = "MATHJAX_FORMAT";

    /**
     * When MathJax is enabled you need to specify the location relative to the
     * HTML output directory using the MATHJAX_RELPATH option. The destination
     * directory should contain the MathJax.js script. For instance, if the
     * mathjax directory is located at the same level as the HTML output
     * directory, then MATHJAX_RELPATH should be ../mathjax. The default value
     * points to the MathJax Content Delivery Network so you can quickly see the
     * result without installing MathJax. However, it is strongly recommended to
     * install a local copy of MathJax from http://www.mathjax.org before
     * deployment.
     * 
     * The default value is: http://cdn.mathjax.org/mathjax/latest.
     * 
     * This tag requires that the tag USE_MATHJAX is set to YES.
     */
    public static final String MATHJAX_RELPATH = "MATHJAX_RELPATH";

    /**
     * The MATHJAX_EXTENSIONS tag can be used to specify one or more MathJax
     * extension names that should be enabled during MathJax rendering. For
     * example
     * 
     * MATHJAX_EXTENSIONS = TeX/AMSmath TeX/AMSsymbols
     * 
     * This tag requires that the tag USE_MATHJAX is set to YES.
     */
    public static final String MATHJAX_EXTENSIONS = "MATHJAX_EXTENSIONS";

    /**
     * The MATHJAX_CODEFILE tag can be used to specify a file with javascript
     * pieces of code that will be used on startup of the MathJax code. See the
     * MathJax site for more details. As an example to disable the
     * "Math Renderer" menu item in the "Math Settings" menu of MathJax:
     * 
     * MATHJAX_CODEFILE = disableRenderer.js with in the file
     * disableRenderer.js:
     * 
     * MathJax.Hub.Config({ menuSettings: { showRenderer: false, } }); This tag
     * requires that the tag USE_MATHJAX is set to YES.
     */
    public static final String MATHJAX_CODEFILE = "MATHJAX_CODEFILE";

    /**
     * When the SEARCHENGINE tag is enabled doxygen will generate a search box
     * for the HTML output. The underlying search engine uses javascript and
     * DHTML and should work on any modern browser. Note that when using HTML
     * help (GENERATE_HTMLHELP), Qt help (GENERATE_QHP), or docsets
     * (GENERATE_DOCSET) there is already a search function so this one should
     * typically be disabled. For large projects the javascript based search
     * engine can be slow, then enabling SERVER_BASED_SEARCH may provide a
     * better solution.
     * 
     * It is possible to search using the keyboard; to jump to the search box
     * use <access key> + S (what the <access key> is depends on the OS and
     * browser, but it is typically <CTRL>, <ALT>/<option>, or both). Inside the
     * search box use the <cursor down key> to jump into the search results
     * window, the results can be navigated using the <cursor keys>. Press
     * <Enter> to select an item or <escape> to cancel the search. The filter
     * options can be selected when the cursor is inside the search box by
     * pressing <Shift>+<cursor down>. Also here use the <cursor keys> to select
     * a filter and <Enter> or <escape> to activate or cancel the filter option.
     * 
     * The default value is: YES.
     * 
     * This tag requires that the tag GENERATE_HTML is set to YES.
     */
    public static final String SEARCHENGINE = "SEARCHENGINE";

    /**
     * When the SERVER_BASED_SEARCH tag is enabled the search engine will be
     * implemented using a web server instead of a web client using Javascript.
     * 
     * There are two flavours of web server based searching depending on the
     * EXTERNAL_SEARCH setting. When disabled, doxygen will generate a PHP
     * script for searching and an index file used by the script. When
     * EXTERNAL_SEARCH is enabled the indexing and searching needs to be
     * provided by external tools. See the section External Indexing and
     * Searching for details.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag SEARCHENGINE is set to YES.
     */
    public static final String SERVER_BASED_SEARCH = "SERVER_BASED_SEARCH";

    /**
     * When EXTERNAL_SEARCH tag is enabled doxygen will no longer generate the
     * PHP script for searching. Instead the search results are written to an
     * XML file which needs to be processed by an external indexer. Doxygen will
     * invoke an external search engine pointed to by the SEARCHENGINE_URL
     * option to obtain the search results. Doxygen ships with an example
     * indexer (doxyindexer) and search engine (doxysearch.cgi) which are based
     * on the open source search engine library Xapian. See the section External
     * Indexing and Searching for details.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag SEARCHENGINE is set to YES.
     */
    public static final String EXTERNAL_SEARCH = "EXTERNAL_SEARCH";

    /**
     * The SEARCHENGINE_URL should point to a search engine hosted by a web
     * server which will return the search results when EXTERNAL_SEARCH is
     * enabled. Doxygen ships with an example indexer (doxyindexer) and search
     * engine (doxysearch.cgi) which are based on the open source search engine
     * library Xapian. See the section External Indexing and Searching for
     * details.
     * 
     * This tag requires that the tag SEARCHENGINE is set to YES.
     */
    public static final String SEARCHENGINE_URL = "SEARCHENGINE_URL";

    /**
     * When SERVER_BASED_SEARCH and EXTERNAL_SEARCH are both enabled the
     * unindexed search data is written to a file for indexing by an external
     * tool. With the SEARCHDATA_FILE tag the name of this file can be
     * specified.
     * 
     * The default file is: searchdata.xml.
     * 
     * This tag requires that the tag SEARCHENGINE is set to YES.
     */
    public static final String SEARCHDATA_FILE = "SEARCHDATA_FILE";

    /**
     * When SERVER_BASED_SEARCH and EXTERNAL_SEARCH are both enabled the
     * EXTERNAL_SEARCH_ID tag can be used as an identifier for the project. This
     * is useful in combination with EXTRA_SEARCH_MAPPINGS to search through
     * multiple projects and redirect the results back to the right project.
     * 
     * This tag requires that the tag SEARCHENGINE is set to YES.
     */
    public static final String EXTERNAL_SEARCH_ID = "EXTERNAL_SEARCH_ID";

    /**
     * The EXTRA_SEARCH_MAPPINGS tag can be used to enable searching through
     * doxygen projects other than the one defined by this configuration file,
     * but that are all added to the same external search index. Each project
     * needs to have a unique id set via EXTERNAL_SEARCH_ID. The search mapping
     * then maps the id of to a relative location where the documentation can be
     * found.
     * 
     * The format is:
     * 
     * EXTRA_SEARCH_MAPPINGS = tagname1=loc1 tagname2=loc2 ...
     * 
     * This tag requires that the tag SEARCHENGINE is set to YES.
     */
    public static final String EXTRA_SEARCH_MAPPINGS = "EXTRA_SEARCH_MAPPINGS";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public OutputHtmlSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
