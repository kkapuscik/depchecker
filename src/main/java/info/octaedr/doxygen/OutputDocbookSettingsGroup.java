package info.octaedr.doxygen;
/** 
 * Configuration options related to the DOCBOOK output. 
 */
public class OutputDocbookSettingsGroup extends SettingsGroup {

    /**
     * If the GENERATE_DOCBOOK tag is set to YES doxygen will generate Docbook
     * files that can be used to generate PDF.
     * 
     * The default value is: NO.
     */
    public static final String GENERATE_DOCBOOK = "GENERATE_DOCBOOK";

    /**
     * The DOCBOOK_OUTPUT tag is used to specify where the Docbook pages will be
     * put. If a relative path is entered the value of OUTPUT_DIRECTORY will be
     * put in front of it.
     * 
     * The default directory is: docbook.
     * 
     * This tag requires that the tag GENERATE_DOCBOOK is set to YES.
     */
    public static final String DOCBOOK_OUTPUT = "DOCBOOK_OUTPUT";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public OutputDocbookSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
