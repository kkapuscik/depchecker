package info.octaedr.doxygen;
/**
 * Configuration options related to the XML output
 */
public class OutputXmlSettingsGroup extends SettingsGroup {

    /**
     * If the GENERATE_XML tag is set to YES doxygen will generate an XML file
     * that captures the structure of the code including all documentation.
     * 
     * The default value is: NO.
     */
    public static final String GENERATE_XML = "GENERATE_XML";

    /**
     * The XML_OUTPUT tag is used to specify where the XML pages will be put. If
     * a relative path is entered the value of OUTPUT_DIRECTORY will be put in
     * front of it.
     * 
     * The default directory is: xml.
     * 
     * This tag requires that the tag GENERATE_XML is set to YES.
     */
    public static final String XML_OUTPUT = "XML_OUTPUT";

    /**
     * The XML_SCHEMA tag can be used to specify a XML schema, which can be used
     * by a validating XML parser to check the syntax of the XML files.
     * 
     * This tag requires that the tag GENERATE_XML is set to YES.
     */
    public static final String XML_SCHEMA = "XML_SCHEMA";
    
    /**
     * The XML_DTD tag can be used to specify a XML DTD, which can be used by a
     * validating XML parser to check the syntax of the XML files.
     * 
     * This tag requires that the tag GENERATE_XML is set to YES.
     */
    public static final String XML_DTD = "XML_DTD";
    
    /**
     * If the XML_PROGRAMLISTING tag is set to YES doxygen will dump the program
     * listings (including syntax highlighting and cross-referencing
     * information) to the XML output. Note that enabling this will significantly
     * increase the size of the XML output.
     * 
     * The default value is: YES.
     * 
     * This tag requires that the tag GENERATE_XML is set to YES.
     */
    public static final String XML_PROGRAMLISTING = "XML_PROGRAMLISTING";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public OutputXmlSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
