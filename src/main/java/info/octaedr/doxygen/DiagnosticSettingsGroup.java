package info.octaedr.doxygen;
/**
 * Configuration options related to warning and progress messages.
 */
public class DiagnosticSettingsGroup extends SettingsGroup {

    /**
     * The QUIET tag can be used to turn on/off the messages that are generated
     * to standard output by doxygen. If QUIET is set to YES this implies that
     * the messages are off.
     * 
     * The default value is: NO.
     */
    public static final String QUIET = "QUIET";

    /**
     * The WARNINGS tag can be used to turn on/off the warning messages that are
     * generated to standard error (stderr) by doxygen. If WARNINGS is set to
     * YES this implies that the warnings are on. Tip: Turn warnings on while
     * writing the documentation.
     * 
     * The default value is: YES.
     */
    public static final String WARNINGS = "WARNINGS";

    /**
     * If the WARN_IF_UNDOCUMENTED tag is set to YES, then doxygen will generate
     * warnings for undocumented members. If EXTRACT_ALL is set to YES then this
     * flag will automatically be disabled.
     * 
     * The default value is: YES.
     */
    public static final String WARN_IF_UNDOCUMENTED = "WARN_IF_UNDOCUMENTED";

    /**
     * If the WARN_IF_DOC_ERROR tag is set to YES, doxygen will generate
     * warnings for potential errors in the documentation, such as not
     * documenting some parameters in a documented function, or documenting
     * parameters that don't exist or using markup commands wrongly.
     * 
     * The default value is: YES.
     */
    public static final String WARN_IF_DOC_ERROR = "WARN_IF_DOC_ERROR";

    /**
     * This WARN_NO_PARAMDOC option can be enabled to get warnings for functions
     * that are documented, but have no documentation for their parameters or
     * return value. If set to NO doxygen will only warn about wrong or
     * incomplete parameter documentation, but not about the absence of
     * documentation.
     * 
     * The default value is: NO.
     */
    public static final String WARN_NO_PARAMDOC = "WARN_NO_PARAMDOC";

    /**
     * The WARN_FORMAT tag determines the format of the warning messages that
     * doxygen can produce. The string should contain the $file, $line, and
     * $text tags, which will be replaced by the file and line number from which
     * the warning originated and the warning text. Optionally the format may
     * contain $version, which will be replaced by the version of the file (if
     * it could be obtained via FILE_VERSION_FILTER)
     * 
     * The default value is: $file:$line: $text.
     */
    public static final String WARN_FORMAT = "WARN_FORMAT";

    /**
     * The WARN_LOGFILE tag can be used to specify a file to which warning and
     * error messages should be written. If left blank the output is written to
     * standard error (stderr).
     */
    public static final String WARN_LOGFILE = "WARN_LOGFILE";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public DiagnosticSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
