package info.octaedr.doxygen;

import java.io.File;

/**
 * Configuration options related to the input files
 */
public class InputSettingsGroup extends SettingsGroup {

    /**
     * The INPUT tag is used to specify the files and/or directories that
     * contain documented source files. You may enter file names like myfile.cpp
     * or directories like /usr/src/myproject. Separate the files or directories
     * with spaces.
     * 
     * Note If this tag is empty the current directory is searched.
     */
    public static final String INPUT = "INPUT";

    /**
     * This tag can be used to specify the character encoding of the source
     * files that doxygen parses. Internally doxygen uses the UTF-8 encoding.
     * Doxygen uses libiconv (or the iconv built into libc) for the transcoding.
     * See the libiconv documentation for the list of possible encodings.
     * 
     * The default value is: UTF-8.
     */
    public static final String INPUT_ENCODING = "INPUT_ENCODING";

    /**
     * If the value of the INPUT tag contains directories, you can use the
     * FILE_PATTERNS tag to specify one or more wildcard patterns (like *.cpp
     * and *.h) to filter out the source-files in the directories. If left blank
     * the following patterns are tested: *.c, *.cc, *.cxx, *.cpp, *.c++,
     * *.java, *.ii, *.ixx, *.ipp, *.i++, *.inl, *.idl, *.ddl, *.odl, *.h, *.hh,
     * *.hxx, *.hpp, *.h++, *.cs, *.d, *.php, *.php4, *.php5, *.phtml, *.inc,
     * *.m, *.markdown, *.md, *.mm, *.dox, *.py, *.f90, *.f, *.for, *.tcl,
     * *.vhd, *.vhdl, *.ucf, *.qsf, *.as and *.js.
     */
    public static final String FILE_PATTERNS = "FILE_PATTERNS";

    /**
     * The RECURSIVE tag can be used to specify whether or not subdirectories
     * should be searched for input files as well.
     * 
     * The default value is: NO.
     */
    public static final String RECURSIVE = "RECURSIVE";

    /**
     * The EXCLUDE tag can be used to specify files and/or directories that
     * should be excluded from the INPUT source files. This way you can easily
     * exclude a subdirectory from a directory tree whose root is specified with
     * the INPUT tag.
     * 
     * Note that relative paths are relative to the directory from which doxygen
     * is run.
     */
    public static final String EXCLUDE = "EXCLUDE";

    /**
     * The EXCLUDE_SYMLINKS tag can be used to select whether or not files or
     * directories that are symbolic links (a Unix file system feature) are
     * excluded from the input.
     * 
     * The default value is: NO.
     */
    public static final String EXCLUDE_SYMLINKS = "EXCLUDE_SYMLINKS";

    /**
     * If the value of the INPUT tag contains directories, you can use the
     * EXCLUDE_PATTERNS tag to specify one or more wildcard patterns to exclude
     * certain files from those directories.
     * 
     * Note that the wildcards are matched against the file with absolute path,
     * so to exclude all test directories for example use the pattern * / test /
     * *
     */
    public static final String EXCLUDE_PATTERNS = "EXCLUDE_PATTERNS";

    /**
     * The EXCLUDE_SYMBOLS tag can be used to specify one or more symbol names
     * (namespaces, classes, functions, etc.) that should be excluded from the
     * output. The symbol name can be a fully qualified name, a word, or if the
     * wildcard * is used, a substring. Examples: ANamespace, AClass,
     * AClass::ANamespace, ANamespace::*Test
     * 
     * Note that the wildcards are matched against the file with absolute path,
     * so to exclude all test directories use the pattern * / test / *
     */
    public static final String EXCLUDE_SYMBOLS = "EXCLUDE_SYMBOLS";

    /**
     * The EXAMPLE_PATH tag can be used to specify one or more files or
     * directories that contain example code fragments that are included (see
     * the \include command).
     */
    public static final String EXAMPLE_PATH = "EXAMPLE_PATH";

    /**
     * If the value of the EXAMPLE_PATH tag contains directories, you can use
     * the EXAMPLE_PATTERNS tag to specify one or more wildcard pattern (like
     * *.cpp and *.h) to filter out the source-files in the directories. If left
     * blank all files are included.
     */
    public static final String EXAMPLE_PATTERNS = "EXAMPLE_PATTERNS";

    /**
     * If the EXAMPLE_RECURSIVE tag is set to YES then subdirectories will be
     * searched for input files to be used with the \include or \dontinclude
     * commands irrespective of the value of the RECURSIVE tag.
     * 
     * The default value is: NO.
     */
    public static final String EXAMPLE_RECURSIVE = "EXAMPLE_RECURSIVE";

    /**
     * The IMAGE_PATH tag can be used to specify one or more files or
     * directories that contain images that are to be included in the
     * documentation (see the \image command).
     */
    public static final String IMAGE_PATH = "IMAGE_PATH";

    /**
     * The INPUT_FILTER tag can be used to specify a program that doxygen should
     * invoke to filter for each input file. Doxygen will invoke the filter
     * program by executing (via popen()) the command: <filter> <input-file>
     * where <filter> is the value of the INPUT_FILTER tag, and <input-file> is
     * the name of an input file. Doxygen will then use the output that the
     * filter program writes to standard output. If FILTER_PATTERNS is
     * specified, this tag will be ignored.
     * 
     * Note that the filter must not add or remove lines; it is applied before
     * the code is scanned, but not when the output code is generated. If lines
     * are added or removed, the anchors will not be placed correctly.
     */
    public static final String INPUT_FILTER = "INPUT_FILTER";

    /**
     * The FILTER_PATTERNS tag can be used to specify filters on a per file
     * pattern basis. Doxygen will compare the file name with each pattern and
     * apply the filter if there is a match. The filters are a list of the form:
     * pattern=filter (like *.cpp=my_cpp_filter). See INPUT_FILTER for further
     * information on how filters are used. If the FILTER_PATTERNS tag is empty
     * or if none of the patterns match the file name, INPUT_FILTER is applied.
     */
    public static final String FILTER_PATTERNS = "FILTER_PATTERNS";

    /**
     * If the FILTER_SOURCE_FILES tag is set to YES, the input filter (if set
     * using INPUT_FILTER ) will also be used to filter the input files that are
     * used for producing the source files to browse (i.e. when SOURCE_BROWSER
     * is set to YES).
     * 
     * The default value is: NO.
     */
    public static final String FILTER_SOURCE_FILES = "FILTER_SOURCE_FILES";

    /**
     * The FILTER_SOURCE_PATTERNS tag can be used to specify source filters per
     * file pattern. A pattern will override the setting for FILTER_PATTERN (if
     * any) and it is also possible to disable source filtering for a specific
     * pattern using *.ext= (so without naming a filter).
     * 
     * This tag requires that the tag FILTER_SOURCE_FILES is set to YES.
     */
    public static final String FILTER_SOURCE_PATTERNS = "FILTER_SOURCE_PATTERNS";

    /**
     * If the USE_MDFILE_AS_MAINPAGE tag refers to the name of a markdown file
     * that is part of the input, its contents will be placed on the main page
     * (index.html). This can be useful if you have a project on for instance
     * GitHub and want to reuse the introduction page also for the doxygen
     * output.
     */
    public static final String USE_MDFILE_AS_MAINPAGE = "USE_MDFILE_AS_MAINPAGE";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public InputSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

    /**
     * Sets value of the {@link #INPUT} setting.
     * 
     * @param paths
     *            Collection of paths.
     */
    public void setInput(final File[] paths) {
        set(INPUT, pathsToValue(paths));
    }

}
