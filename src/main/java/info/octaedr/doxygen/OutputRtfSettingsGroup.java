package info.octaedr.doxygen;
/**
 * Configuration options related to the RTF output
 */
public class OutputRtfSettingsGroup extends SettingsGroup {

    /**
     * If the GENERATE_RTF tag is set to YES doxygen will generate RTF output.
     * The RTF output is optimized for Word 97 and may not look too pretty with
     * other RTF readers/editors.
     * 
     * The default value is: NO.
     */
    public static final String GENERATE_RTF = "GENERATE_RTF";

    /**
     * The RTF_OUTPUT tag is used to specify where the RTF docs will be put. If
     * a relative path is entered the value of OUTPUT_DIRECTORY will be put in
     * front of it.
     * 
     * The default directory is: rtf.
     * 
     * This tag requires that the tag GENERATE_RTF is set to YES.
     */
    public static final String RTF_OUTPUT = "RTF_OUTPUT";

    /**
     * If the COMPACT_RTF tag is set to YES doxygen generates more compact RTF
     * documents. This may be useful for small projects and may help to save
     * some trees in general.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_RTF is set to YES.
     */
    public static final String COMPACT_RTF = "COMPACT_RTF";

    /**
     * If the RTF_HYPERLINKS tag is set to YES, the RTF that is generated will
     * contain hyperlink fields. The RTF file will contain links (just like the
     * HTML output) instead of page references. This makes the output suitable
     * for online browsing using Word or some other Word compatible readers that
     * support those fields.
     * 
     * Note: WordPad (write) and others do not support links.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag GENERATE_RTF is set to YES.
     */
    public static final String RTF_HYPERLINKS = "RTF_HYPERLINKS";

    /**
     * Load stylesheet definitions from file. Syntax is similar to doxygen's
     * config file, i.e. a series of assignments. You only have to provide
     * replacements, missing definitions are set to their default value. See
     * also section Doxygen usage for information on how to generate the default
     * style sheet that doxygen normally uses.
     * 
     * This tag requires that the tag GENERATE_RTF is set to YES.
     */
    public static final String RTF_STYLESHEET_FILE = "RTF_STYLESHEET_FILE";

    /**
     * Set optional variables used in the generation of an RTF document. Syntax
     * is similar to doxygen's config file. A template extensions file can be
     * generated using doxygen -e rtf extensionFile.
     * 
     * This tag requires that the tag GENERATE_RTF is set to YES.
     */
    public static final String RTF_EXTENSIONS_FILE = "RTF_EXTENSIONS_FILE";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public OutputRtfSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

}
