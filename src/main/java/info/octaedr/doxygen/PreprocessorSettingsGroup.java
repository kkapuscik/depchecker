package info.octaedr.doxygen;

import java.io.File;

/**
 * Configuration options related to the preprocessor
 */
public class PreprocessorSettingsGroup extends SettingsGroup {

    /**
     * If the ENABLE_PREPROCESSING tag is set to YES doxygen will evaluate all
     * C-preprocessor directives found in the sources and include files.
     * 
     * The default value is: YES.
     */
    public static final String ENABLE_PREPROCESSING = "ENABLE_PREPROCESSING";

    /**
     * If the MACRO_EXPANSION tag is set to YES doxygen will expand all macro
     * names in the source code. If set to NO only conditional compilation will
     * be performed. Macro expansion can be done in a controlled way by setting
     * EXPAND_ONLY_PREDEF to YES.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag ENABLE_PREPROCESSING is set to YES.
     */
    public static final String MACRO_EXPANSION = "MACRO_EXPANSION";

    /**
     * If the EXPAND_ONLY_PREDEF and MACRO_EXPANSION tags are both set to YES
     * then the macro expansion is limited to the macros specified with the
     * PREDEFINED and EXPAND_AS_DEFINED tags.
     * 
     * The default value is: NO.
     * 
     * This tag requires that the tag ENABLE_PREPROCESSING is set to YES.
     */
    public static final String EXPAND_ONLY_PREDEF = "EXPAND_ONLY_PREDEF";

    /**
     * If the SEARCH_INCLUDES tag is set to YES the includes files in the
     * INCLUDE_PATH will be searched if a #include is found.
     * 
     * The default value is: YES.
     * 
     * This tag requires that the tag ENABLE_PREPROCESSING is set to YES.
     */
    public static final String SEARCH_INCLUDES = "SEARCH_INCLUDES";

    /**
     * The INCLUDE_PATH tag can be used to specify one or more directories that
     * contain include files that are not input files but should be processed by
     * the preprocessor.
     * 
     * This tag requires that the tag SEARCH_INCLUDES is set to YES.
     */
    public static final String INCLUDE_PATH = "INCLUDE_PATH";

    /**
     * You can use the INCLUDE_FILE_PATTERNS tag to specify one or more wildcard
     * patterns (like *.h and *.hpp) to filter out the header-files in the
     * directories. If left blank, the patterns specified with FILE_PATTERNS
     * will be used.
     * 
     * This tag requires that the tag ENABLE_PREPROCESSING is set to YES.
     */
    public static final String INCLUDE_FILE_PATTERNS = "INCLUDE_FILE_PATTERNS";

    /**
     * The PREDEFINED tag can be used to specify one or more macro names that
     * are defined before the preprocessor is started (similar to the -D option
     * of e.g. gcc). The argument of the tag is a list of macros of the form:
     * name or name=definition (no spaces). If the definition and the "=" are
     * omitted, "=1" is assumed. To prevent a macro definition from being
     * undefined via #undef or recursively expanded use the := operator instead
     * of the = operator.
     * 
     * This tag requires that the tag ENABLE_PREPROCESSING is set to YES.
     */
    public static final String PREDEFINED = "PREDEFINED";

    /**
     * 
     If the MACRO_EXPANSION and EXPAND_ONLY_PREDEF tags are set to YES then
     * this tag can be used to specify a list of macro names that should be
     * expanded. The macro definition that is found in the sources will be used.
     * Use the PREDEFINED tag if you want to use a different macro definition
     * that overrules the definition found in the source code.
     * 
     * This tag requires that the tag ENABLE_PREPROCESSING is set to YES.
     */
    public static final String EXPAND_AS_DEFINED = "EXPAND_AS_DEFINED";

    /**
     * If the SKIP_FUNCTION_MACROS tag is set to YES then doxygen's preprocessor
     * will remove all refrences to function-like macros that are alone on a
     * line, have an all uppercase name, and do not end with a semicolon. Such
     * function macros are typically used for boiler-plate code, and will confuse
     * the parser if not removed.
     * 
     * The default value is: YES.
     * 
     * This tag requires that the tag ENABLE_PREPROCESSING is set to YES.
     */
    public static final String SKIP_FUNCTION_MACROS = "SKIP_FUNCTION_MACROS";

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public PreprocessorSettingsGroup(final Settings doxygenSettings) {
        super(doxygenSettings);
    }

    /**
     * Sets value of the {@link #INCLUDE_PATH} setting.
     * 
     * @param paths
     *            Collection of paths.
     */
    public void setSearchIncludes(File[] paths) {
        set(INCLUDE_PATH, pathsToValue(paths));
    }

}
