package info.octaedr.doxygen;

import java.io.File;

/**
 * Base class for classes grouping Doxygen tool settings.
 */
public abstract class SettingsGroup {

    /**
     * The Doxygen settings object for which this group was created.
     */
    private final Settings doxygenSettings;

    /**
     * Constructs settings group.
     * 
     * @param doxygenSettings
     *            Doxygen settings object for which this group iscreated.
     */
    public SettingsGroup(final Settings doxygenSettings) {
        assert doxygenSettings != null;

        this.doxygenSettings = doxygenSettings;
    }

    /**
     * Checks if value is defined for given key.
     * 
     * @param key
     *            Key of the setting.
     * 
     * @return True if value is defined, false otherwise.
     */
    public boolean isSet(final String key) {
        return this.doxygenSettings.isSet(key);
    }

    /**
     * Returns value for given key.
     * 
     * @param key
     *            Key of the setting.
     * 
     * @return Value currently set for given key or null if there is no value
     *         defined.
     */
    public String get(final String key) {
        return this.doxygenSettings.get(key);
    }

    /**
     * Sets value for given key.
     * 
     * @param key
     *            Key of the setting.
     * @param value
     *            Value to set. If null any value currently set for the key will
     *            be reset.
     */
    public void set(final String key, final String value) {
        this.doxygenSettings.set(key, value);
    }
    
    /**
     * Converts collection of paths to setting value.
     * 
     * @param paths
     *      Collection of paths to be converted.
     *      
     * @return
     * Converted value. Null if given collection was null.
     */
    protected String pathsToValue(File[] paths) {
        if (paths != null) {
            final StringBuilder valueBuffer = new StringBuilder();
            for (File path : paths) {
                valueBuffer.append(" \\\n\t\"");
                valueBuffer.append(path.getAbsolutePath());
                valueBuffer.append('"');
            }
            return valueBuffer.toString();
        } else {
            return null;
        }
    }

}
