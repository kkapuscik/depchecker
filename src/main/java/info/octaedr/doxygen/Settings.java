package info.octaedr.doxygen;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Settings for Doxygen utility.
 */
public class Settings {

    /**
     * Value "NO" for boolean settings.
     */
    public static final String VALUE_YES = "YES";

    /**
     * Value "NO" for boolean settings.
     */
    public static final String VALUE_NO = "NO";

    /**
     * Collection of defined settings.
     */
    private final Map<String, String> settingsMap;

    /**
     * Constructs empty settings.
     */
    public Settings() {
        this.settingsMap = new HashMap<>();
    }

    /**
     * Checks if value is defined for given key.
     * 
     * @param key
     *            Key of the setting.
     * 
     * @return True if value is defined, false otherwise.
     */
    public boolean isSet(final String key) {
        return this.settingsMap.containsKey(key);
    }

    /**
     * Returns value for given key.
     * 
     * @param key
     *            Key of the setting.
     * 
     * @return Value currently set for given key or null if there is no value
     *         defined.
     */
    public String get(final String key) {
        return this.settingsMap.get(key);
    }

    /**
     * Sets value for given key.
     * 
     * @param key
     *            Key of the setting.
     * @param value
     *            Value to set. If null any value currently set for the key will
     *            be reset.
     */
    public void set(final String key, final String value) {
        assert key != null;

        if (value != null) {
            this.settingsMap.put(key, value);
        } else {
            this.settingsMap.remove(key);
        }
    }
    
    /**
     * Returns all keys for which there are values currently defined.
     * 
     * @return
     * Collection of keys of defined settings.
     */
    public String[] getKeys() {
        final Set<String> keys = this.settingsMap.keySet();
        return keys.toArray(new String[keys.size()]);
    }

}
