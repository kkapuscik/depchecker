package info.octaedr.cmd;

import info.octaedr.depchecker.model.LogWriter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Thread that reads all data from input reader.
 */
class InputReaderThread extends Thread {

    /**
     * Reader for the data.
     */
    private final BufferedReader reader;

    /**
     * The lines read.
     */
    private final ArrayList<String> lines = new ArrayList<>();

    /**
     * Result flag.
     * 
     * <p>
     * True for success, false otherwise.
     */
    private boolean result = false;

    /**
     * Flag indicating if the reader is for error data (true) or standard output
     * (false).
     */
    private final boolean isErrorReader;

    /**
     * Writer object for printing the information.
     */
    private final LogWriter logWriter;

    /**
     * Constructs the reader thread.
     * 
     * @param inputStream
     *            Input stream from which data shall be read.
     * @param isErrorReader
     *            Flag indicating if the reader is for error data (true) or
     *            standard output (false).
     * @param logWriter
     *            Writer object for printing the information.
     */
    public InputReaderThread(final InputStream inputStream, final boolean isErrorReader,
            final LogWriter logWriter) {
        assert inputStream != null;
        assert logWriter != null;

        this.reader = new BufferedReader(new InputStreamReader(inputStream));
        this.isErrorReader = isErrorReader;
        this.logWriter = logWriter;
    }

    /**
     * Returns execution result.
     * 
     * @return True if all data was successfully read, false otherwise.
     */
    public boolean getResult() {
        return this.result;
    }

    @Override
    public void run() {
        this.result = false;

        try {
            for (;;) {
                String line = this.reader.readLine();
                if (line != null) {
                    this.lines.add(line);

                    if (this.isErrorReader) {
                        this.logWriter.error(line);
                    } else {
                        this.logWriter.info(line);
                    }
                } else {
                    break;
                }
            }
            this.reader.close();

            this.result = true;
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}