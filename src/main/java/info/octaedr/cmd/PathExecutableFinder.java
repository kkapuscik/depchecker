package info.octaedr.cmd;

import java.io.File;

/**
 * Utility for searching for executable file on path.
 */
public class PathExecutableFinder {

    /**
     * Default extension of Windows executable files.
     */
    public static final String DEFAULT_WINDOWS_EXE_EXTENSION = ".exe";

    /**
     * Constructs finder.
     */
    public PathExecutableFinder() {
        // nothing to do
    }

    /**
     * Searches for the executable.
     * 
     * <p>
     * When searching the utility will look for each path entry for a match with
     * any of the given command names. All names are checked before moving to
     * the next path entry.
     * 
     * @param command
     *            Command to search for. A list of file name the application
     *            could have for which to look.
     * 
     * @return Command file or null if not found.
     */
    public File find(final String... command) {
        /* get the environment variable */
        final String pathEnv = System.getenv("PATH");
        if (pathEnv == null) {
            return null;
        }

        /* for all path entries */
        final String[] pathEntries = pathEnv.split(File.pathSeparator);
        for (final String path : pathEntries) {
            /* for all given command names */
            for (final String commandName : command) {
                /* check if a command file exists and is executable */
                final File commandFile = new File(path, commandName);
                if (commandFile.canExecute()) {
                    /* file is found */
                    return commandFile;
                }
            }
        }

        /* nothing found */
        return null;
    }

}
