package info.octaedr.cmd;

import info.octaedr.depchecker.model.LogWriter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Utility for running processes.
 */
public class ProcessExecutor {

    /**
     * Object for writing log messages.
     */
    private LogWriter logWriter;

    /**
     * Name of the executable file.
     */
    private String executableName;

    /**
     * Working directory.
     */
    private File workingDirectory;

    /**
     * List of arguments.
     */
    private final ArrayList<String> arguments = new ArrayList<>();

    /**
     * Constructs the executor.
     */
    protected ProcessExecutor() {
        // nothing to do
    }

    /**
     * Sets object for writing log messages.
     * 
     * @param logWriter
     *            Writer object.
     */
    public void setLogWriter(final LogWriter logWriter) {
        this.logWriter = logWriter;
    }

    /**
     * Returns name of the executable file.
     * 
     * @return Name of the executable file.
     */
    public String getExecutableName() {
        return this.executableName;
    }

    /**
     * Sets name of the executable file.
     * 
     * @param exeName
     *            Name of the executable file. Null to unset.
     */
    public void setExecutableName(final String exeName) {
        this.executableName = exeName;
    }

    /**
     * Sets name of the executable file.
     * 
     * <p>
     * The executable name is set to a absolute path of the given file.
     * 
     * @param executable
     *            Executable file. Null to unset.
     */
    public void setExecutable(final File executable) {
        if (executable != null) {
            setExecutableName(executable.getAbsolutePath());
        } else {
            setExecutableName(null);
        }
    }

    /**
     * Returns working directory.
     * 
     * @return Working directory or null if it was not set.
     */
    public File getWorkingDirectory() {
        return this.workingDirectory;
    }

    /**
     * Sets working directory.
     * 
     * @param workingDirectory
     *            Directory to set. Null to unset.
     */
    public void setWorkingDirectory(final File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    /**
     * Removes all arguments.
     */
    public void clearArguments() {
        this.arguments.clear();
    }

    /**
     * Add execution argument.
     * 
     * @param argValue
     *            Value to be appended as the argument.
     */
    public void addArgument(final String argValue) {
        this.arguments.add(argValue);
    }

    /**
     * Executes the application.
     * 
     * @return True if execution was successful, false otherwise.
     */
    public boolean run() {
        // TODO: polish the solution, catch all errors etc.

        if (this.logWriter == null) {
            return false;
        }

        if (this.workingDirectory == null) {
            return false;
        }

        if (this.executableName == null) {
            return false;
        }

        final ArrayList<String> commandLine = new ArrayList<>();
        commandLine.add(this.executableName);
        commandLine.addAll(this.arguments);

        boolean result = false;

        final ProcessBuilder processBuilder = new ProcessBuilder(commandLine);

        processBuilder.directory(this.workingDirectory);
        try {
            Process process = processBuilder.start();
            final InputReaderThread errThread = new InputReaderThread(process.getErrorStream(),
                    true, this.logWriter);
            final InputReaderThread stdThread = new InputReaderThread(process.getInputStream(),
                    false, this.logWriter);

            errThread.start();
            stdThread.start();

            result = (process.waitFor() == 0);

            errThread.join();
            stdThread.join();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return result;
    }
}
