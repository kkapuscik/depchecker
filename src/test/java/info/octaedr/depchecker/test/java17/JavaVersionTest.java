package info.octaedr.depchecker.test.java17;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test for checking java version.
 * 
 * @author Krzysztof Kapuscik
 */
public class JavaVersionTest {

    /**
     * Checks if string could be used in switch.
     */
    @Test
    public void testStringInSwitch() {
        Assert.assertEquals(isZeroOneOrOther("zero"), 0);
        Assert.assertEquals(isZeroOneOrOther("one"), 1);
        Assert.assertEquals(isZeroOneOrOther("two"), -1);
    }

    /**
     * Returns value for given string.
     * 
     * @param valueString
     *            The value for which value shall be returned.
     * 
     * @return 1 for "one", 0 for "zero", -1 for anything else.
     */
    private int isZeroOneOrOther(final String valueString) {
        switch (valueString) {
            case "zero":
                return 0;
            case "one":
                return 1;
            default:
                return -1;
        }
    }

}
