package info.octaedr.depchecker.test.javafx;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for JavaFX dependency.
 * 
 * @author Krzysztof Kapuscik
 */
public class JavaFxDependencyTest {

    /**
     * Checks if JavaFX classes exists by loading simple JavaFX application class.
     */
    @Test
    public void testJavafxClassesExists() {
        // loading the HelloWorldApp class does the checking
        // as it contains JavaFX imports.
        try {
            Assert.assertNotNull(HelloWorldApp.class.getName());
        } catch (Throwable t) {
            Assert.fail(t.getMessage());
        }
    }
}
