# README

Depchecker is a source code dependency checking application.

Look also at: http://kkapuscik.bitbucket.org/

***

## What is this repository for?

The repository holds source code of the *depchecker* application.

***

## Version

Currently depchecker is not finished and still in development. There is no version information currently available.

***

## How do I get set up?

Following information describes what is needed to build the project.

### Set up needed

The project requires:
* Eclipse
* Maven (could be used in a form of m2e plugin for eclipse).

### Summary of set up

To prepare the project:
* Get the sources.
* Import the project into eclipse.
* Use maven menu to update the project.
* Then build.

### How to run tests

You can run the tests using maven target for tests.

### Deployment instructions

Deployment is not yet defined.

***

## Contribution guidelines

Following contribution guidelines are defined:

* Follow the coding standard of the project.
    * The code formatter & eclipse settings should be available in repository.
    * If you cannot find them please ask the project owner.

***

## Who do I talk to?

### Repo owner or admin

Krzysztof Kapuscik (kkapuscik) - k.kapuscik@gmail.com

***

## Useful links

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)